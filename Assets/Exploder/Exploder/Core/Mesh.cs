﻿// Version 1.6.5
// ©2016 Reindeer Games
// All rights reserved
// Redistribution of source code without permission not allowed

using UnityEngine;
using UnityMesh = UnityEngine.Mesh;

namespace Exploder
{
    public class ExploderMesh
    {
        public int[] triangles;
        public Vector3[] vertices;
        public Vector3[] normals;
        public Vector2[] uv;
        public Vector4[] tangents;
        public Color32[] colors32;

        public Vector3 centroid;
        public int minX, maxX, minY, maxY, minZ, maxZ;

        public ExploderMesh()
        {
        }

        public ExploderMesh(UnityMesh unityMesh)
        {
            triangles = unityMesh.triangles;
            vertices = unityMesh.vertices;
            normals = unityMesh.normals;
            uv = unityMesh.uv;
            tangents = unityMesh.tangents;
            colors32 = unityMesh.colors32;

            CalculateCentroid();
        }

        public void CalculateCentroid()
        {
            centroid = Vector3.zero;
            minX = 0;
            minX = 0;
            minZ = 0;
            maxX = 0;
            maxY = 0;
            maxZ = 0;

            for (int i=0; i<vertices.Length; i++)
            {
                if (vertices[minX].x > vertices[i].x)
                    minX = i;
                if (vertices[minY].y > vertices[i].y)
                    minY = i;
                if (vertices[minZ].z > vertices[i].z)
                    minZ = i;

                if (vertices[maxX].x < vertices[i].x)
                    maxX = i;
                if (vertices[maxY].y < vertices[i].y)
                    maxY = i;
                if (vertices[maxZ].z < vertices[i].z)
                    maxZ = i;

                centroid += vertices[i];
            }

            centroid /= vertices.Length;
        }

        public UnityMesh ToUnityMesh()
        {
            return new UnityMesh
            {
                vertices = vertices,
                normals = normals,
                uv = uv,
                tangents = tangents,
                colors32 = colors32,
                triangles = triangles
            };
        }
    }
}
