// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4795,x:32724,y:32693,varname:node_4795,prsc:2|emission-2393-OUT,alpha-2812-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:32070,y:32476,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ce55edf1ad1a06640a637866363c17a4,ntxv:0,isnm:False|UVIN-2047-UVOUT;n:type:ShaderForge.SFN_Multiply,id:2393,x:32469,y:32761,varname:node_2393,prsc:2|A-6074-RGB,B-797-RGB,C-2294-OUT,D-594-RGB,E-3500-RGB;n:type:ShaderForge.SFN_Color,id:797,x:32097,y:32819,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_ScreenPos,id:4038,x:31648,y:32793,varname:node_4038,prsc:2,sctp:0;n:type:ShaderForge.SFN_RemapRange,id:921,x:31755,y:32553,varname:node_921,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:0.2|IN-4038-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:2294,x:32205,y:33058,ptovrint:False,ptlb:Emission,ptin:_Emission,varname:node_2294,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Panner,id:2047,x:31918,y:32553,varname:node_2047,prsc:2,spu:0,spv:1|UVIN-921-OUT,DIST-4144-T;n:type:ShaderForge.SFN_Time,id:4144,x:31838,y:32793,varname:node_4144,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:594,x:32375,y:32540,ptovrint:False,ptlb:Fire,ptin:_Fire,varname:node_594,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:997e224c4c8499c44953b4bdc61429c7,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2812,x:32497,y:32950,varname:node_2812,prsc:2|A-6074-A,B-594-A,C-3500-A;n:type:ShaderForge.SFN_VertexColor,id:3500,x:32152,y:32669,varname:node_3500,prsc:2;proporder:6074-797-2294-594;pass:END;sub:END;*/

Shader "Shader Forge/Propulsion" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _TintColor ("Color", Color) = (1,1,1,1)
        _Emission ("Emission", Float ) = 1
        _Fire ("Fire", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _TintColor;
            uniform float _Emission;
            uniform sampler2D _Fire; uniform float4 _Fire_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 screenPos : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
////// Lighting:
////// Emissive:
                float4 node_4144 = _Time + _TimeEditor;
                float2 node_2047 = ((i.screenPos.rg*0.1+0.1)+node_4144.g*float2(0,1));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_2047, _MainTex));
                float4 _Fire_var = tex2D(_Fire,TRANSFORM_TEX(i.uv0, _Fire));
                float3 emissive = (_MainTex_var.rgb*_TintColor.rgb*_Emission*_Fire_var.rgb*i.vertexColor.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,(_MainTex_var.a*_Fire_var.a*i.vertexColor.a));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
