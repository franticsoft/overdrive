// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.22 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.22;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4795,x:32724,y:32693,varname:node_4795,prsc:2|emission-2397-OUT;n:type:ShaderForge.SFN_Multiply,id:2393,x:31798,y:32959,varname:node_2393,prsc:2|A-5230-OUT,B-3101-OUT;n:type:ShaderForge.SFN_TexCoord,id:394,x:30687,y:32988,varname:node_394,prsc:2,uv:0;n:type:ShaderForge.SFN_Sin,id:2879,x:31039,y:33062,varname:node_2879,prsc:2|IN-7527-OUT;n:type:ShaderForge.SFN_Multiply,id:7527,x:30867,y:33062,varname:node_7527,prsc:2|A-394-U,B-9279-OUT;n:type:ShaderForge.SFN_Pi,id:9279,x:30703,y:33132,varname:node_9279,prsc:2;n:type:ShaderForge.SFN_Multiply,id:5230,x:31545,y:32858,varname:node_5230,prsc:2|A-7521-OUT,B-2063-OUT;n:type:ShaderForge.SFN_OneMinus,id:7924,x:30919,y:32608,varname:node_7924,prsc:2|IN-3044-V;n:type:ShaderForge.SFN_TexCoord,id:3044,x:30725,y:32618,varname:node_3044,prsc:2,uv:0;n:type:ShaderForge.SFN_Power,id:2063,x:31285,y:33027,varname:node_2063,prsc:2|VAL-2879-OUT,EXP-447-OUT;n:type:ShaderForge.SFN_Vector1,id:447,x:31229,y:33183,varname:node_447,prsc:2,v1:4;n:type:ShaderForge.SFN_Power,id:8971,x:31282,y:32648,varname:node_8971,prsc:2|VAL-7924-OUT,EXP-5500-OUT;n:type:ShaderForge.SFN_Vector1,id:5500,x:31180,y:32776,varname:node_5500,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Vector1,id:3101,x:31641,y:33050,varname:node_3101,prsc:2,v1:1;n:type:ShaderForge.SFN_TexCoord,id:6200,x:30490,y:32175,varname:node_6200,prsc:2,uv:0;n:type:ShaderForge.SFN_Power,id:9733,x:31020,y:32340,varname:node_9733,prsc:2|VAL-7561-OUT,EXP-1415-OUT;n:type:ShaderForge.SFN_Vector1,id:1415,x:30832,y:32395,varname:node_1415,prsc:2,v1:32;n:type:ShaderForge.SFN_OneMinus,id:7561,x:30762,y:32208,varname:node_7561,prsc:2|IN-6200-V;n:type:ShaderForge.SFN_OneMinus,id:1832,x:31221,y:32340,varname:node_1832,prsc:2|IN-9733-OUT;n:type:ShaderForge.SFN_Multiply,id:7521,x:31435,y:32431,varname:node_7521,prsc:2|A-1832-OUT,B-8971-OUT;n:type:ShaderForge.SFN_Blend,id:7565,x:32231,y:32993,varname:node_7565,prsc:2,blmd:3,clmp:True|SRC-2393-OUT,DST-9161-RGB;n:type:ShaderForge.SFN_Color,id:9161,x:32043,y:33053,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_9076,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.8,c3:0.75,c4:1;n:type:ShaderForge.SFN_Fresnel,id:6892,x:32161,y:33284,varname:node_6892,prsc:2|EXP-4970-OUT;n:type:ShaderForge.SFN_Multiply,id:2397,x:32496,y:33106,varname:node_2397,prsc:2|A-7565-OUT,B-2228-OUT;n:type:ShaderForge.SFN_OneMinus,id:2228,x:32326,y:33234,varname:node_2228,prsc:2|IN-6892-OUT;n:type:ShaderForge.SFN_Vector1,id:4970,x:31953,y:33348,varname:node_4970,prsc:2,v1:4;proporder:9161;pass:END;sub:END;*/

Shader "Shader Forge/JetBeam" {
    Properties {
        _Color ("Color", Color) = (1,0.8,0.75,1)
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = (saturate((((((1.0 - pow((1.0 - i.uv0.g),32.0))*pow((1.0 - i.uv0.g),1.5))*pow(sin((i.uv0.r*3.141592654)),4.0))*1.0)+_Color.rgb-1.0))*(1.0 - pow(1.0-max(0,dot(normalDirection, viewDirection)),4.0)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
