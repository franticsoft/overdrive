Shader "Graphic/ScreenDistortion" 
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_DistortionMap("Base (RGB)", 2D) = "white" {}
	}

	SubShader 
	{
		Pass 
		{
			ZTest Always
			ZWrite Off
				
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform sampler2D _DistortionMap;
			uniform half _DistortionFactor;
			uniform half _DistortionOffset;			

			fixed4 frag (v2f_img i) : SV_Target
			{
				fixed4 disto = tex2D(_DistortionMap, i.uv + fixed2(0.0, _DistortionOffset));
				fixed2 uv = i.uv + (fixed2(-1.0, -1.0) + disto.rr*2.0) * _DistortionFactor;
				fixed4 original = tex2D(_MainTex, uv);
				return original;
			}
			ENDCG
		}
	}
Fallback off
}
