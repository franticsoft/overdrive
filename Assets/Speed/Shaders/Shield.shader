// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4795,x:32724,y:32693,varname:node_4795,prsc:2|emission-2393-OUT,alpha-9381-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:32068,y:32526,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:915d8f78ca7887249b27e3c9fb906cc7,ntxv:0,isnm:False|UVIN-5050-UVOUT;n:type:ShaderForge.SFN_Multiply,id:2393,x:32495,y:32793,varname:node_2393,prsc:2|A-6074-RGB,B-797-RGB,C-7211-OUT,D-9046-RGB;n:type:ShaderForge.SFN_Color,id:797,x:32257,y:32937,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.9379311,c3:1,c4:1;n:type:ShaderForge.SFN_Rotator,id:5050,x:31856,y:32707,varname:node_5050,prsc:2|UVIN-6690-UVOUT,PIV-5852-OUT,ANG-4307-OUT;n:type:ShaderForge.SFN_Time,id:5565,x:31558,y:32921,varname:node_5565,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:6690,x:31658,y:32587,varname:node_6690,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector2,id:5852,x:31658,y:32782,varname:node_5852,prsc:2,v1:0,v2:0.5;n:type:ShaderForge.SFN_ValueProperty,id:7211,x:32257,y:33124,ptovrint:False,ptlb:Emission,ptin:_Emission,varname:node_7211,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Tex2d,id:9046,x:32085,y:32748,ptovrint:False,ptlb:node_9046,ptin:_node_9046,varname:node_9046,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:915d8f78ca7887249b27e3c9fb906cc7,ntxv:0,isnm:False|UVIN-3658-UVOUT;n:type:ShaderForge.SFN_Rotator,id:3658,x:31946,y:32911,varname:node_3658,prsc:2|UVIN-6690-UVOUT,ANG-4307-OUT;n:type:ShaderForge.SFN_Multiply,id:6029,x:32453,y:32574,varname:node_6029,prsc:2|A-6074-A,B-1932-OUT;n:type:ShaderForge.SFN_Vector1,id:1932,x:32273,y:32550,varname:node_1932,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Add,id:9381,x:32532,y:32951,varname:node_9381,prsc:2|A-6029-OUT,B-7588-OUT;n:type:ShaderForge.SFN_Multiply,id:7588,x:32491,y:33161,varname:node_7588,prsc:2|A-9046-A,B-6122-OUT;n:type:ShaderForge.SFN_Vector1,id:6122,x:32257,y:33232,varname:node_6122,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:4307,x:31736,y:33022,varname:node_4307,prsc:2|A-5565-TTR,B-2142-OUT;n:type:ShaderForge.SFN_Vector1,id:2142,x:31558,y:33118,varname:node_2142,prsc:2,v1:3;proporder:6074-797-7211-9046;pass:END;sub:END;*/

Shader "Shader Forge/Shield" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _TintColor ("Color", Color) = (0.5,0.9379311,1,1)
        _Emission ("Emission", Float ) = 5
        _node_9046 ("node_9046", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _TintColor;
            uniform float _Emission;
            uniform sampler2D _node_9046; uniform float4 _node_9046_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_5565 = _Time + _TimeEditor;
                float node_4307 = (node_5565.a*3.0);
                float node_5050_ang = node_4307;
                float node_5050_spd = 1.0;
                float node_5050_cos = cos(node_5050_spd*node_5050_ang);
                float node_5050_sin = sin(node_5050_spd*node_5050_ang);
                float2 node_5050_piv = float2(0,0.5);
                float2 node_5050 = (mul(i.uv0-node_5050_piv,float2x2( node_5050_cos, -node_5050_sin, node_5050_sin, node_5050_cos))+node_5050_piv);
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_5050, _MainTex));
                float node_3658_ang = node_4307;
                float node_3658_spd = 1.0;
                float node_3658_cos = cos(node_3658_spd*node_3658_ang);
                float node_3658_sin = sin(node_3658_spd*node_3658_ang);
                float2 node_3658_piv = float2(0.5,0.5);
                float2 node_3658 = (mul(i.uv0-node_3658_piv,float2x2( node_3658_cos, -node_3658_sin, node_3658_sin, node_3658_cos))+node_3658_piv);
                float4 _node_9046_var = tex2D(_node_9046,TRANSFORM_TEX(node_3658, _node_9046));
                float3 emissive = (_MainTex_var.rgb*_TintColor.rgb*_Emission*_node_9046_var.rgb);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,((_MainTex_var.a*0.5)+(_node_9046_var.a*0.5)));
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
