// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.22 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.22;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4795,x:32724,y:32693,varname:node_4795,prsc:2|emission-3945-OUT;n:type:ShaderForge.SFN_TexCoord,id:1658,x:31244,y:32346,varname:node_1658,prsc:2,uv:0;n:type:ShaderForge.SFN_Power,id:9767,x:31681,y:32451,varname:node_9767,prsc:2|VAL-8129-OUT,EXP-1920-OUT;n:type:ShaderForge.SFN_Vector1,id:1920,x:31555,y:32591,varname:node_1920,prsc:2,v1:1.5;n:type:ShaderForge.SFN_OneMinus,id:8129,x:31430,y:32416,varname:node_8129,prsc:2|IN-1658-V;n:type:ShaderForge.SFN_Multiply,id:8838,x:31934,y:32537,varname:node_8838,prsc:2|A-9767-OUT,B-2933-OUT;n:type:ShaderForge.SFN_Vector1,id:2933,x:31747,y:32631,varname:node_2933,prsc:2,v1:6;n:type:ShaderForge.SFN_Blend,id:1373,x:32102,y:32636,varname:node_1373,prsc:2,blmd:3,clmp:True|SRC-8838-OUT,DST-8027-RGB;n:type:ShaderForge.SFN_Color,id:8027,x:31927,y:32754,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_9076,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.8,c3:0.75,c4:1;n:type:ShaderForge.SFN_Multiply,id:3945,x:32385,y:32815,varname:node_3945,prsc:2|A-1373-OUT,B-160-OUT;n:type:ShaderForge.SFN_Fresnel,id:7642,x:32127,y:32935,varname:node_7642,prsc:2|EXP-6799-OUT;n:type:ShaderForge.SFN_Vector1,id:6799,x:31930,y:32980,varname:node_6799,prsc:2,v1:2;n:type:ShaderForge.SFN_OneMinus,id:160,x:32294,y:32956,varname:node_160,prsc:2|IN-7642-OUT;proporder:8027;pass:END;sub:END;*/

Shader "Shader Forge/JetTube" {
    Properties {
        _Color ("Color", Color) = (1,0.8,0.75,1)
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 emissive = (saturate(((pow((1.0 - i.uv0.g),1.5)*6.0)+_Color.rgb-1.0))*(1.0 - pow(1.0-max(0,dot(normalDirection, viewDirection)),2.0)));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
