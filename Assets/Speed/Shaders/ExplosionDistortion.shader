﻿Shader "Custom/ExplosionDistortion"
{
	Properties
	{
		_SceneRT("SceneRT", 2D) = "white" {}
		_Noise("Noise", 2D) = "white" {}
		_Opacity("Opacity", Float) = 1.0
	}
	SubShader
	{
		Tags 
		{ 
			"RenderType"="Opaque" 
			"Queue"="Overlay"
		}
		LOD 100

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float2 screenPos : TEXCOORD1;
			};

			sampler2D _SceneRT;
			sampler2D _Noise;
			uniform half _Opacity;
			
			v2f vert (appdata v)
			{
				v2f o;
				float4 screenPos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.vertex = screenPos;
				o.uv = v.uv;
				
				o.screenPos = screenPos.xy / screenPos.w;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float2 adjust = float2(sin(radians(i.uv.x*180.0)), sin(radians(i.uv.y*180.0)));
				float2 toCenter = normalize(float2(0.5, 0.5) - i.uv);
				float factor = 30.0;
				float distortion = length(tex2D(_Noise, i.uv).rgb);
				float2 displacement = ((1.0/1000.0) * toCenter) * factor * distortion * (adjust.x*adjust.y);
				float2 texCoord = (i.screenPos*0.5) + 0.5; // convert from [-1, 1] => [0, 1]	
			    // Invert uvs - TODO: GL vs DX check, there is a unity macro "UV_INVERTED" apparently.
				//texCoord.y = 1.0 - texCoord.y;
				float4 texColor = tex2D(_SceneRT, texCoord + displacement);				
				return float4(texColor.rgb, saturate(_Opacity));
			}
			ENDCG
		}
	}
}
