﻿Shader "Custom/AdditiveFX" 
{
	Properties
	 {
        _Color ("Main Color", Color) = (1,1,1,0.5)
        _MainTex ("Base (RGB)", 2D) = "white" { }
		_Offset ("Offset", Vector) = (0,0,0,0)
		_Emissive("Emissive", Float) = 1.0
    }

    SubShader 
	{
		Tags {"Queue" = "Transparent" }
        Pass 
		{
		  Cull Off
		  ZWrite Off
		  Blend SrcAlpha One

           CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag

        #include "UnityCG.cginc"

        fixed4 _Color;
        sampler2D _MainTex;
		float4 _Offset;
		float _Emissive;

        struct v2f {
            float4 pos : SV_POSITION;
            float2 uv : TEXCOORD0;
        };

        float4 _MainTex_ST;

        v2f vert (appdata_base v)
        {
            v2f o;
            o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
            o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
			o.uv += _Offset.xy;
            return o;
        }

        fixed4 frag (v2f i) : SV_Target
        {
            fixed4 texcol = tex2D (_MainTex, i.uv);
            return texcol * _Color * _Emissive;
        }
        ENDCG
        }
    }
}

