// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4795,x:32724,y:32693,varname:node_4795,prsc:2|emission-3184-OUT;n:type:ShaderForge.SFN_TexCoord,id:7940,x:31426,y:32584,varname:node_7940,prsc:2,uv:1;n:type:ShaderForge.SFN_Dot,id:9733,x:31607,y:32635,varname:node_9733,prsc:2,dt:0|A-7940-UVOUT,B-690-OUT;n:type:ShaderForge.SFN_Vector2,id:690,x:31426,y:32732,varname:node_690,prsc:2,v1:0,v2:1;n:type:ShaderForge.SFN_Subtract,id:7046,x:31815,y:32513,cmnt:gradient,varname:node_7046,prsc:2|A-9075-OUT,B-9733-OUT;n:type:ShaderForge.SFN_Vector1,id:9075,x:31621,y:32451,varname:node_9075,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:3184,x:32480,y:32791,cmnt:Opacity param,varname:node_3184,prsc:2|A-6449-OUT,B-9734-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9734,x:32480,y:32935,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:node_9734,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:8017,x:32068,y:32788,cmnt:Pulse,varname:node_8017,prsc:2|A-7046-OUT,B-9532-OUT;n:type:ShaderForge.SFN_Sin,id:407,x:31718,y:33016,varname:node_407,prsc:2|IN-4004-OUT;n:type:ShaderForge.SFN_Time,id:572,x:31334,y:32980,varname:node_572,prsc:2;n:type:ShaderForge.SFN_Add,id:137,x:31897,y:33016,varname:node_137,prsc:2|A-407-OUT,B-3398-OUT;n:type:ShaderForge.SFN_Vector1,id:3398,x:31897,y:33141,varname:node_3398,prsc:2,v1:1;n:type:ShaderForge.SFN_Divide,id:9532,x:32087,y:33016,varname:node_9532,prsc:2|A-137-OUT,B-7896-OUT;n:type:ShaderForge.SFN_Vector1,id:7896,x:32087,y:33141,varname:node_7896,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:4004,x:31544,y:33016,varname:node_4004,prsc:2|A-572-T,B-7446-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7446,x:31525,y:33197,ptovrint:False,ptlb:PulseSpeed,ptin:_PulseSpeed,cmnt:Pulse speed param,varname:node_7446,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:8;n:type:ShaderForge.SFN_Multiply,id:6449,x:32284,y:32788,cmnt:Color param,varname:node_6449,prsc:2|A-8017-OUT,B-5650-OUT;n:type:ShaderForge.SFN_Vector3,id:5650,x:32284,y:32919,varname:node_5650,prsc:2,v1:1,v2:0.1655172,v3:0;proporder:9734-7446;pass:END;sub:END;*/

Shader "Shader Forge/MineSensor" {
    Properties {
        _Opacity ("Opacity", Float ) = 1
        _PulseSpeed ("PulseSpeed", Float ) = 8
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _Opacity;
            uniform float _PulseSpeed;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_572 = _Time + _TimeEditor;
                float3 emissive = ((((1.0-dot(i.uv1,float2(0,1)))*((sin((node_572.g*_PulseSpeed))+1.0)/2.0))*float3(1,0.1655172,0))*_Opacity);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
