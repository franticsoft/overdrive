// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4795,x:32724,y:32693,varname:node_4795,prsc:2|emission-2393-OUT,alpha-7961-A;n:type:ShaderForge.SFN_Tex2d,id:6074,x:31529,y:32491,ptovrint:False,ptlb:EmissiveMap,ptin:_EmissiveMap,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9555432dcfffb494eaf9b3113ed2503e,ntxv:0,isnm:False|UVIN-6833-OUT;n:type:ShaderForge.SFN_Multiply,id:2393,x:32462,y:32691,varname:node_2393,prsc:2|A-4956-OUT,B-1150-A,C-7961-RGB,D-6418-OUT,E-824-OUT;n:type:ShaderForge.SFN_ValueProperty,id:824,x:32055,y:32778,ptovrint:False,ptlb:EmissiveAmount,ptin:_EmissiveAmount,varname:node_824,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:32;n:type:ShaderForge.SFN_Tex2d,id:1150,x:32462,y:32872,ptovrint:False,ptlb:Mask,ptin:_Mask,varname:_MainTex_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9555432dcfffb494eaf9b3113ed2503e,ntxv:0,isnm:False;n:type:ShaderForge.SFN_VertexColor,id:7961,x:32104,y:32868,varname:node_7961,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:6456,x:30893,y:32312,varname:node_6456,prsc:2,uv:0;n:type:ShaderForge.SFN_Power,id:5649,x:31753,y:32684,varname:node_5649,prsc:2|VAL-6074-R,EXP-8930-OUT;n:type:ShaderForge.SFN_Vector1,id:8930,x:31753,y:32814,varname:node_8930,prsc:2,v1:4;n:type:ShaderForge.SFN_Sin,id:7197,x:30929,y:32754,varname:node_7197,prsc:2|IN-5433-T;n:type:ShaderForge.SFN_Add,id:6833,x:31353,y:32615,varname:node_6833,prsc:2|A-3610-OUT,B-1525-OUT;n:type:ShaderForge.SFN_Append,id:4436,x:31145,y:32808,varname:node_4436,prsc:2|A-7197-OUT,B-1373-OUT;n:type:ShaderForge.SFN_Time,id:5433,x:30724,y:32830,varname:node_5433,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1373,x:30929,y:32879,varname:node_1373,prsc:2|A-5433-T,B-4140-OUT;n:type:ShaderForge.SFN_Vector1,id:4140,x:30929,y:33000,varname:node_4140,prsc:2,v1:-1;n:type:ShaderForge.SFN_DepthBlend,id:6418,x:32019,y:33058,varname:node_6418,prsc:2|DIST-3206-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3206,x:31848,y:33058,ptovrint:False,ptlb:DepthFadeDistance,ptin:_DepthFadeDistance,varname:_EmissiveAmount_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:25;n:type:ShaderForge.SFN_Multiply,id:3610,x:31098,y:32363,varname:node_3610,prsc:2|A-6456-UVOUT,B-810-OUT;n:type:ShaderForge.SFN_ValueProperty,id:810,x:30879,y:32494,ptovrint:False,ptlb:Tiling,ptin:_Tiling,varname:node_810,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.25;n:type:ShaderForge.SFN_Multiply,id:1525,x:31368,y:32777,varname:node_1525,prsc:2|A-4436-OUT,B-5670-OUT;n:type:ShaderForge.SFN_Vector1,id:5670,x:31256,y:32919,varname:node_5670,prsc:2,v1:0.25;n:type:ShaderForge.SFN_Multiply,id:4956,x:32089,y:32403,varname:node_4956,prsc:2|A-6074-RGB,B-1029-OUT;n:type:ShaderForge.SFN_Lerp,id:1029,x:32055,y:32569,varname:node_1029,prsc:2|A-4620-OUT,B-5649-OUT,T-461-OUT;n:type:ShaderForge.SFN_Power,id:4620,x:31753,y:32566,varname:node_4620,prsc:2|VAL-6074-RGB,EXP-8930-OUT;n:type:ShaderForge.SFN_Vector1,id:461,x:32055,y:32687,varname:node_461,prsc:2,v1:0.5;proporder:6074-824-1150-3206-810;pass:END;sub:END;*/

Shader "Shader Forge/ExplosionFire" {
    Properties {
        _EmissiveMap ("EmissiveMap", 2D) = "white" {}
        _EmissiveAmount ("EmissiveAmount", Float ) = 32
        _Mask ("Mask", 2D) = "white" {}
        _DepthFadeDistance ("DepthFadeDistance", Float ) = 25
        _Tiling ("Tiling", Float ) = 0.25
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _CameraDepthTexture;
            uniform float4 _TimeEditor;
            uniform sampler2D _EmissiveMap; uniform float4 _EmissiveMap_ST;
            uniform float _EmissiveAmount;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform float _DepthFadeDistance;
            uniform float _Tiling;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                float4 projPos : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
////// Lighting:
////// Emissive:
                float4 node_5433 = _Time + _TimeEditor;
                float2 node_6833 = ((i.uv0*_Tiling)+(float2(sin(node_5433.g),(node_5433.g*(-1.0)))*0.25));
                float4 _EmissiveMap_var = tex2D(_EmissiveMap,TRANSFORM_TEX(node_6833, _EmissiveMap));
                float node_8930 = 4.0;
                float node_5649 = pow(_EmissiveMap_var.r,node_8930);
                float4 _Mask_var = tex2D(_Mask,TRANSFORM_TEX(i.uv0, _Mask));
                float3 emissive = ((_EmissiveMap_var.rgb*lerp(pow(_EmissiveMap_var.rgb,node_8930),float3(node_5649,node_5649,node_5649),0.5))*_Mask_var.a*i.vertexColor.rgb*saturate((sceneZ-partZ)/_DepthFadeDistance)*_EmissiveAmount);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,i.vertexColor.a);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
