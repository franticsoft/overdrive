﻿// modified version of "VR/SpatialMapping/Wireframe.shader" from Unity 5.5f2
// added colors, discard option, removed stereo support and color by distance

Shader "UnityLibrary/Effects/Wireframe"
{
	Properties
	{
		_LineColor("LineColor", Color) = (1,1,1,1)
		_FillColor("FillColor", Color) = (0,0,0,0)
		_WireThickness("Wire Thickness", RANGE(0, 800)) = 100
		[MaterialToggle] UseDiscard("Discard Fill", Float) = 1
		_PulseRangeMin("PulseRangeMin", Float) = -3
		_PulseRangeMax("PulseRangeMax", Float) = 5
		_PulseLevel("PulseLevel", RANGE(0, 1)) = 0
		_PulseThickness("PulseThickness", Float) = .3
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}

		Pass
		{
			// Wireframe shader based on the the following
			// http://developer.download.nvidia.com/SDK/10/direct3d/Source/SolidWireframe/Doc/SolidWireframe.pdf

			CGPROGRAM
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			#pragma multi_compile _ USEDISCARD_ON
			#include "UnityCG.cginc"

			float _WireThickness;

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2g
			{
				float4 projectionSpaceVertex : SV_POSITION;
				float4 worldSpacePosition : TEXCOORD0;
				float4 localPosition : TEXCOORD1;
			};

			struct g2f
			{
				float4 projectionSpaceVertex : SV_POSITION;
				float4 worldSpacePosition : TEXCOORD0;
				float4 dist : TEXCOORD1;
				float4 z : TEXCOORD2;
			};

			v2g vert(appdata v)
			{
				v2g o;
				// UNITY_SETUP_INSTANCE_ID(v);
				// UNITY_INITIALIZE_OUTPUT(v2g, o);
				o.projectionSpaceVertex = UnityObjectToClipPos(v.vertex);
				o.worldSpacePosition = mul(unity_ObjectToWorld, v.vertex);
				o.localPosition = v.vertex;
				return o;
			}

			[maxvertexcount(3)]
			void geom(triangle v2g i[3], inout TriangleStream<g2f> triangleStream)
			{
				float2 p0 = i[0].projectionSpaceVertex.xy / i[0].projectionSpaceVertex.w;
				float2 p1 = i[1].projectionSpaceVertex.xy / i[1].projectionSpaceVertex.w;
				float2 p2 = i[2].projectionSpaceVertex.xy / i[2].projectionSpaceVertex.w;

				float2 edge0 = p2 - p1;
				float2 edge1 = p2 - p0;
				float2 edge2 = p1 - p0;

				// To find the distance to the opposite edge, we take the
				// formula for finding the area of a triangle Area = Base/2 * Height, 
				// and solve for the Height = (Area * 2)/Base.
				// We can get the area of a triangle by taking its cross product
				// divided by 2.  However we can avoid dividing our area/base by 2
				// since our cross product will already be double our area.
				float area = abs(edge1.x * edge2.y - edge1.y * edge2.x);
				float wireThickness = 800 - _WireThickness;

				g2f o;
				o.worldSpacePosition = i[0].worldSpacePosition;
				o.projectionSpaceVertex = i[0].projectionSpaceVertex;
				o.dist.xyz = float3((area / length(edge0)), 0.0, 0.0) * o.projectionSpaceVertex.w * wireThickness;
				o.dist.w = 1.0 / o.projectionSpaceVertex.w;
				o.z = i[0].localPosition.z;
				triangleStream.Append(o);

				o.worldSpacePosition = i[1].worldSpacePosition;
				o.projectionSpaceVertex = i[1].projectionSpaceVertex;
				o.dist.xyz = float3(0.0, (area / length(edge1)), 0.0) * o.projectionSpaceVertex.w * wireThickness;
				o.dist.w = 1.0 / o.projectionSpaceVertex.w;
				o.z = i[1].localPosition.z;
				triangleStream.Append(o);

				o.worldSpacePosition = i[2].worldSpacePosition;
				o.projectionSpaceVertex = i[2].projectionSpaceVertex;
				o.dist.xyz = float3(0.0, 0.0, (area / length(edge2))) * o.projectionSpaceVertex.w * wireThickness;
				o.dist.w = 1.0 / o.projectionSpaceVertex.w;
				o.z = i[2].localPosition.z;
				triangleStream.Append(o);
			}

			uniform fixed4 _LineColor;
			uniform fixed4 _FillColor;
			uniform float _DiscardLevel;
			uniform float _PulseRangeMin;
			uniform float _PulseRangeMax;
			uniform float _PulseLevel;
			uniform float _PulseThickness;

			fixed4 frag(g2f i) : SV_Target
			{
				float minDistanceToEdge = min(i.dist[0], min(i.dist[1], i.dist[2])) * i.dist[3];

				// Early out if we know we are not on a line segment.
				if (minDistanceToEdge > 0.9)
				{
					#ifdef USEDISCARD_ON
						discard;
					#else
						return _FillColor;
					#endif
				}

				float dist = lerp(_PulseRangeMin, _PulseRangeMax, _PulseLevel);
				if (i.z[0] > dist + _PulseThickness || i.z[0] < dist - _PulseThickness)
				{
#ifdef USEDISCARD_ON
					discard;
#else
					return _FillColor;
#endif
				}

				return _LineColor * 100.0;
			}
			ENDCG
		}
	}
}