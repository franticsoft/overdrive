﻿Shader "Unlit/MaskEffect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Mask("Mask", 2D) = "white" {}
		_Emissive("Emissive", Range(1.0, 500.0)) = 10.0
	}
	SubShader
	{
		Tags 
		{ 
			"Queue" = "Transparent+1"
		}

		Pass
		{
			Blend One One
			ZWrite Off
			//Cull Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;				
				float4 color : COLOR; // Color will be interpreted as life-time
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _Mask;
			uniform half _Emissive;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) * _Emissive;
				col.r = col.r * col.r + .2; // red shift
				fixed4 mask = tex2D(_Mask, i.uv);
				float maskFactor = floor(mask.r / (1.0 - i.color.r));
				col.rgb = col.rgb * pow(maskFactor, 1.0) * (pow(i.color.r, 1.0));
				return col;
			}
			ENDCG
		}
	}
}
