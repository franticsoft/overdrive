// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4795,x:32817,y:32690,varname:node_4795,prsc:2|emission-6486-OUT;n:type:ShaderForge.SFN_TexCoord,id:1982,x:31552,y:32354,varname:node_1982,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:3578,x:31530,y:32809,varname:node_3578,prsc:2,uv:0;n:type:ShaderForge.SFN_Length,id:9908,x:32143,y:32961,varname:node_9908,prsc:2|IN-6397-OUT;n:type:ShaderForge.SFN_Subtract,id:6560,x:31735,y:32809,varname:node_6560,prsc:2|A-3578-UVOUT,B-8135-OUT;n:type:ShaderForge.SFN_Vector1,id:8135,x:31735,y:32938,varname:node_8135,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:6397,x:31933,y:32938,varname:node_6397,prsc:2|A-6560-OUT,B-7174-OUT;n:type:ShaderForge.SFN_Clamp01,id:3857,x:32143,y:32824,varname:node_3857,prsc:2|IN-9908-OUT;n:type:ShaderForge.SFN_OneMinus,id:9069,x:32143,y:32694,varname:node_9069,prsc:2|IN-3857-OUT;n:type:ShaderForge.SFN_Append,id:7174,x:31933,y:33085,varname:node_7174,prsc:2|A-5928-OUT,B-1515-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5928,x:31735,y:33063,ptovrint:False,ptlb:TilingU,ptin:_TilingU,varname:node_5928,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_ValueProperty,id:1515,x:31735,y:33142,ptovrint:False,ptlb:TilingV,ptin:_TilingV,varname:node_1515,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:16;n:type:ShaderForge.SFN_Multiply,id:4286,x:32131,y:32532,varname:node_4286,prsc:2|A-7680-OUT,B-9069-OUT;n:type:ShaderForge.SFN_Power,id:7680,x:31918,y:32472,varname:node_7680,prsc:2|VAL-3112-OUT,EXP-8573-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8573,x:31711,y:32563,ptovrint:False,ptlb:LengthExp,ptin:_LengthExp,varname:node_8573,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:6486,x:32577,y:32678,varname:node_6486,prsc:2|A-4286-OUT,B-7091-RGB,C-7091-A,D-6093-OUT;n:type:ShaderForge.SFN_VertexColor,id:7091,x:32373,y:32710,varname:node_7091,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:6093,x:32373,y:32860,ptovrint:False,ptlb:EmissiveAmount,ptin:_EmissiveAmount,varname:node_6093,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:16;n:type:ShaderForge.SFN_OneMinus,id:3112,x:31727,y:32402,varname:node_3112,prsc:2|IN-1982-U;proporder:5928-1515-8573-6093;pass:END;sub:END;*/

Shader "Shader Forge/ExplosionSpark" {
    Properties {
        _TilingU ("TilingU", Float ) = 2
        _TilingV ("TilingV", Float ) = 16
        _LengthExp ("LengthExp", Float ) = 2
        _EmissiveAmount ("EmissiveAmount", Float ) = 16
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _TilingU;
            uniform float _TilingV;
            uniform float _LengthExp;
            uniform float _EmissiveAmount;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float3 emissive = ((pow((1.0 - i.uv0.r),_LengthExp)*(1.0 - saturate(length(((i.uv0-0.5)*float2(_TilingU,_TilingV))))))*i.vertexColor.rgb*i.vertexColor.a*_EmissiveAmount);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
