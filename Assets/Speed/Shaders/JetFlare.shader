// Shader created with Shader Forge v1.22 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.22;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4795,x:32724,y:32693,varname:node_4795,prsc:2|emission-4745-OUT;n:type:ShaderForge.SFN_Power,id:682,x:31661,y:32547,varname:node_682,prsc:2|VAL-5172-OUT,EXP-3067-OUT;n:type:ShaderForge.SFN_Vector1,id:7656,x:31343,y:32690,varname:node_7656,prsc:2,v1:4;n:type:ShaderForge.SFN_Multiply,id:7405,x:31874,y:32676,varname:node_7405,prsc:2|A-682-OUT,B-3305-OUT;n:type:ShaderForge.SFN_Vector1,id:3305,x:31874,y:32802,varname:node_3305,prsc:2,v1:4;n:type:ShaderForge.SFN_TexCoord,id:5141,x:30665,y:32809,varname:node_5141,prsc:2,uv:0;n:type:ShaderForge.SFN_RemapRange,id:6064,x:30665,y:32632,varname:node_6064,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-5141-UVOUT;n:type:ShaderForge.SFN_Multiply,id:4663,x:31001,y:32674,varname:node_4663,prsc:2|A-6064-OUT,B-6064-OUT;n:type:ShaderForge.SFN_ComponentMask,id:7326,x:31001,y:32523,varname:node_7326,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-4663-OUT;n:type:ShaderForge.SFN_Add,id:1852,x:31001,y:32389,varname:node_1852,prsc:2|A-7326-R,B-7326-G;n:type:ShaderForge.SFN_OneMinus,id:5172,x:31205,y:32281,varname:node_5172,prsc:2|IN-6655-OUT;n:type:ShaderForge.SFN_Clamp01,id:6655,x:31205,y:32416,varname:node_6655,prsc:2|IN-1852-OUT;n:type:ShaderForge.SFN_DepthBlend,id:7380,x:31828,y:33006,varname:node_7380,prsc:2|DIST-9917-OUT;n:type:ShaderForge.SFN_Multiply,id:7396,x:32497,y:32845,varname:node_7396,prsc:2|A-4745-OUT,B-7380-OUT;n:type:ShaderForge.SFN_Vector1,id:9917,x:31607,y:32996,varname:node_9917,prsc:2,v1:0.05;n:type:ShaderForge.SFN_Time,id:3304,x:30408,y:33153,varname:node_3304,prsc:2;n:type:ShaderForge.SFN_Sin,id:3197,x:30775,y:33176,varname:node_3197,prsc:2|IN-21-OUT;n:type:ShaderForge.SFN_Add,id:3754,x:30937,y:33152,varname:node_3754,prsc:2|A-3197-OUT,B-8704-OUT;n:type:ShaderForge.SFN_Vector1,id:8704,x:30874,y:33280,varname:node_8704,prsc:2,v1:1;n:type:ShaderForge.SFN_Divide,id:369,x:31156,y:33152,varname:node_369,prsc:2|A-3754-OUT,B-7468-OUT;n:type:ShaderForge.SFN_Vector1,id:7468,x:31108,y:33274,varname:node_7468,prsc:2,v1:2;n:type:ShaderForge.SFN_Lerp,id:3067,x:31568,y:32770,varname:node_3067,prsc:2|A-7656-OUT,B-1790-OUT,T-369-OUT;n:type:ShaderForge.SFN_Vector1,id:1790,x:31343,y:32770,varname:node_1790,prsc:2,v1:5;n:type:ShaderForge.SFN_Multiply,id:21,x:30595,y:33176,varname:node_21,prsc:2|A-3304-T,B-680-OUT;n:type:ShaderForge.SFN_Vector1,id:680,x:30455,y:33302,varname:node_680,prsc:2,v1:64;n:type:ShaderForge.SFN_Blend,id:4745,x:32246,y:32698,varname:node_4745,prsc:2,blmd:3,clmp:True|SRC-7405-OUT,DST-9076-RGB;n:type:ShaderForge.SFN_Color,id:9076,x:32058,y:32758,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_9076,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.8,c3:0.75,c4:1;proporder:9076;pass:END;sub:END;*/

Shader "Shader Forge/JetFlare" {
    Properties {
        _Color ("Color", Color) = (1,0.8,0.75,1)
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
/////// Vectors:
////// Lighting:
////// Emissive:
                float2 node_6064 = (i.uv0*2.0+-1.0);
                float2 node_7326 = (node_6064*node_6064).rg;
                float4 node_3304 = _Time + _TimeEditor;
                float3 node_4745 = saturate(((pow((1.0 - saturate((node_7326.r+node_7326.g))),lerp(4.0,5.0,((sin((node_3304.g*64.0))+1.0)/2.0)))*4.0)+_Color.rgb-1.0));
                float3 emissive = node_4745;
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
