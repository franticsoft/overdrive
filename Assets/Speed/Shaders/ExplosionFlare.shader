// Shader created with Shader Forge v1.22 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.22;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:4795,x:32724,y:32693,varname:node_4795,prsc:2|emission-2398-OUT;n:type:ShaderForge.SFN_TexCoord,id:3152,x:31332,y:32642,varname:node_3152,prsc:2,uv:0;n:type:ShaderForge.SFN_Length,id:3721,x:31843,y:32662,varname:node_3721,prsc:2|IN-8927-OUT;n:type:ShaderForge.SFN_Subtract,id:2779,x:31551,y:32695,varname:node_2779,prsc:2|A-3152-UVOUT,B-4387-OUT;n:type:ShaderForge.SFN_Vector1,id:4387,x:31551,y:32822,varname:node_4387,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Power,id:466,x:32204,y:32562,varname:node_466,prsc:2|VAL-3297-OUT,EXP-898-OUT;n:type:ShaderForge.SFN_ValueProperty,id:898,x:32204,y:32708,ptovrint:False,ptlb:Density,ptin:_Density,varname:node_898,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:8927,x:31551,y:32520,varname:node_8927,prsc:2|A-2779-OUT,B-3310-OUT;n:type:ShaderForge.SFN_Vector1,id:3310,x:31551,y:32642,varname:node_3310,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:2398,x:32462,y:32622,varname:node_2398,prsc:2|A-3002-OUT,B-1681-OUT,C-8749-OUT,D-7367-A,E-1718-OUT;n:type:ShaderForge.SFN_VertexColor,id:5644,x:31729,y:32888,varname:node_5644,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:8749,x:32462,y:32764,ptovrint:False,ptlb:EmissiveAmount,ptin:_EmissiveAmount,varname:node_8749,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:64;n:type:ShaderForge.SFN_Clamp01,id:3002,x:32204,y:32430,varname:node_3002,prsc:2|IN-466-OUT;n:type:ShaderForge.SFN_Clamp01,id:2211,x:31843,y:32536,varname:node_2211,prsc:2|IN-3721-OUT;n:type:ShaderForge.SFN_Lerp,id:1681,x:32208,y:32802,varname:node_1681,prsc:2|A-5644-RGB,B-3296-OUT,T-4754-OUT;n:type:ShaderForge.SFN_Vector3,id:4374,x:31948,y:33064,varname:node_4374,prsc:2,v1:1,v2:1,v3:1;n:type:ShaderForge.SFN_Power,id:4754,x:32208,y:32947,varname:node_4754,prsc:2|VAL-3297-OUT,EXP-7043-OUT;n:type:ShaderForge.SFN_Vector1,id:7043,x:32208,y:33077,varname:node_7043,prsc:2,v1:4;n:type:ShaderForge.SFN_Lerp,id:3296,x:31948,y:32936,varname:node_3296,prsc:2|A-5644-RGB,B-4374-OUT,T-3511-OUT;n:type:ShaderForge.SFN_Vector1,id:3511,x:31948,y:33153,varname:node_3511,prsc:2,v1:0.1;n:type:ShaderForge.SFN_VertexColor,id:7367,x:32457,y:32963,varname:node_7367,prsc:2;n:type:ShaderForge.SFN_OneMinus,id:3297,x:31843,y:32416,varname:node_3297,prsc:2|IN-2211-OUT;n:type:ShaderForge.SFN_DepthBlend,id:1718,x:32441,y:33158,varname:node_1718,prsc:2|DIST-2175-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2175,x:32191,y:33226,ptovrint:False,ptlb:DepthBlendDistance,ptin:_DepthBlendDistance,varname:node_2175,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:25;proporder:898-8749-2175;pass:END;sub:END;*/

Shader "Shader Forge/ExplosionFlare" {
    Properties {
        _Density ("Density", Float ) = 2
        _EmissiveAmount ("EmissiveAmount", Float ) = 64
        _DepthBlendDistance ("DepthBlendDistance", Float ) = 25
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _CameraDepthTexture;
            uniform float _Density;
            uniform float _EmissiveAmount;
            uniform float _DepthBlendDistance;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                float4 projPos : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float sceneZ = max(0,LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)))) - _ProjectionParams.g);
                float partZ = max(0,i.projPos.z - _ProjectionParams.g);
/////// Vectors:
////// Lighting:
////// Emissive:
                float node_3297 = (1.0 - saturate(length(((i.uv0-0.5)*2.0))));
                float3 emissive = (saturate(pow(node_3297,_Density))*lerp(i.vertexColor.rgb,lerp(i.vertexColor.rgb,float3(1,1,1),0.1),pow(node_3297,4.0))*_EmissiveAmount*i.vertexColor.a*saturate((sceneZ-partZ)/_DepthBlendDistance));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
