﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

namespace frantic
{
    public class NavigationBar : MonoBehaviour
    {
        public enum ConfigType
        {
            Default,
            Pause,
            MainMenuStart,
            Tutorial
        }

        [Serializable]
        public class Command
        {
            public Texture Icon;
            public string Text;
        }

        [Serializable]
        public class Config
        {
            public ConfigType Type;
            public Command[] Commands;
        }

        public GameObject CommandTemplate;
        public Config[] Configurations;

        void Start()
        {
        }

        void Update()
        {
        }

        public void SetConfiguration(ConfigType type)
        {
            transform.DestroyAllChildren();
            var config = Configurations.FirstOrDefault(c => c.Type == type);
            
            var pos = new Vector3(0.0f, 0.0f, 0.0f);
            for (int i=0; i<config.Commands.Length; ++i)
            {
                var command = Instantiate(CommandTemplate);
                var commandRect = command.GetComponent<RectTransform>();
                commandRect.localPosition = pos;
                command.transform.SetParent(transform, false);
                
                var icon = command.transform.FindChild("Icon").GetComponent<RawImage>();
                var text = command.transform.FindChild("Text").GetComponent<Text>();
                icon.texture = config.Commands[i].Icon;
                text.text = config.Commands[i].Text;

                pos.x += commandRect.rect.width;
            }

            var rect = GetComponent<RectTransform>();
            var height = rect.sizeDelta.y;
            rect.sizeDelta = new Vector2(pos.x, height);
        }
    }
}
