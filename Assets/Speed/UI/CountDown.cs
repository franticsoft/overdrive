﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class CountDown : MonoBehaviour
    {
        public void OnCountDownReady()
        {
            var manager = FranticNetworkManager.singleton as FranticNetworkManager;
            manager.LevelManager.CountDownInProgress = false;
        }

        public void OnCountDownFinished()
        {
            var rootGO = transform.parent.parent.gameObject;
            Destroy(rootGO);
        }
    }
}

