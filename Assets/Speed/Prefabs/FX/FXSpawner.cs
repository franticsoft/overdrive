﻿using UnityEngine;
using System.Collections;

public class FXSpawner : MonoBehaviour
{

    public GameObject FXToSpawn;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && FXToSpawn != null)
        {
            Instantiate(FXToSpawn, this.transform.position, this.transform.rotation);
        }
    }
}
