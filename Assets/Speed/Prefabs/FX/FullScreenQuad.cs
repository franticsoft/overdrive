﻿using UnityEngine;

public class FullScreenQuad : MonoBehaviour
{
	void Start ()
    {
        var vertices = new Vector3[6];
        var uvs = new Vector2[6];
        uvs[0] = new Vector2(1.0f, 1.0f); vertices[0] = new Vector3(1.0f, 1.0f, 0.0f); // Top Right
        uvs[1] = new Vector2(1.0f, 0.0f); vertices[1] = new Vector3(1.0f, -1.0f, 0.0f); // Bottom Right
        uvs[2] = new Vector2(0.0f, 1.0f); vertices[2] = new Vector3(-1.0f, 1.0f, 0.0f); // Top Left
        uvs[3] = new Vector2(0.0f, 1.0f); vertices[3] = new Vector3(-1.0f, 1.0f, 0.0f); // Top Left
        uvs[4] = new Vector2(1.0f, 0.0f); vertices[4] = new Vector3(1.0f, -1.0f, 0.0f); // Bottom Right
        uvs[5] = new Vector2(0.0f, 0.0f); vertices[5] = new Vector3(-1.0f, -1.0f, 0.0f); // Bottom Left
        var fullScreenQuad = new Mesh();
        fullScreenQuad.vertices = vertices;
        fullScreenQuad.uv = uvs;
        fullScreenQuad.triangles = new int[] { 0, 2, 1, 3, 5, 4 }; // DX
        //fullScreenQuad.triangles = new int[] { 0, 1, 2, 3, 4, 5 }; // OpenGL
        fullScreenQuad.name = "FullScreenQuad";
        GetComponent<MeshFilter>().mesh = fullScreenQuad;
	}	
}
