﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class ExplosionFlares : MonoBehaviour
    {
        public GameObject Flare;
        public FloatRange Angle = new FloatRange(45.0f, 90.0f); // 90 = shooting towards up
        public int NumFlares = 5;
        public FloatRange ExplosionForce = new FloatRange(100.0f, 120.0f);

        void Start()
        {
            for(int i=0; i< NumFlares; ++i)
            {
                var flare = Instantiate(Flare);
                flare.transform.SetParent(transform, false);

                var baseAngle = Random.Range(Angle.Min, Angle.Max) * Mathf.Deg2Rad;
                var baseVec = new Vector3(0.0f, Mathf.Sin(baseAngle), Mathf.Cos(baseAngle)).normalized;
                var angle = Random.Range(0.0f, 360.0f) * Mathf.Deg2Rad; // for random orientation
                var rotation = Quaternion.LookRotation(new Vector3(Mathf.Cos(angle), 0.0f, Mathf.Sin(angle)), Vector3.up);
                var direction = (rotation * baseVec).normalized;

                var rb = flare.GetComponent<Rigidbody>();
                rb.AddForce(direction * Random.Range(ExplosionForce.Min, ExplosionForce.Max), ForceMode.Impulse);
            }
        }
    }
}

