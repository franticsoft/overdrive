﻿using UnityEngine;

namespace frantic
{
    public class DistortionBillboard : MonoBehaviour
    {
        void Start()
        {
            var manager = FranticNetworkManager.singleton as FranticNetworkManager;
            GetComponent<MeshRenderer>().sharedMaterial.SetTexture("_SceneRT", manager.PlayerCamera.SceneCamera.targetTexture);
        }
    }
}

