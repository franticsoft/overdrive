﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class JetFX : MonoBehaviour
    {

        //GameObject jetBeam;
        //GameObject jetFlare;
        //GameObject jetFlare2;
        //GameObject JetTube;
        //GameObject jetSmoke;

        //void Start() 
        //{
        //    jetBeam = transform.FindChild("JetBeam");
        //    jetFlare = transform.FindChild("JetFlare");
        //    jetFlare2 = transform.FindChild("JetFlare2");
        //    JetTube = transform.FindChild("JetTube");
        //    jetSmoke = transform.FindChild("JetSmoke");
        //}

        public void SetIntensity(float intensity)
        {
            float f = Mathf.Clamp01(intensity);
            f = (-Mathf.Pow(2, -10 * f) + 1); //expo ease out
            transform.localScale = new Vector3(f, f, f);
        }
    }
}
