﻿using UnityEngine;
using System.Collections;

public class MissileHitFX : MonoBehaviour
{
    Light Light;
    float LightInitialIntensity;
    float LightFadeDuration = 1;
    float TotalDuration = 5;


    void Start()
    {
        Light = GetComponent<Light>();

        StartCoroutine("FadeLight");

        Invoke("DestroySelf", TotalDuration);
    }

    IEnumerator FadeLight() {
        Light.enabled = true;
        LightInitialIntensity = Light.intensity;

        float t = 0;
        while (t < 1) { 
            Light.intensity = Mathf.Lerp(LightInitialIntensity,0,t);
            t += Time.deltaTime / LightFadeDuration;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        yield return null;
    }

    void DestroySelf()
    {
        Destroy(this.gameObject);
    }

}
