﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class MenuCamera : MonoBehaviour
    {
        public float FollowFactor = 1.0f;
        public float LookAtFactor = 1.0f;
        public float FollowDuration = 5.0f;
        public float SwitchThreshold = 20.0f;

        MenuAI _followTarget;
        Vector3 _toCamera;
        float _timer;
        MenuAI[] _targets;

        void Start()
        {
            enabled = false;
        }

        void Update()
        {
            var toCamera = _followTarget.transform.forward * _toCamera.z + _followTarget.transform.up * _toCamera.y;
            var targetPos = _followTarget.transform.position + toCamera;
            transform.position = Vector3.Lerp(transform.position, targetPos, Mathf.Clamp01(FollowFactor * Time.smoothDeltaTime));

            var toLookAtTarget = (_followTarget.transform.position - transform.position).normalized;
            var targetRot = Quaternion.LookRotation(toLookAtTarget, _followTarget.transform.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Mathf.Clamp01(LookAtFactor * Time.smoothDeltaTime));

            _timer += Time.deltaTime;
            if(_timer > FollowDuration)
            {
                foreach(var target in _targets)
                {
                    if (ReferenceEquals(target, _followTarget))
                        continue;

                    var dist = (target.transform.position - _followTarget.transform.position).magnitude;
                    if(dist < SwitchThreshold)
                    {
                        _followTarget = target;
                        _timer = 0.0f;
                        break;
                    }
                }
            }
        }

        public void OnMenuAnimationFinished()
        {
            _targets = GameObject.FindObjectsOfType<MenuAI>();
            var targetIndex = Random.Range(0, _targets.Length);
            var target = _targets[targetIndex];
            _followTarget = target;
            _toCamera = _followTarget.transform.FindChild("CameraSpot").localPosition;
            _timer = 0.0f;
            enabled = true;
            GetComponent<Animator>().enabled = false;
        }
    }
}

