﻿using UnityEditor;
using System.IO;

namespace frantic
{
    [CustomEditor(typeof(MainMenu))]
    public class MainMenuEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }
}

