﻿Shader "Unlit/LogoDistortion"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_FireMap("Texture", 2D) = "white" {}
		_FireFactor("Fire Factor", Float) = 1.0
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }

		Pass
		{
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			uniform sampler2D _FireMap;
			uniform float4 _FireOffset;
			uniform float _FireFactor;

			float4 _MainTex_ST;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 fire = tex2D(_FireMap, i.uv + _FireOffset.xy);
				fixed4 fire2 = tex2D(_FireMap, float2(1.0, -1.0) - i.uv - _FireOffset.xy);
				fixed4 final = (col * col) + (fire * fire2 * _FireFactor);
				final.a = col.a;
				return final;
			}
			ENDCG
		}
	}
    Fallback off
}
