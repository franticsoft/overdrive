﻿using UnityEngine;

namespace frantic
{
    public class MenuAI : MonoBehaviour
    {
        public GameObject StartingTrack;
        public float Accel = 1.0f;
        public float MaxSpeed = 100.0f;

        BezierPath _track;
        TrackSegment _currentSegment;
        //float _curveRadius;
        int _curveIndex = 0;
        Vector2 _curvePos = Vector2.zero;
        float _forwardDist = 0.0f;
        PhysicBody _physicBody;

        void Start()
        {
            var body = transform.GetChild(0);
            _physicBody = body.GetComponent<PhysicBody>();
            _track = StartingTrack.GetComponent<BezierPath>();
            _currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();
            //_curveRadius = _track.transform.localScale.x * _currentSegment.Radius;
            //_curvePos.x = transform.position.x + _curveRadius;
            var _percentAccel = Accel * 0.4f;
            var _percentSpeed = MaxSpeed * 0.4f;
            Accel += Random.Range(-_percentAccel, _percentAccel);
            MaxSpeed += Random.Range(-_percentSpeed, _percentSpeed);
        }

        void Update()
        {
            // steering
            //_physicBody.Speed += Accel;
            //_physicBody.Speed = Mathf.Clamp(_physicBody.Speed, 0.0f, MaxSpeed);

            //// determine local basis
            //var tangent = _track.GetTangent(_curveIndex, _curvePos.y).normalized;
            //var up = _track.Curves[_curveIndex].GetUp(_curvePos.y);
            //var right = Vector3.Cross(up, tangent);
            //var banking = Quaternion.AngleAxis(_track.GetBanking(_curveIndex, _curvePos.y), tangent);
            //right = banking * right;
            //up = banking * up;

            //_physicBody.Direction = tangent;

            //// forward motion
            //var fwdFactor = Vector3.Dot(tangent, _physicBody.Direction);
            //var deltaDist = _physicBody.Speed * fwdFactor * Time.deltaTime;
            //_forwardDist += deltaDist;
            //var curveLength = _track.GetLength(_curveIndex);
            //if (_forwardDist > curveLength)
            //{
            //    _curveIndex = (_curveIndex + 1) % _track.Curves.Length;
            //    _forwardDist = _forwardDist - curveLength;
            //    _currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();
            //}

            //// update transform
            //_curvePos.y = _track.GetFactor(_curveIndex, _forwardDist);
            //var curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
            //var lateral0 = curvePoint - right * _curveRadius;
            //transform.position = lateral0 + right * _curvePos.x;
            //transform.rotation = Quaternion.LookRotation(tangent, up);
        }
    }
}

