﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace frantic
{
    public class LogoDistortion : MonoBehaviour
    {
        public float Speed = 1.0f;
        public float SinAmplitude = 1.0f;

        Material _material;
        void Start()
        {
            var image = GetComponent<RawImage>();
            _material = image.materialForRendering;
        }

        Vector4 _fireOffset = new Vector4();
        float _time = 0.0f;
        void Update()
        {
            _fireOffset.x = Mathf.Sin(_time) * SinAmplitude;
            _fireOffset.y -= Time.deltaTime * Speed;            
            _material.SetVector("_FireOffset", _fireOffset);
            _time += Time.deltaTime * Speed;
        }
    }
}

