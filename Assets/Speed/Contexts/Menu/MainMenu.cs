﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Linq;
using System.Collections.Generic;

namespace frantic
{
    [Serializable]
    public class MenuOptionDefinition : System.Object
    {
        public string Text;
        public bool Enabled;
    }

    public class MainMenu : MonoBehaviour
    {
        public enum State
        {
            Initial,
            Online,
            OnlineFindingGame,
        }

        public MenuOptionDefinition[] Definitions;
        public GameObject MenuItem;
        public GameObject Selector;
        public float LeftMargin = 10.0f;
        public float TopMargin = 10.0f;
        public float InputAcceptDuration = 0.4f;
        public Color MenuItemDisabledColor = Color.grey;

        public NavigationBar NavBar  { get { return _navBar; } }
        public Camera MenuCamera { get { return _menuCamera; } }

        GameObject _selector;
        RectTransform _selectorRect;
        int _selection;
        float _offsetY;
        float _inputAcceptTimer = -1.0f;
        GameSettings _gameSettings;
        NavigationBar _navBar;
        FranticNetworkManager _manager;
        Camera _menuCamera;
        GameState _gameState;
        Transform _menuItems;
        Vector3 _menuStartPos;
        State _state = State.Initial;
        MenuOptionDefinition[] _definitions;

        void Start()
        {
            _manager = NetworkManager.singleton as FranticNetworkManager;
            _manager.MainMenu = this;

            _gameSettings = _manager.GetComponent<GameSettings>();
            _menuCamera = GameObject.Find("MenuCamera").GetComponent<Camera>();
            _gameState = _manager.GetComponent<GameState>();
            _menuStartPos = new Vector3(LeftMargin, -TopMargin, 0.0f);

            var bg = transform.FindChild("BG");
            _selector = Instantiate(Selector);
            _selectorRect = _selector.GetComponent<RectTransform>();
            _selectorRect.localPosition = _menuStartPos;
            _selector.transform.SetParent(bg, false);

            var menuItemsGO = new GameObject("MenuItems");
            _menuItems = menuItemsGO.GetComponent<Transform>();
            _menuItems.SetParent(bg, false);

            PopulateMenu(Definitions);

            _navBar = Instantiate(_gameSettings.NavigationBar).GetComponent<NavigationBar>();
            _navBar.transform.SetParent(transform.parent, false);
            _navBar.SetConfiguration(NavigationBar.ConfigType.MainMenuStart);
        }

        void Scroll(bool down)
        {
            if(down)
            {
                if (_selection < _definitions.Length - 1)
                    ++_selection;
            }
            else
            {
                if(_selection > 0)
                    --_selection;
            }

            UpdateSelectorPosition();
        }

        AsyncOperation _shipSelectionSceneLoad;
        void Update()
        {
            if(_shipSelectionSceneLoad != null)
            {
                if(_shipSelectionSceneLoad.isDone)
                {
                    _shipSelectionSceneLoad = null;
                    Enable(false);

                    //_menuCamera.SetActive(false);
                    if (_manager.IsSplitScreenGame)
                    {
                        _menuCamera.cullingMask = 0;
                        _manager.StartLocalSplitScreen();
					}
                    else
                    {
                        switch(_state)
                        {
                            case State.Online:
                                {
                                    var gameName = _manager.networkAddress.ToString();
                                    _manager.matchMaker.CreateMatch(gameName, _manager.matchSize, true, string.Empty, string.Empty, string.Empty, 0, 0, new NetworkMatch.DataResponseDelegate<MatchInfo>(_manager.OnMatchCreate));
                                }
                                break;

                            case State.OnlineFindingGame:
                                {
                                    var matches = _manager.matches.OrderByDescending(m => m.currentSize).ToList();
                                    var fullestMatch = matches.FirstOrDefault();
                                    _manager.matchMaker.JoinMatch(fullestMatch.networkId, string.Empty, string.Empty, string.Empty, 0, 0, new NetworkMatch.DataResponseDelegate<MatchInfo>(_manager.OnMatchJoined));
                                }
                                break;
                        }
                    }
                }
                return;
            }

            switch (_state)
            {
                case State.Initial:
                    {
                        UpdateSelection();
                        if (Utils.IsInputAccept())
                        {                            
                            switch (_selection)
                            {
                                case 0: // single player
                                    _manager.ScreenDistortion.Distort();
                                    Enable(false);
                                    _navBar.SetConfiguration(NavigationBar.ConfigType.Default);

                                    OpenShipSelection();                                    
                                    break;                                

                                case 1: // online
                                        SwitchState(State.Online);                                    
                                    break;

                                case 2: // split-screen
                                    _manager.IsSplitScreenGame = true;
                                    _shipSelectionSceneLoad = OpenShipSelection();
                                    break;
                            }
                        }                       
                    }
                    break;

                case State.Online:
                    {
                        UpdateSelection();
                        if (Utils.IsInputAccept())
                        {
                            _manager.ScreenDistortion.Distort();
                            switch (_selection)
                            {
                                case 0: // create game
                                    {
                                        _shipSelectionSceneLoad = OpenShipSelection();
                                    }
                                    break;

                                case 1: // join game
                                    {
                                        FindGame();
                                        _state = State.OnlineFindingGame;
                                    }
                                    break;
                            }
                        }
                        else if(Utils.IsInputCancel())
                        {
                            _manager.ScreenDistortion.Distort();
                            SwitchState(State.Initial);                            
                        }
                    }
                    break;

                case State.OnlineFindingGame:
                    {
                        if (Utils.IsInputCancel())
                        {
                            _manager.ScreenDistortion.Distort();
                            SwitchState(State.Initial);
                        }
                        else if (_manager.matchMaker != null)
                        {
                            if(_manager.matches != null)
                            {
                                if (_manager.matches.Any())                                
                                    _shipSelectionSceneLoad = OpenShipSelection();
                                else
                                {
                                    _manager.matches = null;
                                    FindGame();
                                }
                            }
                        }
                    }
                    break;        
            }
        }

        void UpdateSelection()
        {
            var vertical = Input.GetAxis("Vertical");
            if (!MathUtils.IsZero(vertical))
            {
                if (_inputAcceptTimer < 0.0f)
                {
                    Scroll(vertical > 0.0f);
                    _inputAcceptTimer = InputAcceptDuration;
                }
                else
                {
                    _inputAcceptTimer -= Time.deltaTime;
                }
            }
            else
            {
                _inputAcceptTimer = -1.0f;
            }
        }

        public void SwitchState(State newState)
        {
            Enable(true);

            if (_state == newState)
                return;

            // clean up
            switch(_state)
            {
                case State.Online:
                    {                        
                    }
                    break;
            }

            // initialize
            switch(newState)
            {
                case State.Initial:
                    {
                        PopulateMenu(Definitions);
                        _navBar.SetConfiguration(NavigationBar.ConfigType.MainMenuStart);
                    }
                    break;

                case State.Online:
                    {
                        if(_manager.matchMaker == null)
                            _manager.StartMatchMaker();

                        var online = new MenuOptionDefinition[]
                        {
                             new MenuOptionDefinition() { Enabled = true, Text = "Create Game" },
                             new MenuOptionDefinition() { Enabled = true, Text = "Join Game" },
                        };
                        PopulateMenu(online);
                        _navBar.SetConfiguration(NavigationBar.ConfigType.Default);
                    }
                    break;
            }

            _state = newState;
        }

        void FindGame()
        {
            _manager.matchMaker.ListMatches(0, 20, string.Empty, true, 0, 0, new NetworkMatch.DataResponseDelegate<List<MatchInfoSnapshot>>(_manager.OnMatchList));
        }

        AsyncOperation OpenShipSelection()
        {
            //_menuCamera.GetComponent<Animator>().SetTrigger("ShipSelectionIn");
            //GetComponent<Animator>().SetTrigger("MenuTransitionOut");
            _gameState.GameContext = GameState.Context.ShipSelection;
            return SceneManager.LoadSceneAsync(_manager.ShipSelectionScene, LoadSceneMode.Additive);
        }

        void PopulateMenu(MenuOptionDefinition[] definitions)
        {
            _definitions = definitions;
            _menuItems.DestroyAllChildren();
            var startPos = _menuStartPos;
            foreach (var definition in _definitions)
            {
                var go = Instantiate(MenuItem);
                var rc = go.GetComponent<RectTransform>();
                rc.localPosition = startPos;
                _offsetY = rc.rect.height;
                go.transform.SetParent(_menuItems, false);
                var text = go.GetComponent<Text>();
                text.text = definition.Text;

                if (!definition.Enabled)
                    text.color = MenuItemDisabledColor;

                startPos.y -= rc.rect.height;
            }

            _selection = 0; // Mathf.Clamp(_selection, 0, _definitions.Length-1);
            UpdateSelectorPosition();
        }

        void UpdateSelectorPosition()
        {
            var pos = _selectorRect.localPosition;
            _selectorRect.localPosition = new Vector3(pos.x, -TopMargin - _offsetY * _selection);
        }

        void Enable(bool enable)
        {
            enabled = enable;
            transform.FindChild("Logo-wip").gameObject.SetActive(enable);
            transform.FindChild("BG").gameObject.SetActive(enable);
        }
    }
}
