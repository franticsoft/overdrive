﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking.NetworkSystem;

namespace frantic
{
    public class ShipSelector : MonoBehaviour
    {
        public GameObject[] Ships;
        public float RotateDegPerSecond = 180.0f;
        public float RotationXRange = 60.0f;
        public GameObject SplitScreenCamera;
        public GameObject ReadyIndicator;
        public float DelayToStartAnimation = 2.5f;
        public float DefaultSpotRotation = 45.0f;

        [Header("Stats")]
        public GameObject StatsPanel;
        public GameObject StatsProperty;
        public GameObject StatsPropertyName;
        public GameObject StatsGage;
        public GameObject StatsGageElem;
        public float TopMargin = 16.0f;
        public float LeftMargin = 16.0f;
        public float PropertyInterval = 5.0f;
        public float GageElemInterval = 5.0f;
        public Color GageLow = Color.red;
        public Color GageHigh = Color.yellow;
        public float GageAnimDelay = 0.06f;

        [Header("Online")]
        public GameObject OnlinePlayersContainer;
        public GameObject OnlinePlayersPanel;

        public class ShipSelectionContext
        {
            public GameObject screen;
            public GameObject selectedShipGO;
            public GameObject nextShipGO;
            public int nextShipIndex = 0;
            public Transform spots;
            public Transform middleSpot;
            public GameObject statsPanel;
            public float inputAcceptTimer = -1.0f;
            public List<TransitionAnim> transitions = new List<TransitionAnim>();
            public List<GageAnim> gageAnims;
            public int playerIndex;
        }

        ShipSelectionContext _context;
        GameSettings _gameSetings;
        GameState _gameState;
        FranticNetworkManager _manager;
        Transform _onlineContainer;
        GameObject _readyIndicator;
        MainMenu _mainMenu;

        public struct PlayerAnim
        {
            public GameObject player { get; set; }
            public Transform srcSpot { get; set; }
            public Transform destSpot { get; set; }
        }

        public class TransitionAnim
        {
            public float timer { get; set; }
            public bool right { get; set; }
            public PlayerAnim[] playerAnims;
        }

        public class GageAnim
        {
            public Transform gage { get; set; }
            public int targetValue { get; set; }
            public int currentValue;
            public float timer;
        }

        List<GageAnim> _gageAnimsToRemove = new List<GageAnim>();

        void Start()
        {
            _manager = NetworkManager.singleton as FranticNetworkManager;
            _manager.ShipSelector = this;

            _gameSetings = _manager.GetComponent<GameSettings>();
            _gameState = _manager.GetComponent<GameState>();
            _mainMenu = GameObject.FindGameObjectWithTag("MainMenu").GetComponent<MainMenu>();

            var canvasGO = GameObject.Find("ShipSelectionCanvas");
            var canvasScaler = canvasGO.GetComponent<CanvasScaler>();
            canvasScaler.referenceResolution = _manager.IsSplitScreenGame ? new Vector2(1280.0f, 720.0f) : new Vector2(1024.0f, 768.0f);
            var canvas = canvasGO.GetComponent<Canvas>();

            if (!_manager.IsSplitScreenGame)
                canvas.worldCamera = _manager.MainMenu.MenuCamera;

            _context = new ShipSelectionContext();
            _context.screen = GameObject.Find("ShipSelectionScreen");
            var splitScreen = _context.screen.transform.FindChild("SplitScreen").gameObject;
            splitScreen.SetActive(_manager.IsSplitScreenGame);

            if (!_manager.IsSplitScreenGame)
            {
                _context.spots = transform.FindChild("TopSpots");
                _context.middleSpot = _context.spots.GetChild(1);
                _context.selectedShipGO = CreateShip(0, _gameState.SelectedShipIndex[0]);
                AttachToSpot(_context.middleSpot, _context.selectedShipGO);

                InitStats(ref _context);
                _context.statsPanel.transform.SetParent(_context.screen.transform, false);
                UpdateStats(ref _context);

                _readyIndicator = GameObject.Instantiate(ReadyIndicator);
                _readyIndicator.transform.SetParent(_context.screen.transform, false);
                _readyIndicator.SetActive(false);
            }
        }

        public GameObject CreateShip(int playerIndex, int shipIndex)
        {
            var ship = Ships[shipIndex].transform.FindChild("Player").gameObject;
            var shipGO = Instantiate(ship);
            var player = shipGO.GetComponent<Player>();
            player.CommonInit();
            player.enabled = false;
            var body = player.Body;
            body.GetComponent<Hover>().enabled = false;
            var childRenderers = body.GetComponentsInChildren<Renderer>();
            foreach(var renderer in childRenderers)
                renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

            var topLight = shipGO.transform.FindChild("TopLight");
            topLight.GetComponent<Light>().intensity = 1.0f;

            if (_manager.IsSplitScreenGame)
            {
                var isTop = (playerIndex == 0);
                var layer = isTop ? "SplitScreenTop" : "SplitScreenBottom";
                var layerIndex = LayerMask.NameToLayer(layer);
                var renderers = shipGO.GetComponentsInChildren<MeshRenderer>();
                foreach (var renderer in renderers)
                    renderer.gameObject.layer = layerIndex;
            }

            // wireframe animation
            var shipBody = body.FindChildWithTag("WireframePulse");
            var wireFrameEntity = shipBody.transform.AddChild(_manager.GetComponent<GameSettings>().WireframePulse);
            var mf = wireFrameEntity.AddComponent<MeshFilter>();
            mf.sharedMesh = shipBody.GetComponent<MeshFilter>().sharedMesh;           

            //var leftProp = body.FindChild("PropulsionLeft");
            //var rightProp = body.FindChild("PropulsionRight");
            //if (player.PropulsionBoost)
            //{
            //    leftProp.AddChild(player.Propulsion).GetComponent<ParticleSystem>();
            //    rightProp.AddChild(player.Propulsion).GetComponent<ParticleSystem>();                
            //}
            //else
            //{
            //    leftProp.AddChild(player.Propulsion).GetComponent<JetFX>();
            //    rightProp.AddChild(player.Propulsion).GetComponent<JetFX>();
            //}

            return shipGO;
        }

        float _spotAnimationTimer;
        public void AttachToSpot(Transform spot, GameObject player)
        {
            player.transform.parent = spot;
            player.transform.ResetLocally();

            // set default rotation
            spot.transform.rotation = Quaternion.Euler(0.0f, DefaultSpotRotation, 0.0f);

            // disable animation
            var animator = spot.GetComponent<Animator>();
            animator.enabled = false;
            _spotAnimationTimer = 0.0f;
        }

        public void InitStats(ref ShipSelectionContext context)
        {
            context.statsPanel = Instantiate(StatsPanel);
            var stats = typeof(ShipStats).GetFields(BindingFlags.Public | BindingFlags.Instance);
            var propertyPos = new Vector3(LeftMargin, -TopMargin, 0.0f);
            foreach(var stat in stats)
            {
                var property = Instantiate(StatsProperty);
                var propertyRect = property.GetComponent<RectTransform>();
                propertyRect.localPosition = propertyPos;
                property.transform.SetParent(context.statsPanel.transform, false);
 
                var propertyName = Instantiate(StatsPropertyName);
                propertyName.transform.SetParent(property.transform, false);
                var propertyNameText = propertyName.GetComponent<Text>();
                propertyNameText.text = stat.Name;

                var gage = Instantiate(StatsGage);
                gage.transform.SetParent(property.transform, false);

                propertyPos.y -= (propertyRect.rect.height + PropertyInterval);
            }
        }

        public void UpdateStats(ref ShipSelectionContext context)
        {
            var stats = context.selectedShipGO.GetComponent<ShipStats>();
            var statFields = typeof(ShipStats).GetFields(BindingFlags.Public | BindingFlags.Instance);
            context.gageAnims = new List<GageAnim>();
            for (int i=0; i<statFields.Length; ++i)            
            {
                var value = (int)statFields[i].GetValue(stats);
                var property = context.statsPanel.transform.GetChild(i);
                var gage = property.transform.FindChildWithTag("Gage").transform;
                gage.DestroyAllChildren();
                var gageElemPos = new Vector3(0.0f, 0.0f, 0.0f);
                for (int j=0; j< _gameSetings.MaxStat; ++j)
                {
                    var gageElem = Instantiate(StatsGageElem);
                    var gageElemRect = gageElem.GetComponent<RectTransform>();
                    gageElemRect.localPosition = gageElemPos;

                    gageElem.transform.SetParent(gage.transform, false);
                    gageElemPos.x += (gageElemRect.rect.width + GageElemInterval);
                }

                // gage animation
                var gameAnim = new GageAnim
                {
                    gage = gage,
                    targetValue = value
                };
                gameAnim.currentValue = 0;
                gameAnim.timer = 0.0f;
                context.gageAnims.Add(gameAnim);
            }
        }

        public TransitionAnim Scroll(bool right)
        {
            var anim = new TransitionAnim
            {
                timer = 0.0f,
                right = right
            };
            return anim;
        }

        public void UpdateInput(ref ShipSelectionContext context)
        {
            var lateral = Input.GetAxis("P" + (context.playerIndex + 1) + "_Horizontal");
            if (!MathUtils.IsZero(lateral))
            {
                if (context.inputAcceptTimer < 0.0f)
                {
                    context.transitions.Add(Scroll(lateral > 0.0f));
                    context.inputAcceptTimer = 0.0f;
                }
                else
                {
                    context.inputAcceptTimer += Time.deltaTime;
                    if (context.inputAcceptTimer > _gameSetings.InputAcceptDuration)
                        context.inputAcceptTimer = -1.0f;
                }
            }
            else
            {
                context.inputAcceptTimer = -1.0f;
            }
        }

        public void HandleLookInput(ref ShipSelectionContext context)
        {
            var rotHorizontal = Input.GetAxis("P" + (context.playerIndex+1) + "_RotateHorizontal");
            if (!MathUtils.IsZero(rotHorizontal))
            {
                var animator = context.middleSpot.GetComponent<Animator>();
                animator.enabled = false;
                _spotAnimationTimer = 0.0f;
                var delta = rotHorizontal * RotateDegPerSecond * Time.smoothDeltaTime;
                context.middleSpot.rotation *= Quaternion.AngleAxis(delta, Vector3.up);
            }
        }
                
        public bool UpdateTransitions(ref ShipSelectionContext context)
        {
            if (!context.transitions.Any())
                return false;

            var transition = context.transitions.First();
            if (transition.timer < _gameSetings.TransitionDuration)
            {
                if (transition.playerAnims == null)
                {
                    transition.playerAnims = new PlayerAnim[2];
                    var previousSpot = transition.right ? context.spots.GetChild(0) : context.spots.GetChild(2);
                    var nextSpot = transition.right ? context.spots.GetChild(2) : context.spots.GetChild(0);

                    var nextShipIndex = (_gameState.SelectedShipIndex[context.playerIndex] + 1) % Ships.Length;
                    var previousShipIndex = _gameState.SelectedShipIndex[context.playerIndex] > 0 ? (_gameState.SelectedShipIndex[context.playerIndex] - 1) : (Ships.Length - 1);
                    context.nextShipIndex = transition.right ? previousShipIndex : nextShipIndex;
                    context.nextShipGO = CreateShip(context.playerIndex, nextShipIndex);
                    AttachToSpot(previousSpot, context.nextShipGO);

                    var animCurrent = new PlayerAnim
                    {
                        player = context.selectedShipGO,
                        srcSpot = context.middleSpot,
                        destSpot = nextSpot
                    };

                    var animNext = new PlayerAnim
                    {
                        player = context.nextShipGO,
                        srcSpot = previousSpot,
                        destSpot = context.middleSpot
                    };

                    transition.playerAnims[0] = animCurrent;
                    transition.playerAnims[1] = animNext;
                }

                var factor = transition.timer / _gameSetings.TransitionDuration;
                foreach (var anim in transition.playerAnims)
                {
                    var pos = Vector3.Lerp(anim.srcSpot.position, anim.destSpot.position, factor * factor);
                    anim.player.transform.position = pos;
                }
                transition.timer += Time.deltaTime;
            }
            else
            {
                // Transition finished
                foreach (var anim in transition.playerAnims)
                {
                    anim.player.transform.position = anim.destSpot.position;
                    AttachToSpot(anim.destSpot, anim.player);
                    if (!ReferenceEquals(anim.destSpot, context.middleSpot))
                        DestroyImmediate(anim.player);
                }
                _gameState.SelectedShipIndex[context.playerIndex] = context.nextShipIndex;
                context.selectedShipGO = context.nextShipGO;
                context.transitions.RemoveAt(0);
                UpdateStats(ref context);
            }

            return true;
        }

        void Update()
        {
            if(_manager.IsSplitScreenGame)               
                return;

            UpdateInput(ref _context);

            if(!UpdateTransitions(ref _context))           
            {
                HandleLookInput(ref _context);

                if (Utils.IsInputAccept())
                {
                    _manager.ScreenDistortion.Distort();
                    _gameState.SelectedShip[_context.playerIndex] = Ships[_gameState.SelectedShipIndex[_context.playerIndex]];
                    if (NetworkClient.active)
                    {
                        var localPlayer = GameObject.FindObjectsOfType<FranticLobbyPlayer>().Where(p => p.isLocalPlayer).FirstOrDefault();
                        localPlayer.CmdReadyStatus(true);
                        _readyIndicator.SetActive(true);

                        // TODO: start after count-down
                        if(NetworkServer.active)
                        {
                            if (_manager.AreAllPlayersReady())
                            {
                                enabled = false;
                                EnterTrackSelection();
                            }
                        }
                    }
                    else
                    {
                        enabled = false;
                        EnterTrackSelection();
                    }
                    return;
                }
                else if(Utils.IsInputCancel())
                {
                    _manager.ScreenDistortion.Distort();
                    bool wasReady = false;
                    if (NetworkClient.active)
                    {
                        var localPlayer = GameObject.FindObjectsOfType<FranticLobbyPlayer>().Where(p => p.isLocalPlayer).FirstOrDefault();
                        wasReady = localPlayer.IsReady();
                        localPlayer.CmdReadyStatus(false);
                        _readyIndicator.SetActive(false);
                    }

                    if(!wasReady)
                    {
                        enabled = false;
                        ExitShipSelection();
                    }                    
                    return;
                }
            }

            UpdateGageAnims(ref _context);

            // update rotation anim
            if (_spotAnimationTimer > DelayToStartAnimation)
            {
                var spot = _context.spots.GetChild(1);
                var animator = spot.GetComponent<Animator>();
                if (!animator.enabled)
                {
                    var rotation = spot.transform.eulerAngles.y;
                    var timeFactor = rotation / 360.0f;                    
                    animator.Play("ShipSelectionAnim", 0, timeFactor);
                    animator.enabled = true;
                }                
            }
            else
                _spotAnimationTimer += Time.deltaTime;

            // Update player statuses
            if (NetworkClient.active)
            {
                if(_onlineContainer == null)
                {
                    var onlineContainerGO = GameObject.Instantiate(OnlinePlayersContainer);
                    _onlineContainer = onlineContainerGO.transform;
                    _onlineContainer.SetParent(_context.screen.transform, false);
                }

                var players = GameObject.FindObjectsOfType<FranticLobbyPlayer>();
                if(players.Length != _onlineContainer.childCount || _playerLobbyDirty)
                {
                    // add missing slots
                    if(_onlineContainer.childCount < players.Length)
                    {
                        var panelRect = OnlinePlayersPanel.GetComponent<RectTransform>();
                        var margin = panelRect.localPosition.x;
                        var width = panelRect.sizeDelta.x;
                        var startingX = _onlineContainer.childCount * width + (_onlineContainer.childCount + 1) * margin;
                        var panelPos = new Vector3(startingX, 0.0f, 0.0f);
                        for (int i = _onlineContainer.childCount; i < players.Length; ++i)
                        {
                            var playerPanel = GameObject.Instantiate(OnlinePlayersPanel);
                            playerPanel.transform.localPosition = panelPos;
                            playerPanel.transform.SetParent(_onlineContainer, false);
                            panelPos.x += (width + margin);
                        }
                    }

                    // update slots
                    for (int i = 0; i < players.Length; ++i)
                    {
                        var player = players[i];
                        var panel = _onlineContainer.GetChild(i);
                        panel.gameObject.SetActive(true);
                        var name = panel.FindChild("OnlinePlayerName").GetComponent<Text>();
                        name.text = player.isLocalPlayer ? "YOU" : "Player" + (i+1);
                        var ready = panel.FindChild("OnlinePlayerReady");
                        ready.gameObject.SetActive(player.IsReady());
                    }

                    // hide extra slots
                    for (int i = players.Length; i < _onlineContainer.childCount; ++i)
                    {
                        var panel = _onlineContainer.GetChild(i);
                        panel.gameObject.SetActive(false);
                    }

                    if(_playerLobbyDirty)
                    {

                    }

                    _playerLobbyDirty = false;
                }
            }           
        }

        bool _playerLobbyDirty;
        public void SetPlayerLobbyDirty()
        {
            _playerLobbyDirty = true;
        }

        public void ExitShipSelection()
        {
            var isClient = !NetworkServer.active;
            _manager.ExitOnlineGame();
            if (isClient)
            {
                var menuCamera = GameObject.Find("MenuCamera");
                //menuCamera.GetComponent<Animator>().SetTrigger("ShipSelectionOut");
                //_mainMenu.GetComponent<Animator>().SetTrigger("MenuTransitionIn");
                _gameState.GameContext = GameState.Context.MainMenu;
                SceneManager.UnloadScene(_manager.ShipSelectionScene);
                _mainMenu.SwitchState(MainMenu.State.Initial);
            }
        }

        public void EnterTrackSelection()
        {
            _gameState.GameContext = GameState.Context.TrackSelection;
            //var menuCamera = GameObject.Find("MenuCamera");
            //var dof = menuCamera.GetComponent<UnityStandardAssets.ImageEffects.DepthOfField>();
            //dof.focalLength = _manager.TrackSelectionFocalDist;
            SceneManager.UnloadScene(_manager.ShipSelectionScene);
            SceneManager.LoadSceneAsync(_manager.TrackSelectionScene, LoadSceneMode.Additive);

            if (NetworkServer.active)            
                NetworkServer.SendToAll((short)FranticMessage.EnterTrackSelection, new IntegerMessage());            
        }

        public void UpdateGageAnims(ref ShipSelectionContext context)
        {
            if (!context.gageAnims.Any())
                return;

            _gageAnimsToRemove.Clear();
            foreach (var anim in context.gageAnims)
            {
                if (anim.timer > GageAnimDelay)
                {
                    if (anim.currentValue > anim.targetValue)
                    {
                        _gageAnimsToRemove.Add(anim);
                        continue;
                    }

                    // update color
                    var gageElem = anim.gage.GetChild(anim.currentValue);
                    var background = gageElem.transform.FindChild("Background");
                    var bgImage = background.GetComponent<RawImage>();
                    var statFactor = (float)anim.currentValue / _gameSetings.MaxStat;
                    var color = Color.Lerp(GageLow, GageHigh, statFactor);
                    bgImage.color = color;

                    anim.currentValue++;
                    anim.timer = 0.0f;
                }
                else
                    anim.timer += Time.deltaTime;
            }

            foreach (var anim in _gageAnimsToRemove)
                context.gageAnims.Remove(anim);
        }
    }
}
