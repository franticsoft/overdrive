﻿using UnityEditor;
using UnityEngine;
using System.IO;

namespace frantic
{
    [CustomEditor(typeof(TrackSelector))]
    public class TrackSelectorEditor : Editor
    {
        string[] _scenes;

        void OnEnable()
        {
            _scenes = SceneInspector.Init();
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var selector = target as TrackSelector;
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("+"))
            {
                Undo.RecordObject(selector, "Add Track");
                selector.AddTrack();
            }                
            if(selector.Tracks != null && selector.Tracks.Length > 0)
            {
                if (GUILayout.Button("-"))
                {
                    Undo.RecordObject(selector, "Add Track");
                    selector.RemoveTrack();
                }
            }
            GUILayout.EndHorizontal();

            if (selector.Tracks != null)
            {
                for (int i = 0; i < selector.Tracks.Length; ++i)
                {
                    selector.Tracks[i].Scene = SceneInspector.EditSceneProperty(selector, "Scene", _scenes, selector.Tracks[i].Scene);

                    //selector.Tracks[i].Track = EditorGUILayout.ObjectField(selector.Tracks[i].Track, typeof(GameObject), false) as GameObject;
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("+"))
                    {
                        Undo.RecordObject(selector, "Add Sub Track");
                        selector.AddSubTrack(i);
                    }
                        
                    if (selector.Tracks[i].SubTracks != null && selector.Tracks[i].SubTracks.Length > 0)
                    {
                        if (GUILayout.Button("-"))
                        {
                            Undo.RecordObject(selector, "Remove Sub Track");
                            selector.RemoveSubTrack(i);
                        }                            
                    }
                    GUILayout.EndHorizontal();

                    if (selector.Tracks[i].SubTracks != null)
                    {
                        for (int j = 0; j < selector.Tracks[i].SubTracks.Length; ++j)
                        {
                            var subTrack = EditorGUILayout.ObjectField(selector.Tracks[i].SubTracks[j], typeof(GameObject), false) as GameObject;
                            if(!ReferenceEquals(subTrack, selector.Tracks[i].SubTracks[j]))
                            {
                                Undo.RecordObject(selector, "Set Sub Track");
                                selector.Tracks[i].SubTracks[j] = subTrack;
                            }
                        }                           
                    }

                    selector.Tracks[i].PanelTexture = EditorGUILayout.ObjectField(selector.Tracks[i].PanelTexture, typeof(Texture), false) as Texture;
                    selector.Tracks[i].TrackName = EditorGUILayout.TextField("TrackName", selector.Tracks[i].TrackName);
                }
            }            
        }
    }
}

