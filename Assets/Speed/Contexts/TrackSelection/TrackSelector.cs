﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System;
using UnityEngine.Networking.NetworkSystem;

namespace frantic
{
    [Serializable]
    public class TrackInfo
    {
        public GameObject[] SubTracks;
        public string Scene;
        public Texture PanelTexture;
        public string TrackName;
    }

    public class TrackSelector : MonoBehaviour
    {
        public RawImage TrackPanel;
        public Text TrackName;

        [HideInInspector]
        public TrackInfo[] Tracks;        

        bool _trackSelected;
        GameSettings _gameSettings;
        GameState _gameState;
        FranticNetworkManager _manager;
        Transform _canvas;

        void Start()
        {
            _manager = NetworkManager.singleton as FranticNetworkManager;
            _manager.TrackSelector = this;

            _gameSettings = _manager.GetComponent<GameSettings>();
            _gameState = _manager.GetComponent<GameState>();
            _canvas = transform.FindChild("Canvas");

            var canvas = _canvas.GetComponent<Canvas>();
            canvas.worldCamera = _manager.MainMenu.MenuCamera;

            var navBar = Instantiate(_gameSettings.NavigationBar).GetComponent<NavigationBar>();
            navBar.transform.SetParent(_canvas, false);
            navBar.SetConfiguration(NavigationBar.ConfigType.Default);

            UpdateUI();
        }

        float _inputAcceptTimer = -1.0f;
        AsyncOperation _shipSelectionSceneLoad;
        void Update()
        {
            if (_shipSelectionSceneLoad != null)
            {
                if (_shipSelectionSceneLoad.isDone)
                {
                    _shipSelectionSceneLoad = null;

                    if (_manager.IsSplitScreenGame)
                    {
                        var players = GameObject.FindObjectsOfType<FranticLobbyPlayer>();
                        foreach (var player in players)
                            player.InitForSplitScreen();
                    }                        

                    enabled = false;
                    SceneManager.UnloadScene(_manager.TrackSelectionScene);
                }
                return;
            }

            var manager = NetworkManager.singleton as FranticNetworkManager;
            if (_trackSelected)
            {
                if (NetworkServer.active && NetworkManager.networkSceneName == manager.offlineScene)
                {
                    if (manager.AreAllPlayersReady())
                    {
                        manager.ServerStartGame(Tracks[_gameState.SelectedTrackIndex].Scene);
                    }
                }
                return;
            }

            // Input
            var lateral = Input.GetAxis("P1_Horizontal");
            if (!MathUtils.IsZero(lateral))
            {
                if (_inputAcceptTimer < 0.0f)
                {
                    Scroll(lateral > 0.0f);
                    _inputAcceptTimer = 0.0f;
                }
                else
                {
                    _inputAcceptTimer += Time.deltaTime;
                    if (_inputAcceptTimer > _gameSettings.InputAcceptDuration)
                        _inputAcceptTimer = -1.0f;
                }
            }
            else
            {
                _inputAcceptTimer = -1.0f;
            }

            if (Utils.IsInputAccept())
            {
                _manager.ScreenDistortion.Distort();
                if (!manager.IsSplitScreenGame && !NetworkServer.active)
                    manager.StartSinglePlayer();

                _trackSelected = true;
            }
            else if (Utils.IsInputCancel())
            {
                _manager.ScreenDistortion.Distort();
                ExitTrackSelection();
            }
        }

        public void AddTrack()
        {
            if (Tracks == null)
                Tracks = new TrackInfo[1];
            else            
                Array.Resize(ref Tracks, Tracks.Length + 1);

            Tracks[Tracks.Length - 1] = new TrackInfo();
        }

        public void RemoveTrack()
        {
            Array.Resize(ref Tracks, Tracks.Length - 1);
        }        

        public void AddSubTrack(int trackIndex)
        {
            if (Tracks[trackIndex].SubTracks == null)
                Tracks[trackIndex].SubTracks = new GameObject[1];
            else
                Array.Resize(ref Tracks[trackIndex].SubTracks, Tracks[trackIndex].SubTracks.Length + 1);
        }

        public void RemoveSubTrack(int trackIndex)
        {
            Array.Resize(ref Tracks[trackIndex].SubTracks, Tracks[trackIndex].SubTracks.Length - 1);
        }

        void Scroll(bool right)
        {
            if (right)
                _gameState.SelectedTrackIndex = (_gameState.SelectedTrackIndex + 1) % Tracks.Length;
            else
            {
                if (_gameState.SelectedTrackIndex > 0)
                    _gameState.SelectedTrackIndex--;
                else
                    _gameState.SelectedTrackIndex = Tracks.Length - 1;
            }

            UpdateUI();
        }

        void UpdateUI()
        {
            TrackPanel.texture = Tracks[_gameState.SelectedTrackIndex].PanelTexture;
            TrackName.text = Tracks[_gameState.SelectedTrackIndex].TrackName;

            var trackRenderer = GetComponentInChildren<TrackRenderer>();
            if(trackRenderer)
                trackRenderer.SetTrack(Tracks[_gameState.SelectedTrackIndex].SubTracks);
        }

        public void ExitTrackSelection()
        {
            _gameState.GameContext = GameState.Context.ShipSelection;
            //var dof = MainMenu.Instance.MenuCamera.GetComponent<UnityStandardAssets.ImageEffects.DepthOfField>();
            //dof.focalLength = _manager.ShipSelectionFocalDist;
            _shipSelectionSceneLoad = SceneManager.LoadSceneAsync(_manager.ShipSelectionScene, LoadSceneMode.Additive);

            if (NetworkServer.active)            
                NetworkServer.SendToAll((short)FranticMessage.ExitTrackSelection, new IntegerMessage());
        }
    }
}

