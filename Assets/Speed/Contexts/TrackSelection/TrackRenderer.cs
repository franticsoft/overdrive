﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class TrackRenderer : MonoBehaviour
    {
        public Material TrackMaterial;
        public int Resolution = 10;
        public float LineWidth = 0.2f;
        public float Size = 10.0f;

        LineRenderer _shape;

        public void SetTrack(GameObject[] subTracks)
        {
            _shape = gameObject.GetComponent<LineRenderer>();
            if (_shape == null)
                _shape = gameObject.AddComponent<LineRenderer>();

            _shape.sharedMaterial = TrackMaterial;
            _shape.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            _shape.receiveShadows = false;
            _shape.useWorldSpace = false;
            _shape.SetWidth(LineWidth, LineWidth);

            var pointsPerCurve = Resolution;
            int numVertices = 0;
            foreach(var subTrack in subTracks)
            {
                var data = subTrack.GetComponent<BezierPathData>();
                numVertices += (data.Length * (pointsPerCurve - 1)) + 1;
            }                
            //var numVertices = (data.Length * (pointsPerCurve - 1)) + 1;
            var vertices = new Vector3[numVertices];
            _shape.SetVertexCount(numVertices);

            int vertex = 0;
            var min = Vector3.one * float.MaxValue;
            var max = Vector3.one * float.MinValue;
            foreach (var subTrack in subTracks)
            {
                var data = subTrack.GetComponent<BezierPathData>();
                var trackTransform = subTrack.GetComponent<Transform>();
                for (int i = 0; i < data.Length; ++i)
                {
                    var curve = data[i];

                    for (int j = 0; j < pointsPerCurve; ++j)
                    {
                        if (j == 0 && i > 0) // skip first point of curves except first one
                            continue;

                        var t = (1.0f / (pointsPerCurve - 1));
                        Debug.Assert(vertex < numVertices);
                        //var pos = curve.GetPoint(t * j);
                        var pos = trackTransform.TransformPoint(curve.GetPoint(t * j));
                        vertices[vertex++] = pos;

                        if (pos.x < min.x) min.x = pos.x;
                        if (pos.x > max.x) max.x = pos.x;
                        if (pos.y < min.y) min.y = pos.y;
                        if (pos.y > max.y) max.y = pos.y;
                        if (pos.z < min.z) min.z = pos.z;
                        if (pos.z > max.z) max.z = pos.z;
                    }
                }
            }             

            Debug.Assert(vertex == numVertices);
            var size = new Vector3(Mathf.Abs(max.x - min.x), Mathf.Abs(max.y - min.y), Mathf.Abs(max.z - min.z));            

            // determine aspect ratio
            var sizeFactor = Vector3.one;
            if (size.x < size.z)
                sizeFactor.x = size.x / size.z;
            else
                sizeFactor.z = size.z / size.x;

            sizeFactor.y = size.y / Mathf.Max(size.x, size.z);

            var clampedSize = new Vector3(Mathf.Clamp(size.x, 1.0f, float.MaxValue), Mathf.Clamp(size.y, 1.0f, float.MaxValue), Mathf.Clamp(size.z, 1.0f, float.MaxValue));
            var normalizedSize = size.DivideBy(clampedSize);
            for (int i=0; i<numVertices; ++i)
            {
                var normalized = (vertices[i] - min).DivideBy(clampedSize);
                var centered = (normalized * 2.0f) - normalizedSize;
                var pos = centered.MultiplyBy(sizeFactor) * (Size / 2.0f);
                _shape.SetPosition(i, pos);
            }
        }
    }
}
