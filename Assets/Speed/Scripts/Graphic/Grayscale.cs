using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
    [ExecuteInEditMode]
    [AddComponentMenu("Image Effects/Color Adjustments/Grayscale")]
    public class Grayscale : ImageEffectBase {
        public Texture  textureRamp;
        public float    rampOffset;
        [Range(0.0f, 1.0f)]
        public float    GrayFactor;

        [Range(0.0f, 1.0f)]
        public float FadeFactor;

        // Called by camera to apply image effect
        void OnRenderImage (RenderTexture source, RenderTexture destination)
        {
            material.SetTexture("_RampTex", textureRamp);
            material.SetFloat("_RampOffset", rampOffset);
            material.SetFloat("_GrayFactor", GrayFactor);
            material.SetFloat("_FadeFactor", FadeFactor);
            Graphics.Blit (source, destination, material, 0);
        }
    }
}
