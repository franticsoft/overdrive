using System;
using UnityEngine;
using UnityEngine.Networking;

namespace frantic
{
    [AddComponentMenu("Graphic/ScreenDistortion")]
    public class ScreenDistortion : UnityStandardAssets.ImageEffects.ImageEffectBase
    {
        public Texture DistortionMap;

        public FloatRange DistortionFactor = new FloatRange(0.2f, 0.8f);
        public float DistortionSpeed = 0.3f;
        public float DistortionDuration = 0.02f;
        public int RandomFactor = 3;

        protected override void Start()
        {
            base.Start();
            var manager = NetworkManager.singleton as FranticNetworkManager;
            manager.ScreenDistortion = this;
        }

        public void Distort()
        {
            _targetDistortion = UnityEngine.Random.Range(DistortionFactor.Min, DistortionFactor.Max);
        }

        // Called by camera to apply image effect
        void OnRenderImage (RenderTexture source, RenderTexture destination)
        {
            material.SetTexture("_DistortionMap", DistortionMap);            
            material.SetFloat("_DistortionFactor", _targetDistortion);
            material.SetFloat("_DistortionOffset", _targetDistortion * UnityEngine.Random.Range(0.0f, 100.0f));
            Graphics.Blit (source, destination, material);
        }

        //float _distortionAngle = 0.0f;
        //bool _set = false;
        float _targetDistortion = 0.0f;
        void Update()
        {
            //var amplitude = Mathf.Sin(_distortionAngle * Mathf.Deg2Rad);
            //if (amplitude > 0.9f)
            //{
            //    if (!_set)
            //    {
            //        int r = UnityEngine.Random.Range(0, RandomFactor);
            //        if (r == 0)
            //            _targetDistortion = UnityEngine.Random.Range(DistortionFactor.Min, DistortionFactor.Max);

            //        _set = true;
            //    }
            //}
            //else
            //    _set = false;

            var factor = Time.deltaTime / DistortionDuration;
            _targetDistortion = Mathf.Lerp(_targetDistortion, 0.0f, factor);
            //_distortionAngle += Time.deltaTime * DistortionSpeed;
        }
    }
}
