﻿
using UnityEngine;

namespace frantic
{
    [RequireComponent(typeof(Camera))]
    public class EffectsRenderer : MonoBehaviour
    {
        public Shader DepthRenderingShader;
        public LayerMask DepthRenderingCullingMask;
        public Shader FullScreenShader;        
        public GameObject FullScreenQuad;

        Camera _camera;
        Camera _depthCamera;
        void Start()
        {
            // Regen depth information from Scene rendering pass
            _camera = GetComponent<Camera>();
            var camCopy = new GameObject();
            camCopy.hideFlags = HideFlags.HideAndDontSave;
            _depthCamera = camCopy.AddComponent<Camera>();
            _depthCamera.enabled = false;            
            _depthCamera.CopyFrom(_camera);
            _depthCamera.cullingMask = DepthRenderingCullingMask;
            _depthCamera.clearFlags = CameraClearFlags.Depth;

            // Setup rendering of SceneRT into a full screen quad
            var quad = Instantiate(FullScreenQuad);
            var material = new Material(FullScreenShader);
            var parentCamera = transform.parent.GetComponent<Camera>();
            material.mainTexture = parentCamera.targetTexture;
            quad.GetComponent<MeshRenderer>().sharedMaterial = material;
            quad.transform.SetParent(transform, false); // make the quad child of the camera so it's always rendered (otherwise frustum test kicks in)            
        }

        void OnPreRender()
        {
            if (_camera == null || _depthCamera == null)
                return;

            _depthCamera.transform.position = _camera.transform.position;
            _depthCamera.transform.rotation = _camera.transform.rotation;
            _depthCamera.fieldOfView = _camera.fieldOfView;
            _depthCamera.RenderWithShader(DepthRenderingShader, "RenderType");            
        }
    }    
}

