using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
    [ExecuteInEditMode]
    [AddComponentMenu("Graphic/Transition")]
    public class Transition : ImageEffectBase 
    {
        public Texture  textureRamp;
        public float    rampOffset;

        [Range(0.0f, 1.0f)]
        public float    fadeFactor;

        [Range(0.0f, 1.0f)]
        public float    grayFactor;

        // Called by camera to apply image effect
        void OnRenderImage (RenderTexture source, RenderTexture destination)
        {
            material.SetTexture("_RampTex", textureRamp);
            material.SetFloat("_RampOffset", rampOffset);
            material.SetFloat("_FadeFactor", fadeFactor);
            material.SetFloat("_GrayFactor", grayFactor);
            Graphics.Blit (source, destination, material);
        }
    }
}
