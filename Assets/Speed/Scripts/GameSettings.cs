﻿using UnityEngine;

namespace frantic
{
    public class GameSettings : MonoBehaviour
    {
        [Header("Game-play")]
        public int MaxStat = 10;
        public float ShootingFrequency = .4f;
        public float CollisionResponseDuration = 2.0f;
        public float MissileHitFactor = 0.8f;
        public float PowerUpSpawnFrequency = 3.0f;
        public float CollisionDecel = 1.0f;
        public float CollisionDecelDuration = 0.3f;
        public int InitialInventoryItemCount = 2;
        public int MaxLaps = 5;
        public RenderTexture SplitScreenTopTarget;
        public RenderTexture SplitScreenBottomTarget;
        public GameObject RaceFinishedPanel;
        public GameObject CountDownPanel;
        public float BulletsFrequency = 0.2f;
        public float BulletMaxSpeed = 600.0f;
        public float BulletAccel = 20.0f;
        public float BulletLife = 3.0f;
        public float CameraAnticipationAngle = 30.0f;
        public float CameraAnticipationDuration = 0.5f;
        public float CameraAnticipationUpOffset = 1.0f;
        public float MiniFlyDampDuration = 0.1f;
        public float MissileDamage = 20.0f;
        public float BulletDamage = 5.0f;
        public float LookingUpCameraTransitionDuration = 0.3f;
        public float AirRotationAccelFactor = 0.2f;
        public float AIPercentVariation = 0.25f;

        [Header("UI")]
        public int MiniMapResolution = 10;
        public GameObject NavigationBar;

        [Header("Menu")]
        public float TransitionDuration = 0.4f;
        public float TrackTransitionDuration = 0.4f;
        public float InputAcceptDuration = 0.4f;

        [Header("Audio")]
        public AudioClip[] MusicClips;
        public AudioClip[] InGameMusicClips;

        [Header("FX")]
        public GameObject WireframePulse;
    }
}

