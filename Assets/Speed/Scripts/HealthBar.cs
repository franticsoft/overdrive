﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class HealthBar : MonoBehaviour
    {
        public GameObject FG;

        RectTransform _fgRect;

        public void SetProgress(float progress)
        {
            var s = _fgRect.localScale;
            s.x = Mathf.Clamp01(progress);
            _fgRect.localScale = s;
        }

        void Start()
        {
            _fgRect = FG.GetComponent<RectTransform>();
        }
    }
}

