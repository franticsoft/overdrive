﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BezierPoint
{
    public Vector3 Position;
    public float Banking;
    public bool FlatBanking;
    public bool JumpSpot;
}
