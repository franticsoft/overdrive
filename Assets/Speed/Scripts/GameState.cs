﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class GameState : MonoBehaviour
    {
        public int SelectedTrackIndex;
        public int[] SelectedShipIndex;
        public GameObject[] SelectedShip;
        public Context GameContext;

        public enum Context
        {
            MainMenu,
            ShipSelection,
            TrackSelection,
            InGame
        }

        void Start()
        {
            GameContext = Context.MainMenu;
            SelectedShipIndex = new int[2];
            SelectedShip = new GameObject[2];
        }
    }
}

