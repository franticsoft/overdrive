﻿using UnityEngine;
using System;

namespace frantic
{
    public class TrackDecorator : MonoBehaviour
    {
        [Serializable]
        public enum Type
        {
            BoostLeft,
            BoostRight,
            RoadPanelLeft,
            RoadPanelRight,
            FenceLeft,
            FenceRight
        }

        public Type CurrentType = Type.BoostLeft;
    }
}

