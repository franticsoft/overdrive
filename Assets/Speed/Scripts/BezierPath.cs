﻿using UnityEngine;
using System.Collections.Generic;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace frantic
{
    [ExecuteInEditMode]
    public class BezierPath : MonoBehaviour
    {
        public GameObject NextTrack;

        [HideInInspector]
        public GameObject[] Meshes;

        [HideInInspector]
        public bool IsLooping;

        public BezierPathData Curves;

        [HideInInspector]
        public int TrackIndex { get; set; }

        public int StartingCurveIndex = 0;

        struct CurveFactors
        {
            public float[] f;
        }

        struct ChildCurveFactors
        {
            public CurveFactors[] _factors;
        }

        struct TurnIndicatorsInfo
        {
            public GameObject OriginalMesh;
            public CurveFactors CurveFactors;
        }

        CurveFactors[] _curveFactors;
        ChildCurveFactors[] _childCurveFactors;
        TurnIndicatorsInfo[] _turnIndicatorInfo;

        class DecoratorInfo
        {
            public GameObject Resource;
            public CurveFactors CurveFactors;
        }

        Dictionary<TrackDecorator.Type, DecoratorInfo> _decorators;
        GameObject _previousTrack;
        Vector3 _forward = Vector3.forward;        

        [SerializeField, HideInInspector]
        bool _alignedToNextTrack;
        [SerializeField, HideInInspector]
        bool _alignedToPreviousTrack;
        [SerializeField, HideInInspector]
        float _boostLength;

        public GameObject GetPreviousTrack() { return _previousTrack; }
        public bool IsAlignedToNextTrack() { return NextTrack != null && _alignedToNextTrack; }
        public bool IsAlignedToPreviousTrack() { return _previousTrack != null && _alignedToPreviousTrack; }
        public float GetBoostLength() { return _boostLength; }

        void Awake()
        {
            if (Curves == null)
                Reset();

            InitCurveFactors();
        }

        void Reset()
        {
            Clear();
        }

        void Start()
        {
            if (NextTrack != null)
            {
                var nextPath = NextTrack.GetComponent<BezierPath>();
                nextPath._previousTrack = this.gameObject;
            }

#if UNITY_EDITOR
            int index = 0;
            foreach(Transform t in transform)
            {
                t.GetComponent<TrackSegment>().IndexInTrack = index++;
            }
#endif
        }

        void AlignTo(BezierPath path)
        {
            var targetPoint = path.Curves[0].Points[0].Position;
            var targetTangentIn = path.Curves[0].Points[1].Position;
            if (!ReferenceEquals(path, this))
            {
                var tr = path.gameObject.transform;
                targetPoint = tr.TransformPoint(targetPoint);
                targetTangentIn = tr.TransformPoint(targetTangentIn);

                // to local
                targetPoint = transform.InverseTransformPoint(targetPoint);
                targetTangentIn = transform.InverseTransformPoint(targetTangentIn);
            }

            var lastCurve = Curves[Curves.Length - 1];
            lastCurve.Points[3].Position = targetPoint;
            var toFirstTangentIn = targetTangentIn - targetPoint;
            lastCurve.Points[2].Position = lastCurve.Points[3].Position - toFirstTangentIn;
            UpdateMesh(Curves.Length - 1);
        }

        public void Clear()
        {
            StartingCurveIndex = 0;
            Curves = null;            
            _curveFactors = null;
            _childCurveFactors = null;

            IsLooping = false;

            while (transform.childCount > 0)
                GameObject.DestroyImmediate(transform.GetChild(0).gameObject);

            if(NextTrack != null)
            {
                BreakFromNext();
                DestroyImmediate(NextTrack);
                NextTrack = null;
            }

            if (Meshes != null && Meshes.Length > 0 && Meshes[0] != null)
            {
                // add mesh
                AddSegment(0, transform.position, transform.rotation);

                // initialize curve
                var step = Meshes[0].GetComponent<MeshFilter>().sharedMesh.bounds.size.z / 3.0f;
                var curvesGOName = gameObject.name + "_data";
                var curvesGO = GameObject.Find(curvesGOName);
                if (curvesGO)
                    DestroyImmediate(curvesGO);
                curvesGO = new GameObject(curvesGOName);
                Curves = curvesGO.AddComponent<BezierPathData>();
                var newCurve = new BezierCurve()
                {
                    Points = new BezierPoint[]
                                            {
                                                new BezierPoint() { Position = _forward * step*0.0f },
                                                new BezierPoint() { Position = _forward * step*1.0f },
                                                new BezierPoint() { Position = _forward * step*2.0f },
                                                new BezierPoint() { Position = _forward * step*3.0f }
                                            },

                    InitialUp = Vector3.up
                };
                Curves.SetCurve(newCurve, 0);

                InitCurveFactors();
            }
        }

        public void AddPiece(int meshIndex)
        {
            if (Curves == null || Curves.Length < 1)
                return;

            var lastCurve = Curves[Curves.Length - 1];
            var lastControlPoint = lastCurve.Points[3];
            var curveDir = lastCurve.GetTangent(1.0f).normalized;

            // add mesh
            var meshPos = lastControlPoint.Position;
            AddSegment(meshIndex, transform.TransformPoint(meshPos), Quaternion.identity);

            // initialize curve
            var step = Meshes[meshIndex].GetComponent<MeshFilter>().sharedMesh.bounds.size.z / 3.0f;
            var forceZeroBanking = MathUtils.Equals(Mathf.Abs(lastControlPoint.Banking), 360.0f);
            var banking = forceZeroBanking ? 0.0f : lastControlPoint.Banking;
            var newCurve = new BezierCurve()
            {
                Points = new BezierPoint[] 
                {
                    new BezierPoint() { Position = lastControlPoint.Position, Banking = banking },
                    new BezierPoint() { Position = lastControlPoint.Position + curveDir*step, Banking = banking },
                    new BezierPoint() { Position = lastControlPoint.Position + curveDir*step*2.0f, Banking = banking },
                    new BezierPoint() { Position = lastControlPoint.Position + curveDir*step*3.0f, Banking = banking }
                },

                InitialUp = lastCurve.GetUp(1.0f)
            };
            Curves.SetCurve(newCurve, Curves.Length);

            UpdateMesh(Curves.Length - 1);
        }

        void AddSegment(int meshIndex, Vector3 position, Quaternion rotation)
        {
            var meshGO = Meshes[meshIndex];
            var mesh = GameObject.Instantiate(meshGO, position, rotation) as GameObject;
            var meshCopy = Instantiate(meshGO.GetComponent<MeshFilter>().sharedMesh);
            mesh.GetComponent<MeshFilter>().sharedMesh = meshCopy;
            mesh.transform.parent = transform;
            mesh.transform.localScale = Vector3.one;
            var trackSegment = mesh.GetComponent<TrackSegment>();
            if (trackSegment == null)
                trackSegment = mesh.AddComponent<TrackSegment>();
            trackSegment.MeshIndex = meshIndex;

            // Initialize children
            for (int i = 0; i < mesh.transform.childCount; ++i)
            {
                var childMF = meshGO.transform.GetChild(i).GetComponent<MeshFilter>();
                var childMeshCopy = Instantiate(childMF.sharedMesh);
                var child = mesh.transform.GetChild(i);
                var mf = child.GetComponent<MeshFilter>();
                mf.sharedMesh = childMeshCopy;
                var oldTransform = child.transform.GetLocalMatrix();
                child.transform.ResetLocally();
                var vertices = mf.sharedMesh.vertices;
                for (int j = 0; j < mf.sharedMesh.vertexCount; ++j)
                    vertices[j] = oldTransform.MultiplyPoint3x4(vertices[j]);

                mf.sharedMesh.vertices = vertices;
                mf.sharedMesh.RecalculateBounds();
                mf.sharedMesh.RecalculateNormals();
            }

            if(_turnIndicatorInfo != null)
                Array.Resize(ref _turnIndicatorInfo, _turnIndicatorInfo.Length + 1);
        }

        public void MakeLooping()
        {
            AlignTo(this);
            IsLooping = true;
        }

        public void BreakLoop()
        {
            IsLooping = false;
        }

        public void AlignToNext()
        {
            var nextPath = NextTrack.GetComponent<BezierPath>();
            AlignTo(nextPath);
            _alignedToNextTrack = true;
            nextPath._alignedToPreviousTrack = true;
            nextPath._previousTrack = this.gameObject;
        }

        public void BreakFromNext()
        {
            var nextPath = NextTrack.GetComponent<BezierPath>();
            _alignedToNextTrack = false;
            nextPath._alignedToPreviousTrack = false;
            nextPath._previousTrack = null;
        }

        public void UpdateMesh(int curveIndex)
        {
            var segment = transform.GetChild(curveIndex).GetComponent<TrackSegment>();
            var meshIndex = segment.MeshIndex;
            var meshGO = Meshes[meshIndex];
            var mesh = transform.GetChild(curveIndex).GetComponent<MeshFilter>().sharedMesh;
            var mr = transform.GetChild(curveIndex).GetComponent<MeshRenderer>();
            mr.sharedMaterials = meshGO.GetComponent<MeshRenderer>().sharedMaterials;

            if (meshGO.GetComponent<MeshFilter>().sharedMesh.vertexCount != _curveFactors[meshIndex].f.Length)
                UpdateCurveFactors(meshIndex);

            var segmentSize = meshGO.GetComponent<MeshFilter>().sharedMesh.bounds.size.z;
            UpdateMesh(ref _curveFactors[meshIndex], curveIndex, meshGO, mesh, segmentSize);
            int nonDecoratorChildIndex = 0;
            var decorators = new List<Transform>();
            Transform turnIndicator = null;
            for (int i = 0; i < segment.transform.childCount; ++i)
            {
                var child = segment.transform.GetChild(i);
                if (child.GetComponent<ReflectionProbe>() != null)
                    continue;

                if (child.GetComponent<TrackDecorator>())
                {
                    decorators.Add(child);
                    continue;
                }

                if (child.name == "TurnIndicator")
                {
                    Debug.Assert(turnIndicator == null, "Multiple turn indicators detected");
                    turnIndicator = child;
                    continue;
                }

                var childMesh = child.GetComponent<MeshFilter>().sharedMesh;
                var childMR = child.GetComponent<MeshRenderer>();
                var childMeshGO = meshGO.transform.FindChild(child.name).gameObject;
                var originalMR = childMeshGO.GetComponent<MeshRenderer>();
                childMR.sharedMaterials = originalMR.sharedMaterials;
                childMR.shadowCastingMode = originalMR.shadowCastingMode;

                if (_childCurveFactors[meshIndex]._factors == null || nonDecoratorChildIndex >= _childCurveFactors[meshIndex]._factors.Length)
                    UpdateCurveFactors(meshIndex);

                UpdateMesh(ref _childCurveFactors[meshIndex]._factors[nonDecoratorChildIndex], curveIndex, childMeshGO, childMesh, segmentSize);
                ++nonDecoratorChildIndex;
            }

            // decorators
            foreach (var decorator in decorators)
            {
                var td = decorator.GetComponent<TrackDecorator>();
                var go = _decorators[td.CurrentType].Resource;
                var m = decorator.GetComponent<MeshFilter>().sharedMesh;
                UpdateMesh(ref _decorators[td.CurrentType].CurveFactors, curveIndex, go, m, segmentSize);
            }           

            // turn indicator
            if (turnIndicator != null)
            {
                if (_turnIndicatorInfo[curveIndex].OriginalMesh == null)
                    CreateTurnIndicatorOriginalMesh(segment, curveIndex, segmentSize);

                var go = _turnIndicatorInfo[curveIndex].OriginalMesh;
                var m = turnIndicator.GetComponent<MeshFilter>().sharedMesh;
                if(_turnIndicatorInfo[curveIndex].CurveFactors.f == null)
                    UpdateCurveFactors(ref _turnIndicatorInfo[curveIndex].CurveFactors, go.gameObject, segmentSize);

                UpdateMesh(ref _turnIndicatorInfo[curveIndex].CurveFactors, curveIndex, go, m, segmentSize);
            }

            Curves[curveIndex].UpdateInfos();
        }

        private void UpdateMesh(ref CurveFactors factors, int curveIndex, GameObject meshGO, Mesh mesh, float segmentSize)
        {
            var originalMesh = meshGO.GetComponent<MeshFilter>().sharedMesh;
            var originalVertices = originalMesh.vertices;

            var curve = Curves[curveIndex];
            var vertices = mesh.vertices;            
            var localOffset = transform.GetChild(curveIndex).localPosition;
            var vertexCount = originalMesh.vertexCount;
            for (int j = 0; j < vertexCount; ++j)
            {
                var curveFactor = factors.f[j];
                var oldCurvePos = new Vector3(0.0f, 0.0f, curveFactor * segmentSize);
                var newCurvePos = curve.GetPoint(curveFactor) - localOffset;
                var deltaPos = newCurvePos - oldCurvePos;
                var curveDir = curve.GetTangent(curveFactor).normalized;

                var up = curve.GetUp(curveFactor);
                var curveBanking = curve.GetBanking(curveFactor);
                up = Quaternion.AngleAxis(curveBanking, curveDir) * up; // apply banking
                var lookAt = Quaternion.LookRotation(curveDir, up);

                var initialPos = meshGO.transform.TransformPoint(originalVertices[j]);                
                var rotated = lookAt * new Vector3(initialPos.x, initialPos.y, 0.0f);
                vertices[j] = rotated + deltaPos + new Vector3(0.0f, 0.0f, initialPos.z);
            }

            mesh.vertices = vertices;
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();            
        }

        public void UpdateMeshes()
        {
            for (int i = 0; i < transform.childCount; ++i)
            {
                UpdateSegment(transform.GetChild(i).GetComponent<TrackSegment>());
            }
        }

        public void RemoveLast()
        {
            if (IsAlignedToNextTrack())
                BreakFromNext();

            Curves.RemoveLastCurve();
            DestroyImmediate(transform.GetChild(transform.childCount - 1).gameObject);
            Array.Resize(ref _turnIndicatorInfo, _turnIndicatorInfo.Length - 1);
        }

        public void CreateNext()
        {
            var lastCurve = Curves.Length - 1;
            var lastPos = GetPoint(lastCurve, 1.0f);
            var lastTangent = GetTangent(lastCurve, 1.0f).normalized;

            var newTrack = new GameObject("New track");
            newTrack.transform.localScale = transform.localScale;
            newTrack.transform.position = lastPos;
            newTrack.transform.parent = transform.parent;
            var newPath = newTrack.AddComponent<BezierPath>();
            newPath._forward = lastTangent;
            newPath.Meshes = new GameObject[Meshes.Length];
            Array.Copy(Meshes, newPath.Meshes, Meshes.Length);
            newPath.Clear();
            newPath.UpdateMeshes();

            NextTrack = newTrack;
            AlignToNext();
        }

        void InitCurveFactors()
        {
            if (Meshes == null || Meshes.Length == 0)
                return;

            _curveFactors = new CurveFactors[Meshes.Length];
            _childCurveFactors = new ChildCurveFactors[Meshes.Length];
            for (int i = 0; i < Meshes.Length; ++i)
                UpdateCurveFactors(i);

            // initialize decorators
            if(Meshes[0] != null)
            {
                var segmentSize = Meshes[0].GetComponent<MeshFilter>().sharedMesh.bounds.size.z;
                _decorators = new Dictionary<TrackDecorator.Type, DecoratorInfo>();
                foreach (var t in Enum.GetValues(typeof(TrackDecorator.Type)))
                {
                    var type = (TrackDecorator.Type)t;
                    var resource = Resources.Load(type.ToString()) as GameObject;
                    CurveFactors factors = new CurveFactors();
                    UpdateCurveFactors(ref factors, resource, segmentSize);
                    _decorators[type] = new DecoratorInfo() { Resource = resource, CurveFactors = factors };
                }
            }

            // turn indicators
            _turnIndicatorInfo = new TurnIndicatorsInfo[Curves.Length];
        }

        private void UpdateCurveFactors(ref CurveFactors factors, GameObject meshGO, float segmentLength)
        {
            var originalMesh = meshGO.GetComponent<MeshFilter>().sharedMesh;
            var originalVertices = originalMesh.vertices;

            factors.f = new float[originalVertices.Length];
            for (int i = 0; i < originalVertices.Length; ++i)
            {
                var orig = meshGO.transform.TransformPoint(originalVertices[i]);
                factors.f[i] = orig.z / segmentLength;
            }
        }

        public void UpdateCurveFactors(int meshIndex)
        {
            var meshGO = Meshes[meshIndex];
            if (meshGO == null)
                return;

            var segmentSize = meshGO.GetComponent<MeshFilter>().sharedMesh.bounds.size.z;
            UpdateCurveFactors(ref _curveFactors[meshIndex], meshGO, segmentSize);
            if (meshGO.transform.childCount > 0)
            {
                _childCurveFactors[meshIndex]._factors = new CurveFactors[meshGO.transform.childCount];
                for (int i = 0; i < meshGO.transform.childCount; ++i)
                {
                    if(meshGO.transform.GetChild(i).GetComponent<MeshFilter>() != null)
                        UpdateCurveFactors(ref _childCurveFactors[meshIndex]._factors[i], meshGO.transform.GetChild(i).gameObject, segmentSize);
                }
            }
        }

        public void AddSegmentType()
        {
            if (Meshes == null)
            {
                Meshes = new GameObject[1];
                _curveFactors = new CurveFactors[1];
                _childCurveFactors = new ChildCurveFactors[1];
            }
            else
            {
                Array.Resize(ref Meshes, Meshes.Length + 1);
                Array.Resize(ref _curveFactors, _curveFactors.Length + 1);
                Array.Resize(ref _childCurveFactors, _childCurveFactors.Length + 1);
            }
        }

        public void RemoveSegmentType()
        {
            Array.Resize(ref Meshes, Meshes.Length - 1);
            Array.Resize(ref _curveFactors, _curveFactors.Length - 1);
            Array.Resize(ref _childCurveFactors, _childCurveFactors.Length - 1);
        }
        
        GameObject CreateDecorator(TrackDecorator.Type t, Transform parent)
        {
#if UNITY_EDITOR
            if (_decorators == null)
                InitCurveFactors();
#endif

            var decorator = Instantiate(_decorators[t].Resource);
            decorator.transform.parent = parent;
            var oldTransform = decorator.transform.GetLocalMatrix();
            decorator.transform.ResetLocally();

            var originalMF = _decorators[t].Resource.GetComponent<MeshFilter>();
            var meshCopy = Instantiate(originalMF.sharedMesh);
            var mf = decorator.GetComponent<MeshFilter>();
            mf.sharedMesh = meshCopy;
            var vertices = mf.sharedMesh.vertices;
            for (int j = 0; j < mf.sharedMesh.vertexCount; ++j)
                vertices[j] = oldTransform.MultiplyPoint3x4(vertices[j]);
            mf.sharedMesh.vertices = vertices;
            mf.sharedMesh.RecalculateBounds();
            mf.sharedMesh.RecalculateNormals();
            return decorator;
        }

        public float TurnIndicatorHeight = 0.3f;
        public float TurnIndicatorUVPerUnit = 2.0f;
        public float TurnIndicatorSegmentPerUnit = 2.0f;
        void CreateTurnIndicatorOriginalMesh(TrackSegment segment, int curveIndex, float trackSegmentSize)
        {
            var turnIndicator = new GameObject("TurnIndicator");
            turnIndicator.SetActive(false);
            turnIndicator.hideFlags = HideFlags.HideAndDontSave;
            var mf = turnIndicator.AddComponent<MeshFilter>();
            var mr = turnIndicator.AddComponent<MeshRenderer>();
            var indicatorMesh = new Mesh();
            var indicatorStart = trackSegmentSize * segment.TurnIndicatorMin;
            var indicatorEnd = trackSegmentSize * segment.TurnIndicatorMax;
            var indicatorSide = segment.TurnIndicador == TrackSegment.TurnIndicadorType.Right ? 1.0f : -1.0f;
            var segmentSize = 1.0f / TurnIndicatorSegmentPerUnit;
            var numSegments = (int)((indicatorEnd - indicatorStart) * TurnIndicatorSegmentPerUnit);
            var vertices = new Vector3[numSegments*4];
            var uvs = new Vector2[numSegments*4];
            var triangles = new int[numSegments * 6];
            var segmentStart = indicatorStart;
            var uvStart = 0.0f;
            var uvSize = segmentSize * TurnIndicatorUVPerUnit;
            for(int i=0; i<numSegments; ++i)
            {
                // vertices & uvs
                var idx = i * 4;
                vertices[idx+0] = new Vector3(indicatorSide, 0.0f, segmentStart); 
                vertices[idx+1] = new Vector3(indicatorSide, 0.0f, segmentStart+segmentSize); 
                vertices[idx+2] = new Vector3(indicatorSide, TurnIndicatorHeight, segmentStart+segmentSize); 
                vertices[idx+3] = new Vector3(indicatorSide, TurnIndicatorHeight, segmentStart);
                uvs[idx+0] = new Vector2(uvStart, 1.0f);
                uvs[idx+1] = new Vector2(uvStart - uvSize, 1.0f);
                uvs[idx+2] = new Vector2(uvStart - uvSize, 0.0f);
                uvs[idx+3] = new Vector2(uvStart, 0.0f);
                segmentStart += segmentSize;
                uvStart -= uvSize;

                // indices
                var tri = i * 6;
                triangles[tri + 0] = idx + 0;
                triangles[tri + 1] = idx + 1;
                triangles[tri + 2] = idx + 2;
                triangles[tri + 3] = idx + 0;
                triangles[tri + 4] = idx + 2;
                triangles[tri + 5] = idx + 3;
            }
            indicatorMesh.vertices = vertices;
            indicatorMesh.uv = uvs;
            indicatorMesh.triangles = triangles;
            // indicatorMesh.triangles = indicatorSide > 0.0f ? (new int[] { 0, 1, 2, 0, 2, 3 }) : (new int[] { 0, 3, 2, 0, 2, 1 }); // Use this if back face culling is active
            mf.mesh = indicatorMesh;

#if UNITY_EDITOR
            if (_turnIndicatorInfo == null)
                InitCurveFactors();
#endif

            _turnIndicatorInfo[curveIndex].OriginalMesh = turnIndicator;
            _turnIndicatorInfo[curveIndex].CurveFactors.f = null;
        }

        void CreateTurnIndicatorInstance(TrackSegment segment, int curveIndex)
        {
            var originalTurnIndicator = _turnIndicatorInfo[curveIndex].OriginalMesh;
            var indicatorInstance = Instantiate(originalTurnIndicator);
            indicatorInstance.SetActive(true);
            indicatorInstance.name = originalTurnIndicator.name;
            indicatorInstance.transform.parent = segment.transform;
            indicatorInstance.transform.ResetLocally();
            var originalMF = originalTurnIndicator.GetComponent<MeshFilter>();
            var meshCopy = Instantiate(originalMF.sharedMesh);
            indicatorInstance.GetComponent<MeshFilter>().sharedMesh = meshCopy;
#if UNITY_EDITOR
            var mat = AssetDatabase.LoadAssetAtPath("Assets/Speed/Materials/BoostMaterial.mat", typeof(Material));
            indicatorInstance.GetComponent<MeshRenderer>().sharedMaterial = mat as Material;
            var animator = indicatorInstance.AddComponent<Animator>();
            var controller = AssetDatabase.LoadAssetAtPath("Assets/Speed/Animations/Boost.controller", typeof(RuntimeAnimatorController));
            animator.runtimeAnimatorController = controller as RuntimeAnimatorController;
#endif
        }

        public void UpdateSegment(TrackSegment segment)
        {
            int curveIndex = 0;
            for (int i = 0; i < transform.childCount; ++i)
            {
                if (ReferenceEquals(transform.GetChild(i).gameObject, segment.gameObject))
                    break;
                ++curveIndex;
            }

            segment.transform.DestroyAllChildren();
            var meshGO = Meshes[segment.MeshIndex];

            // Init mesh
            var meshCopy = Instantiate(meshGO.GetComponent<MeshFilter>().sharedMesh);
            var mesh = transform.GetChild(curveIndex);
            mesh.GetComponent<MeshFilter>().sharedMesh = meshCopy;
            mesh.gameObject.layer = meshGO.layer;

            // Init decorators
            if (segment.Boost == TrackSegment.DecoratorType.Left || segment.Boost == TrackSegment.DecoratorType.Both)
                CreateDecorator(TrackDecorator.Type.BoostLeft, segment.gameObject.transform);

            if (segment.Boost == TrackSegment.DecoratorType.Right || segment.Boost == TrackSegment.DecoratorType.Both)
               CreateDecorator(TrackDecorator.Type.BoostRight, segment.gameObject.transform);

            if (segment.Fences == TrackSegment.DecoratorType.Left || segment.Fences == TrackSegment.DecoratorType.Both)
                CreateDecorator(TrackDecorator.Type.FenceLeft, segment.gameObject.transform);

            if (segment.Fences == TrackSegment.DecoratorType.Right || segment.Fences == TrackSegment.DecoratorType.Both)
                CreateDecorator(TrackDecorator.Type.FenceRight, segment.gameObject.transform);

            if (segment.Panels == TrackSegment.DecoratorType.Left || segment.Panels == TrackSegment.DecoratorType.Both)
                CreateDecorator(TrackDecorator.Type.RoadPanelLeft, segment.gameObject.transform);

            if (segment.Panels == TrackSegment.DecoratorType.Right || segment.Panels == TrackSegment.DecoratorType.Both)
                CreateDecorator(TrackDecorator.Type.RoadPanelRight, segment.gameObject.transform);

            // Turn indicators
            if (segment.TurnIndicador != TrackSegment.TurnIndicadorType.None)
            {
                var segmentSize = meshGO.GetComponent<MeshFilter>().sharedMesh.bounds.size.z;
                CreateTurnIndicatorOriginalMesh(segment, curveIndex, segmentSize);
                CreateTurnIndicatorInstance(segment, curveIndex);
            }

            // Init children
            for (int i = 0; i < meshGO.transform.childCount; ++i)
            {
                var childGO = meshGO.transform.GetChild(i).gameObject;
                var child = Instantiate(childGO);
                child.name = childGO.name;
                child.transform.parent = segment.gameObject.transform;
                var oldTransform = child.transform.GetLocalMatrix();
                child.transform.ResetLocally();
                if (child.GetComponent<MeshFilter>())
                {
                    var childOriginalMF = childGO.GetComponent<MeshFilter>();
                    var childMeshCopy = Instantiate(childOriginalMF.sharedMesh);
                    var mf = child.GetComponent<MeshFilter>();
                    mf.sharedMesh = childMeshCopy;
                    var vertices = mf.sharedMesh.vertices;
                    for (int j = 0; j < mf.sharedMesh.vertexCount; ++j)
                        vertices[j] = oldTransform.MultiplyPoint3x4(vertices[j]);
                    mf.sharedMesh.vertices = vertices;
                    mf.sharedMesh.RecalculateBounds();
                    mf.sharedMesh.RecalculateNormals();
                }
            }

            UpdateMesh(curveIndex);
        }

        public Vector3 GetPoint(int curveIndex, float t)
        {
            return transform.TransformPoint(Curves[curveIndex].GetPoint(t));
        }

        public Vector3 GetTangent(int curveIndex, float t)
        {
            return transform.TransformPoint(Curves[curveIndex].GetTangent(t)) - transform.position;
        }

        public Vector3 GetUp(int curveIndex, float t)
        {
            var up = Curves[curveIndex].GetUp(t);
            return transform.TransformPoint(up) - transform.position;
        }

        public float GetBanking(int curveIndex, float t)
        {
            return Curves[curveIndex].GetBanking(t);
        }

        public float GetFactor(int curveIndex, float dist)
        {
            var localDist = dist / transform.localScale.x;
            return Curves[curveIndex].GetFactor(localDist);
        }

        public float GetLength(int curveIndex)
        {
            return Curves[curveIndex].GetLength() * transform.localScale.x;
        }
    }
}

