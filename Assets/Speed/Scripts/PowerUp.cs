﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace frantic
{
    public class PowerUp : NetworkBehaviour 
    {
        public enum EType
        {
            None,
            Boost,
            Missile,            
            Mine,
            Shield,
        }

        public EType Type = EType.Boost;

        [SyncVar]
        public Vector3 RotationAxis;

        [HideInInspector]
        public TrackSegment TrackSegment;

        public override void OnStartServer()
        {
            //Console.Print(string.Format("PowerUp.OnStartServer({0})", netId.Value));
        }

        public override void OnStartClient()
        {
            //Console.Print(string.Format("PowerUp.OnStartClient({0}) - RotationAxis: {1}", netId.Value, RotationAxis));
            var billBoard = transform.GetChild(0).GetComponent<Billboard>();
            billBoard.RotationAxis = RotationAxis;
        }

        public override void OnNetworkDestroy()
        {
            base.OnNetworkDestroy();

            if (TrackSegment != null)
                TrackSegment.PowerUp = null;
        }
    }
}

