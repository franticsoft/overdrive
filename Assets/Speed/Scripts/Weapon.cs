﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class Weapon : MonoBehaviour
    {
        public Transform MuzzleLight;
        public Transform LeftCannon;
        public Transform RightCannon;
        public float Range = 20.0f;

        GameObject _light;
        GameObject _leftFlash;
        GameObject _rightFlash;
        GameObject _leftBeam;
        GameObject _rightBeam;
        bool _isShooting;

        Prefabs _prefabs;
        LevelManager _levelMgr;

        //public void StartShooting(Player player)
        //{
        //    Shoot(player, true);
        //}

        //public void StopShooting()
        //{
        //    Shoot(null, false);
        //}

        void Start()
        {
            //_levelMgr = GameObject.Find("LevelManager").GetComponent<LevelManager>();
            //_prefabs = _levelMgr.Prefabs;

            //_light = MuzzleLight.AddChild(_prefabs.MuzzleLight);
            //_leftFlash = LeftCannon.AddChild(_prefabs.MuzzleFlash);
            //_rightFlash = RightCannon.AddChild(_prefabs.MuzzleFlash);

            //_leftBeam = LeftCannon.AddChild(_prefabs.ElectricBeam);
            //_rightBeam = RightCannon.AddChild(_prefabs.ElectricBeam);

            //Shoot(null, false);
        }

        void Update()
        {
            //if (!_isShooting)
            //    return;

            //RaycastHit info;
            //var ray = new Ray(transform.position, transform.forward);
            //if (Physics.Raycast(ray, out info, Range))
            //{
            //    var parent = info.collider.gameObject.transform.parent;
            //    var parentGO = parent != null ? parent.gameObject : null;
            //    var ai = parentGO != null ? parentGO.GetComponent<BasicAI>() : null;
            //    if (ai != null)
            //    {
            //        var body = parentGO.transform.FindChild("Body");
            //        var explosion = Instantiate(_prefabs.Explosion, body.position, _prefabs.Explosion.transform.localRotation);
            //        Destroy(explosion, 2.0f);
            //        //Destroy(parentGO);
            //        ai.Respawn();
            //    }
            //    else
            //    {
            //        Sparks(info.point, info.normal);
            //    }
            //}
        }

        //void Shoot(Player player)
        //{
        //    //_light.SetActive(shoot);
        //    //_leftFlash.SetActive(shoot);
        //    //_rightFlash.SetActive(shoot);

        //    //_leftBeam.SetActive(shoot);
        //    //_rightBeam.SetActive(shoot);

        //    var cannon = Random.Range(0, 2) == 0 ? LeftCannon : RightCannon;
        //    var missile = _levelMgr.transform.AddChild(_prefabs.Missile);
        //    missile.GetComponent<Missile>().Init(player, cannon);

        //    //_isShooting = shoot;
        //}

        //void Sparks(Vector3 position, Vector3 normal)
        //{
        //    var sparks = Instantiate(_prefabs.Sparks, position, Quaternion.LookRotation(normal)) as GameObject;
        //    var particles = sparks.GetComponent<ParticleSystem>();
        //    Destroy(sparks, particles.duration);
        //}
    }
}
