﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace frantic
{
    public class PhysicBody : MonoBehaviour
    {
        public float Mass = 1.0f;

        [HideInInspector]
        public float Speed
        {
            get { return _speed; }
            set
            {
                _speed = value;
            }
        }

        [HideInInspector]
        public float CollisionResponseTimer = -1.0f;

        [HideInInspector]
        public Vector3 PostCollisionDir = Vector3.zero;

        [HideInInspector]
        public Vector3 Direction = Vector3.forward;

        List<PhysicBody> _processedCollisions = new List<PhysicBody>();
        PlayerSync _playerSync;
        GameSettings _gameSettings;

        float _speed;

        void Start()
        {
            CollisionResponseTimer = -1.0f;
            var isAI = transform.parent.GetComponent<BasicAI>() != null;
            if (!isAI)
                _playerSync = transform.parent.parent.GetComponent<PlayerSync>();

            _gameSettings = FranticNetworkManager.singleton.GetComponent<GameSettings>();
        }

        void FixedUpdate()
        {
            _processedCollisions.Clear();
        }

        void OnCollisionEnter(Collision collision)
        {
            HandleCollision(collision);
        }

        void OnCollisionStay(Collision collision)
        {
            //HandleCollision(collision);
        }

        void HandleCollision(Collision collision)
        {
            if (_playerSync == null || !_playerSync.isLocalPlayer)
                return;

            if (GetComponent<Rigidbody>() == null)
                return;

            var other = collision.gameObject.GetComponent<PhysicBody>();
            if (other == null)
            {
                var mine = collision.gameObject.GetComponent<Mine>();
                if (mine != null)
                {
                    var hover = GetComponent<Hover>();
                    if (hover.GetState() != Hover.State.MiniFly)
                    {
                        var pardon = (mine._netPardonPlayerWithID == (int)_playerSync.netId.Value);
                        if (!pardon)
                        {
                            Speed *= (1.0f - mine.SpeedLostFactor);
                            // TODO add small explosion
                            GetComponent<Hover>().Fly(mine.BurstFactor);
                            _playerSync.ClientDestroyNetObject(mine.netId.Value);
                        }
                        else
                        {
                            Console.Print(string.Format("HandleCollision - Mine {0} pardoning player {1}", mine.netId.Value, _playerSync.netId.Value));
                        }
                    }
                    return;
                }
                var powerup = collision.gameObject.GetComponent<PowerUp>();
                if (powerup != null)
                {
                    _playerSync.ClientObtainPowerUp(powerup);
                    return;
                }
                return;
            }

            //if (other._processedCollisions.FirstOrDefault(c => ReferenceEquals(c, this))
            //  || _processedCollisions.FirstOrDefault(c => ReferenceEquals(c, other)))
            //{
            //    return;
            //}

            //Console.Print(string.Format("Collision [this - [dir {0}][speed {1}], [other - [dir {2}][speed {3}]", Direction, Speed, other.Direction, other.Speed));

            //var collisionAxe = (other.transform.position - transform.position).normalized;
            //var tangentAxe = Vector3.Cross(transform.up, collisionAxe);

            //var velocity1 = Direction * Speed;
            //var movingTowardsCol1 = Vector3.Dot(velocity1.normalized, collisionAxe) > MathUtils.Epsilon;            
            //var colAxe1 = Vector3.Project(velocity1, collisionAxe);
            //var colComponent1 = colAxe1.magnitude * (movingTowardsCol1 ? 1.0f : -1.0f);
            //colAxe1.Normalize();
            //var tangentAxe1 = Vector3.Project(velocity1, tangentAxe);
            //var tangentComponent1 = tangentAxe1.magnitude;
            //tangentAxe1.Normalize();

            //var collisionAxe2 = -collisionAxe;
            //var velocity2 = other.Direction * other.Speed;
            //var movingTowardsCol2 = Vector3.Dot(velocity2.normalized, collisionAxe2) > MathUtils.Epsilon;
            //var colAxe2 = Vector3.Project(velocity2, collisionAxe2);
            //var colComponent2 = colAxe2.magnitude * (movingTowardsCol2 ? 1.0f : -1.0f);
            //colAxe2.Normalize();
            //var tangentAxe2 = Vector3.Project(velocity2, tangentAxe);
            //var tangentComponent2 = tangentAxe2.magnitude;
            //tangentAxe2.Normalize();      
            
            //if(movingTowardsCol1 && movingTowardsCol2)
            //{
            //    Debug.Assert(colComponent1 >= 0.0f);
            //    Debug.Assert(colComponent2 >= 0.0f);
            //    var newColComponent1 = colComponent1 - colComponent2;
            //    var newColComponent2 = colComponent2 - colComponent1;
            //    colComponent1 = newColComponent1;
            //    colComponent2 = newColComponent2;
            //}
            //else
            //{
            //    var newColComponent1 = movingTowardsCol1 ? colComponent1 : -colComponent2;
            //    var newColComponent2 = movingTowardsCol1 ? -colComponent1 : colComponent2;
            //    colComponent1 = newColComponent1;
            //    colComponent2 = newColComponent2;
            //}           

            //var postCollisionVelocity1 = collisionAxe * colComponent1 + tangentAxe1 * tangentComponent1;
            //PostCollisionDir = postCollisionVelocity1.normalized;
            //var originalSpeed = Speed;
            //Speed = postCollisionVelocity1.magnitude; // * _gameSettings.EnergyConservation;
            //CollisionResponseTimer = _gameSettings.CollisionResponseDuration;
            //if (originalSpeed > 0.0f)
            //    GetComponent<Hover>().Oscillate(1.0f - (Speed / originalSpeed));

            //var postCollisionVelocity2 = collisionAxe2 * colComponent2 + tangentAxe2 * tangentComponent2;
            //other.PostCollisionDir = postCollisionVelocity2.normalized;
            //var originalSpeed2 = other.Speed;
            //other.Speed = postCollisionVelocity2.magnitude; // * _gameSettings.EnergyConservation;
            //other.CollisionResponseTimer = _gameSettings.CollisionResponseDuration;
            //if (originalSpeed2 > 0.0f)
            //{
            //    var hover = other.GetComponent<Hover>();
            //    if(hover)
            //        hover.Oscillate(1.0f - (other.Speed / originalSpeed2));
            //}

            //if (!_playerSync.isServer)
            //{
            //    // Let colliding objects know about the collision
            //    var ai = other.transform.parent.GetComponent<BasicAI>();
            //    if (ai)
            //        _playerSync.ClientCollideWithAI(ai.netId.Value, other.PostCollisionDir, other.Speed);
            //    else
            //    {
            //        // colliding with a player
            //    }
            //}

            //Console.Print(string.Format("Result [this - [dir {0}][speed {1}], [other - [dir {2}][speed {3}]", PostCollisionDir, Speed, other.PostCollisionDir, other.Speed));

            //_processedCollisions.Add(other);
            //other._processedCollisions.Add(this);
        }

        //void OnDrawGizmos()
        //{
        //    Gizmos.color = Color.red;
        //    Gizmos.DrawSphere(transform.position + _dir * 1.0f, .2f);
        //    Gizmos.DrawLine(transform.position, transform.position + _dir * 1.0f);

        //    Gizmos.color = Color.blue;
        //    Gizmos.DrawSphere(transform.position + _postCollisionDir * 1.0f, .2f);
        //    Gizmos.DrawLine(transform.position, transform.position + _postCollisionDir * 1.0f);
        //}
    }
}
