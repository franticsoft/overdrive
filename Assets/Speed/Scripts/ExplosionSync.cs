﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace frantic
{
    public class ExplosionSync : NetworkBehaviour
    {
        public float Duration = 3.0f;

        float _timer = 0.0f;

        public override void OnStartServer()
        {
            //Console.Print(string.Format("ExplosionSync.OnStartServer({0})", netId.Value));
        }

        void Update()
        {
            if (isServer)
            {
                _timer += Time.deltaTime;
                if (_timer > Duration)
                    NetworkServer.Destroy(gameObject);
            }
        }
    }
}

