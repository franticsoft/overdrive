﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Linq;

namespace frantic
{
    public class Missile : NetworkBehaviour
    {
        [SyncVar]
        public Vector3 _netPos;

        [SyncVar]
        public Quaternion _netRotation;

        [SyncVar]
        public uint _netOwnerPlayerID;

        TrailRenderer _trail;
        MeshRenderer _missileRenderer;
        float _delayToDestruction;
        bool _toBeDestroyed;

        public override void OnStartServer()
        {
            //Console.Print(string.Format("Missile.OnStartServer({0})", netId.Value));
            _existsOnServer = true;
        }

        public override void OnStartClient()
        {
            _netPos = transform.position;
            transform.rotation = _netRotation;
            _existsOnServer = true;

			// check for redundant missile that is replicated by the server on this client
            var player = _netOwnerPlayerID != 0 ? GameObject.FindObjectsOfType<PlayerSync>().FirstOrDefault(p => p.netId.Value == _netOwnerPlayerID) : null;
            if (!isServer && player && player.isLocalPlayer)
            {
                Console.Print("Deactivating missile with id: " + netId.Value);
                gameObject.SetActive(false);
            }
            else
            {
                _manager.LevelManager.OnMissileLaunched(this);
            }
        }

        public override void OnNetworkDestroy()
        {
            //Console.Print("OnNetworkDestroy missile with id: " + netId.Value);
            if(gameObject.activeSelf)
                _manager.LevelManager.OnMissileDestroyed(this);
        }

        void OnDestroy()
        {
            if(!_existsOnServer)
                _manager.LevelManager.OnMissileDestroyed(this);
        }

        float _syncTimer = 0.0f;
        const float _syncFrequency = 0.1f;
        const float _lerpDuration = 0.1f;

        public float MaxSpeed = 3.0f;
        public float Accel = 0.1f;
        public float AnglePerSec = 180.0f;
        public float HomingDistance = 20.0f;

        Vector2 _curvePos;
        float _curveDist;
        BezierPath _track;
        int _curveIndex;
        float _speed;
        Transform _cannon;
        Vector3 _forward;

        Prefabs _prefabs;
        GameObject _target;
        SphereCollider _targetCollider;
        bool _targetLocked;
        Player _ownerPlayer;
        BasicAI _ownerAI;
        bool _existsOnServer;
        FranticNetworkManager _manager;
        Transform _bodyHolder;

        public void Init(Player player, BasicAI ai, int cannonIndex, float speed)
        {
            _manager = FranticNetworkManager.singleton as FranticNetworkManager;
            _ownerPlayer = player;
            _ownerAI = ai;
            var missileChild = transform.FindChild("Missile");
            _trail = missileChild.FindChild("Trail").GetComponent<TrailRenderer>();
            _missileRenderer = missileChild.GetComponent<MeshRenderer>();

            var ownerTransform = player != null ? player.transform : ai.transform;
            var ownerBodyTransform = player != null ? player.Body.transform : ai.transform.FindChild("Body");
            var bodyGO = ownerBodyTransform.gameObject;
            var weapon = bodyGO.GetComponent<Weapon>();
            var cannon = cannonIndex == 0 ? weapon.LeftCannon : weapon.RightCannon;
            _cannon = cannon;
            _bodyHolder = cannon.parent;

            if (player)
            {
                _bodyHolder = _bodyHolder.parent;
                Debug.Assert(ReferenceEquals(_bodyHolder.parent.gameObject, player.gameObject));
            }

            var cannonHeight = _bodyHolder.localPosition.y;

            if(player)
                player.GetCurveCoords(out _curvePos, out _curveDist, out _track, out _curveIndex);
            else
                ai.GetCurveCoords(out _curvePos, out _curveDist, out _track, out _curveIndex);

            var cannonAbsPos = cannon.position;
            var playerAbsPos = ownerTransform.position + ownerTransform.up * cannonHeight;
            GetCurvePos(_track, _curveIndex, _curvePos, _curveDist, playerAbsPos, cannonAbsPos, out _curvePos, out _curveDist, out _curveIndex, out _track);

            var currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();
            var tangent = _track.GetTangent(_curveIndex, _curvePos.y).normalized;
            var up = _track.Curves[_curveIndex].GetUp(_curvePos.y);
            var right = Vector3.Cross(up, tangent);
            var banking = Quaternion.AngleAxis(_track.GetBanking(_curveIndex, _curvePos.y), tangent);
            right = banking * right;
            up = banking * up;

            // update transform
            var curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
            var curveRadius = _manager.LevelManager.TrackRadius;
            var lateral0 = curvePoint - right * curveRadius;
            _forward = ownerBodyTransform.forward;
            transform.position = lateral0 + right * _curvePos.x + up * cannonHeight;
            transform.rotation = Quaternion.LookRotation(_forward, up);

            _speed = speed;
            if (speed > MaxSpeed)
                MaxSpeed = speed;

            _prefabs = _manager.LevelManager.Prefabs;
            _target = _manager.LevelManager.GetMissileTarget(_ownerPlayer, _ownerAI, transform);
            _targetCollider = null;
            if(_target != null)
            {
                if (_target.GetComponent<Player>() != null)
                    _targetCollider = _target.transform.FindChild("BodyHolder/Body").GetComponent<SphereCollider>();
                else
                    _targetCollider = _target.transform.FindChild("Body").GetComponent<SphereCollider>();
            }
            _targetLocked = false;
        }

        void Update()
        {
            if (_manager.LevelManager.IsPaused)
                return;

            if (!isServer && _existsOnServer)
            {
                var lerpFactor = Time.deltaTime / _lerpDuration;
                transform.position = Vector3.Lerp(transform.position, _netPos, lerpFactor);
                transform.rotation = Quaternion.Lerp(transform.rotation, _netRotation, lerpFactor);
                return;
            }

            if (_toBeDestroyed)
            {
                _delayToDestruction += Time.deltaTime;
                if (_delayToDestruction > 1.0f)
                {
                    if (_existsOnServer)
                        NetworkServer.Destroy(gameObject);
                    else
                        Destroy(this);
                    _toBeDestroyed = false;
                }
                return;
            }

            _speed += Accel;
            if (_speed > MaxSpeed)
                _speed = MaxSpeed;

            var tangent = _track.GetTangent(_curveIndex, _curvePos.y).normalized;
            var fwdFactor = Vector3.Dot(tangent, _forward);
            _curveDist += _speed * fwdFactor * _manager.LevelManager.AverageDT;
            var curveLength = _track.GetLength(_curveIndex);
            if (_curveDist > curveLength)
            {
                if (_curveIndex == (_track.Curves.Length - 1) && _track.NextTrack != null)
                {
                    var thisTrack = _track;
                    _track = _track.NextTrack.GetComponent<BezierPath>();
                    _curveIndex = 0;

                    if (!thisTrack.IsAlignedToNextTrack())
                    {
                        // TODO missile free-fly
                        Debug.Assert(false);
                        return;
                    }
                    else
                    {
                        _curveDist = _curveDist - curveLength;
                    }
                }
                else
                {
                    _curveDist = _curveDist - curveLength;
                    _curveIndex = (_curveIndex + 1) % _track.Curves.Length;
                }
            }
            else if (_curveDist < 0.0f)
            {
                if (_track.GetPreviousTrack() != null)
                {
                    if (_track.IsAlignedToPreviousTrack())
                    {
                        _track = _track.GetPreviousTrack().GetComponent<BezierPath>();
                        _curveIndex = _track.Curves.Length - 1;
                        curveLength = _track.GetLength(_curveIndex);
                        _curveDist = curveLength + _curveDist;
                    }
                    else
                    {
                        // TODO missile free-fly
                        Debug.Assert(false);
                        return;
                    }
                }
                else
                {
                    _curveIndex--;
                    if (_curveIndex < 0)
                        _curveIndex = _track.Curves.Length - 1;
                    curveLength = _track.GetLength(_curveIndex);
                    _curveDist = curveLength + _curveDist;
                }
            }

            var factor = _track.GetFactor(_curveIndex, _curveDist);
            _curvePos.y = factor;

            var curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
            var up = _track.Curves[_curveIndex].GetUp(_curvePos.y);
            var right = Vector3.Cross(up, tangent);
            var banking = Quaternion.AngleAxis(_track.GetBanking(_curveIndex, _curvePos.y), tangent);
            right = banking * right;
            up = banking * up;

            // get tip curve coords and check collision
            Vector2 missileTipPos;
            var missileTipIndex = _curveIndex;
            var missileTipTrack = _track;
            var missileTipDist = _curveDist;
            var absPos = transform.position;
            var missileLength = transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh.bounds.size.y;
            var tipAbsPos = absPos + _forward * missileLength;
            GetCurvePos(_track, _curveIndex, _curvePos, _curveDist, absPos, tipAbsPos, out missileTipPos, out missileTipDist, out missileTipIndex, out missileTipTrack);
            var currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();
            var lateralFactor = Vector3.Dot(right, _forward);
            var curveRadius = _manager.LevelManager.TrackRadius;
            var collisionDistToBorder = 0.0f;
            var maxX = (curveRadius * 2.0f) - collisionDistToBorder;
            var deltaX = _speed * lateralFactor * _manager.LevelManager.AverageDT;
            if (missileTipPos.x + deltaX >= 0.0f && missileTipPos.x + deltaX <= maxX)
            {
                _curvePos.x += deltaX;

                // adjust forward depending on up vector, by projecting forward onto base plane
                var plane = new Plane(up, transform.position);
                var rayOrigin = transform.position + _forward;
                var rayDir = plane.GetSide(rayOrigin) ? -up : up;
                var ray = new Ray(rayOrigin, rayDir);
                float toPlane = 0.0f;
                if (plane.Raycast(ray, out toPlane))
                {
                    var intersection = ray.origin + ray.direction * toPlane;
                    _forward = (intersection - transform.position).normalized;
                }

                // home to target
                var toTarget = Vector3.forward;
                if(_target != null)
                {
                    var targetPos = _target.transform.position;
                    toTarget = (targetPos - transform.position);
                    var distToTarget = toTarget.magnitude;
                    toTarget /= distToTarget; // normalize
                    if (!_targetLocked)
                    {
                        if (distToTarget < HomingDistance)
                            _targetLocked = true;
                        else
                            toTarget = tangent;
                    }
                }
                else
                {
                    toTarget = tangent;
                }

                Vector3 localRight, localUp;
                MathUtils.GetBasis(_forward, out localUp, out localRight);
                Vector3 toTargetRight, toTargetUp;
                MathUtils.GetBasis(toTarget, out toTargetUp, out toTargetRight);
                var angle = Vector3.Angle(localRight, toTargetRight);
                if (angle > MathUtils.Epsilon)
                {
                    var rotationAxis = Vector3.Cross(_forward, toTarget).normalized;
                    var rotationDir = Mathf.Sign(Vector3.Dot(rotationAxis, up));
                    _forward = Quaternion.AngleAxis(rotationDir * AnglePerSec * _manager.LevelManager.AverageDT, up) * _forward;
                }

                var lateral0 = curvePoint - right * curveRadius;
                transform.position = lateral0 + right * _curvePos.x + up * _bodyHolder.localPosition.y;
                transform.rotation = Quaternion.LookRotation(_forward, up);

                // check collision with target
                if (_targetCollider)
                {
                    var tipToTargetSq = (_targetCollider.transform.position - tipAbsPos).sqrMagnitude;
                    if (tipToTargetSq < _targetCollider.radius * _targetCollider.radius)
                    {
                        var t = _target.GetComponent<Transform>();
                        var explosion = Explode(_prefabs.Explosion, t.position, t.rotation);

                        // Hack for split screen
                        if (_manager.IsSplitScreenGame)
                            explosion.transform.FindChild("ExplosionTrails").gameObject.SetActive(false);

                        if (_existsOnServer)
                        {
                            // TODO: proper handling, if AI / or human apply damage properly
                            //Destroy(_target);
                            var ai = _target.GetComponent<BasicAI>();
                            if (ai)
                                ai.OnMissileHit();
                            else
                            {
                                var player = _target.GetComponent<Player>();
                                if (!ReferenceEquals(player, _ownerPlayer))
                                    player.OnMissileHit();
                            }
                        }
                        return;
                    }
                }
            }
            else
            {
                Explode(_prefabs.WallExplosion, tipAbsPos, _prefabs.WallExplosion.transform.localRotation);
                return;
            }

            // update clients
            if (_syncTimer > _syncFrequency)
            {
                _netPos = transform.position;
                _netRotation = transform.rotation;
                _syncTimer = 0.0f;
            }
            _syncTimer += Time.deltaTime;
        }

        GameObject Explode(GameObject explosionGO, Vector3 pos, Quaternion rot)
        {
            _missileRenderer.enabled = false;
            _toBeDestroyed = true;
            _delayToDestruction = 0.0f;
            if (_existsOnServer)
            {
                Debug.Assert(isServer);
                var explosion = Instantiate(explosionGO, pos, rot) as GameObject;
                NetworkServer.Spawn(explosion);
                return explosion;
            }
            return null;
        }

        public static bool GetCurvePos(BezierPath fromTrack, int fromCurveIndex, Vector2 fromPos, float fromDist, Vector3 fromAbs, Vector3 toAbs, out Vector2 pos, out float dist, out int curveIndex, out BezierPath track)
        {
            var tangent = fromTrack.GetTangent(fromCurveIndex, fromPos.y).normalized;
            Vector3 up, right;
            MathUtils.GetBasis(tangent, out up, out right);
            var banking = Quaternion.AngleAxis(fromTrack.GetBanking(fromCurveIndex, fromPos.y), tangent);
            right = banking * right;
            up = banking * up;

            var toDest = toAbs - fromAbs;
            var projOnTangent = Vector3.Project(toDest, tangent);
            var projOnRight = Vector3.Project(toDest, right);
            dist = fromDist;
            pos = fromPos;
            dist += projOnTangent.magnitude * Mathf.Sign(Vector3.Dot(projOnTangent.normalized, tangent));
            pos.x += projOnRight.magnitude * Mathf.Sign(Vector3.Dot(projOnRight.normalized, right));
            curveIndex = fromCurveIndex;
            track = fromTrack;

            // is destination in another segment?
            var curveLength = fromTrack.GetLength(fromCurveIndex);
            if (dist > curveLength)
            {
                if (fromCurveIndex == (fromTrack.Curves.Length - 1) && fromTrack.NextTrack != null)
                {
                    track = fromTrack.NextTrack.GetComponent<BezierPath>();
                    curveIndex = 0;

                    if (!fromTrack.IsAlignedToNextTrack())
                    {
                        // TODO missile free-fly
                        Debug.Assert(false);
                        return false;
                    }
                    else
                    {
                        dist = fromDist - curveLength;
                    }
                }
                else
                {
                    dist = dist - curveLength;
                    curveIndex = (curveIndex + 1) % fromTrack.Curves.Length;
                }
            }
            else if (dist < 0.0f)
            {
                if (fromTrack.GetPreviousTrack() != null)
                {
                    if (fromTrack.IsAlignedToPreviousTrack())
                    {
                        track = fromTrack.GetPreviousTrack().GetComponent<BezierPath>();
                        curveIndex = track.Curves.Length - 1;
                        curveLength = fromTrack.GetLength(fromCurveIndex);
                        dist = curveLength + dist;
                    }
                    else
                    {
                        // TODO missile free-fly
                        Debug.Assert(false);
                        return false;
                    }
                }
                else
                {
                    curveIndex--;
                    if (curveIndex < 0)
                        curveIndex = fromTrack.Curves.Length - 1;
                    curveLength = fromTrack.GetLength(fromCurveIndex);
                    dist = curveLength + dist;
                }
            }

            var factor = track.GetFactor(curveIndex, dist);
            pos.y = factor;
            return true;
        }
    }
}
