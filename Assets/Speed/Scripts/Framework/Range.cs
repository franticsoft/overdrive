﻿using UnityEngine;
using System.Collections;
using System;

namespace frantic
{
    [Serializable]
    public class FloatRange : System.Object
    {
        public float Min;
        public float Max;

        public FloatRange(float min, float max)
        {
            Min = min;
            Max = max;
        }
    }

    [Serializable]
    public class IntRange : System.Object
    {
        public int Min;
        public int Max;

        public IntRange(int min, int max)
        {
            Min = min;
            Max = max;
        }
    }
}

