﻿using UnityEngine;
using System.Collections.Generic;
using System.Timers;
using System;

namespace frantic
{
    public static class Utils
    {
        public static GameObject AddChild(this Transform parent, GameObject source)
        {
            var child = GameObject.Instantiate(source);
            child.transform.localPosition = Vector3.zero;
            child.transform.SetParent(parent, false);
            return child;
        }

        public static void DestroyAllChildren(this Transform t)
        {
            #if UNITY_EDITOR
                var children = new List<GameObject>();
                foreach (Transform child in t)
                    children.Add(child.gameObject);
                foreach (var go in children)
                    GameObject.DestroyImmediate(go);            
            #else
                foreach (Transform child in t)            
                    GameObject.Destroy(child.gameObject);            
            #endif
        }

        public static void ExecuteLater(float delayInSeconds, Action func)
        {
            var timer = new Timer((double)delayInSeconds * 1000);
            timer.Elapsed += new ElapsedEventHandler(delegate(System.Object o, ElapsedEventArgs a)
            {
                func();
            });
            timer.Enabled = true;
        }

        public static GameObject FindChildWithTag(this Transform parent, string tag)
        {
            foreach(Transform t in parent)
            {
                if (t.tag.Equals(tag))
                    return t.gameObject;
            }
            return null;
        }

        public static void SetLayerMask(this GameObject go, int layerMask)
        {
            go.layer = layerMask;

            foreach (Transform t in go.transform)
                SetLayerMask(t.gameObject, layerMask);
        }

        public static void PlayParticles(this ParticleSystem ps)
        {
            if (!ps.isPlaying)
                ps.Play(true);

            // Unity bug, sometimes particles don't start!
            ps.gameObject.SetActive(false);
            ps.gameObject.SetActive(true);
        }

        public static void StopParticles(this ParticleSystem ps)
        {
            if (ps.isPlaying)
                ps.Stop(true);
        }

        public static bool IsInputAccept()
        {
            return Input.GetButtonUp("P1_Submit") || Input.GetButtonUp("Pause");
        }

        public static bool IsInputCancel()
        {
            return Input.GetButtonUp("P1_Cancel");
        }

        public static float Random(this FloatRange range)
        {
            return UnityEngine.Random.Range(range.Min, range.Max);
        }
    }
}

