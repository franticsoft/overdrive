﻿
using UnityEditor;

namespace frantic
{
    [CustomEditor(typeof(Billboard))]
    public class BillboardEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.DrawDefaultInspector();

            var bb = target as Billboard;
            if (bb.AxisAligned)
            {
                PropertyInspector.EditValueVariables(bb, new string[] { "RotationAxis" });
            }
        }
    }
}
