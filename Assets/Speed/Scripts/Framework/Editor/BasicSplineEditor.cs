﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace frantic
{
    [CustomEditor(typeof(BasicSpline))]
    public class BasicSplineEditor : Editor
    {
        const float numSegments = 40;
        const float handleSize = 0.04f;
        const float pickSize = 0.06f;

        int _selectedPointIndex = -1;

        void OnSceneGUI()
        {
            var spline = target as BasicSpline;
            if (spline == null || spline.Points == null)
                return;

            for (int i = 0; i < numSegments-1; ++i)
            {
                var t0 = (float)i / numSegments;
                var t1 = (float)(i+1) / numSegments;
                var curPos = spline.transform.TransformPoint(spline.GetPoint(t0));
                var nextPos = spline.transform.TransformPoint(spline.GetPoint(t1));
                Handles.color = Color.red;
                Handles.DrawLine(curPos, nextPos);
            }

            for (int i = 0; i < spline.Points.Length; ++i)
            {
                var absolute = spline.transform.TransformPoint(spline.Points[i]);
                float size = HandleUtility.GetHandleSize(absolute);
                if (Handles.Button(absolute, Quaternion.identity, handleSize * size, pickSize * size, Handles.DotCap))
                {
                    _selectedPointIndex = i;
                    break;
                }
            }

            if (_selectedPointIndex >= spline.Points.Length)
                _selectedPointIndex = -1;

            if (_selectedPointIndex >= 0)
            {
                var absolute = spline.transform.TransformPoint(spline.Points[_selectedPointIndex]);
                EditorGUI.BeginChangeCheck();
                absolute = Handles.DoPositionHandle(absolute, Quaternion.identity);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(spline, "Translate Point");
                    spline.Points[_selectedPointIndex] = spline.transform.InverseTransformPoint(absolute);
                }
            }
        }
    }
}

