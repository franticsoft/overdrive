﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;

namespace frantic
{
    public enum FranticMessage
    {
        EnterTrackSelection = MsgType.Highest + 1,
        ExitTrackSelection
    };

    public class FranticNetworkManager : NetworkManager
    {
        [Header("Menu")]
        [HideInInspector]
        public string ShipSelectionScene;
        [HideInInspector]
        public string TrackSelectionScene;
        public float ShipSelectionFocalDist = 5.0f;
        public float TrackSelectionFocalDist = 17.0f;

        public bool IsSplitScreenGame { get; set; }
        public bool IsSinglePlayerGame { get; set; }
        public bool IsLocalGame
        {
            get
            {
                return IsSplitScreenGame || IsSinglePlayerGame;
            }
        }
        public MainMenu MainMenu { get; set; }
        public TrackSelector TrackSelector { get; set; }
        public ShipSelector ShipSelector { get; set; }
        public ScreenDistortion ScreenDistortion { get; set; }
        public LevelManager LevelManager { get; set; }
        public PlayerCamera PlayerCamera { get; set; }

        AudioSource _audioSource;
        GameSettings _gameSettings;

        void Start()
        {
            //QualitySettings.vSyncCount = 0;  // VSync must be disabled            
            //Application.targetFrameRate = 60;
            networkSceneName = offlineScene;
            _audioSource = GetComponent<AudioSource>();
            _gameSettings = GetComponent<GameSettings>();
        }

        void Update()
        {
            if (!_audioSource.isPlaying)
            {
                _audioSource.clip = _gameSettings.MusicClips[GetRandomMusic(ref _musicIndices, ref _gameSettings.MusicClips)];
                _audioSource.Play();
            }
        }

        List<int> _musicIndices;
        List<int> _inGameMusicIndices;
        int GetRandomMusic(ref List<int> indices, ref AudioClip[] clips)
        {
            if(indices == null || indices.Count == 0)
            {
                indices = new List<int>();
                for (int i = 0; i < clips.Length; ++i)
                    indices.Add(i);
            }

            int random = UnityEngine.Random.Range(0, indices.Count);
            int index = indices[random];
            indices.RemoveAt(random);
            return index;
        }

        public void StartLocalSplitScreen()
        {
            StartHost();
            IsSplitScreenGame = true;
        }

        public void StartSinglePlayer()
        {
            StartHost();
            IsSinglePlayerGame = true;
        }

        public override void OnClientConnect(NetworkConnection conn)
        {
            Console.Print("OnClientConnect!");
            ClientScene.Ready(conn);
            ClientScene.AddPlayer(0);
            if (IsSplitScreenGame)
            {
                ClientScene.AddPlayer(1);
            }

            client.RegisterHandler((short)FranticMessage.EnterTrackSelection, OnEnterTrackSelection);
            client.RegisterHandler((short)FranticMessage.ExitTrackSelection, OnExitTrackSelection);
        }

        public override void OnClientDisconnect(NetworkConnection conn)
        {
            Console.Print("OnClientDisconnect!");

            if (matchMaker)
                StopMatchMaker();

            if (!IsSplitScreenGame)
            {
                var lobbyPlayers = GameObject.FindObjectsOfType<FranticLobbyPlayer>();
                foreach (var player in lobbyPlayers)
                    Destroy(player.gameObject);

                var ingamePlayers = GameObject.FindObjectsOfType<PlayerSync>();
                foreach (var player in ingamePlayers)
                    Destroy(player.gameObject);

                SceneManager.LoadScene(offlineScene);
            }
        }

        public bool AreAllPlayersReady()
        {
            var players = GameObject.FindObjectsOfType<FranticLobbyPlayer>();
            if (!players.Any())
                return false;

            foreach (var player in players)
            {
                if (!player.IsReady())
                    return false;
            }

            return true;
        }

        string _gameScene;
        public void ServerStartGame(string gameScene)
        {
            Console.Print("ServerStartGame: " + gameScene);
            ServerChangeScene(gameScene);
            _gameScene = gameScene;
        }

        public override void OnClientSceneChanged(NetworkConnection conn)
        {
            base.OnClientSceneChanged(conn);

            Console.Print("OnClientSceneChanged: " + SceneManager.GetActiveScene().name);
            if (SceneManager.GetActiveScene().name.Equals(_gameScene))
            {
                var splitScreen = GameObject.Find("SplitScreen");
                if(splitScreen)
                    splitScreen.SetActive(IsSplitScreenGame);
            }
        }

        public override void OnServerSceneChanged(string sceneName)
        {
            Console.Print("OnServerSceneChanged: " + sceneName);
            if (sceneName.Equals(_gameScene))
            {
                var spawnPoints = GameObject.FindObjectsOfType<SpawnPoint>().ToList();
                Console.Print("spawnPoints: " + spawnPoints.Count);

                LevelManager.LaneRange = new IntRange(0, spawnPoints.Count-1);

                var lobbyPlayers = GameObject.FindObjectsOfType<FranticLobbyPlayer>();
                Console.Print("lobbyPlayers: " + lobbyPlayers.ToList().Count);

                var gameState = GetComponent<GameState>();
                foreach (var lobbyPlayer in lobbyPlayers)
                {
                    var spawnPoint = spawnPoints.FirstOrDefault(p => !p.IsTaken && MathUtils.IsZero(p.transform.position.z));
                    Console.Print("non-taken spawn point: " + spawnPoint);
                    if (spawnPoint != null)
                    {
                        // TODO: retrieve the selected ship from clients
                        var go = (GameObject)Instantiate(gameState.SelectedShip[lobbyPlayer.playerControllerId], spawnPoint.transform.position, Quaternion.identity);
                        go.GetComponent<PlayerSync>()._netRotation = Quaternion.identity;
                        spawnPoint.IsTaken = true;
                        Console.Print("NetworkServer.ReplacePlayerForConnection..");
                        NetworkServer.ReplacePlayerForConnection(lobbyPlayer.connectionToClient, go, lobbyPlayer.playerControllerId);
                    }
                }

                // spawn AIs in remaining spawn points
                var freeSpawnPoints = spawnPoints.Where(p => !p.IsTaken).ToList();
                Console.Print("remaining spawn points for AI: " + freeSpawnPoints.Count);
                var prefabs = LevelManager.Prefabs;
                foreach (var point in freeSpawnPoints)
                {
                    var laneIndex = spawnPoints.IndexOf(point);
                    var go = (GameObject)Instantiate(prefabs.BasicAI, point.transform.position, Quaternion.identity);
                    point.IsTaken = true;
                    NetworkServer.Spawn(go);
                }

                //_audioSource.clip = _gameSettings.InGameMusicClips[GetRandomMusic(ref _inGameMusicIndices, ref _gameSettings.InGameMusicClips)];
                //_audioSource.Play();
            }
        }

        public override void OnStopHost()
        {
            Console.Print("OnStopHost");
            IsSplitScreenGame = false;
            IsSinglePlayerGame = false;
        }

        MatchInfo _createMatchInfo;
        public override void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
        {
            Console.Print(String.Format("OnMatchCreate: {0}, networkdId: {1}", success, matchInfo.networkId));
            _createMatchInfo = matchInfo;
            base.OnMatchCreate(success, extendedInfo, matchInfo);
        }

        MatchInfo _joinMatchInfo;
        public override void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
        {
            Console.Print(String.Format("OnMatchJoined: {0}, networkdId: {1}", success, matchInfo.networkId));
            _joinMatchInfo = matchInfo;

            base.OnMatchJoined(success, extendedInfo, matchInfo);
            if (success)
            {
                // Unity bug supposedly fixed in 5.4
                //try
                //{
                //    Utility.SetAccessTokenForNetwork(matchInfo.networkId, new NetworkAccessToken(matchInfo.accessTokenString));
                //}
                //catch(Exception)
                //{
                //}
                //StartClient(new MatchInfo(matchInfo));
            }
            else
            {
                ShipSelector.ExitShipSelection();
            }
        }

        public void ExitOnlineGame()
        {
            if (NetworkServer.active)
            {
                if (matchMaker)
                    DestroyMatch();
                else
                    StopHost();
            }
            else
            {
                if (NetworkClient.active)
                {
                    if (matchMaker)
                        LeaveMatch();
                    else
                        StopClient();
                }
            }
        }

        public void DestroyMatch()
        {
            if (_createMatchInfo == null)
                return;

            matchMaker.DestroyMatch(_createMatchInfo.networkId, 0, new NetworkMatch.BasicResponseDelegate(OnMatchDestroyed));
            _createMatchInfo = null;
        }

        public void LeaveMatch()
        {
            if (_joinMatchInfo == null)
                return;

            matchMaker.DropConnection(_joinMatchInfo.networkId, _joinMatchInfo.nodeId, 0, new NetworkMatch.BasicResponseDelegate(OnMatchLeft));
            _joinMatchInfo = null;            
        }

        public void OnMatchDestroyed(bool success, string extendedInfo)
        {
            Console.Print("OnMatchDestroyed: " + success);
            StopHost();
        }

        public void OnMatchLeft(bool success, string extendedInfo)
        {
            Console.Print("OnMatchLeft: " + success);
            StopClient();
        }

        void OnEnterTrackSelection(NetworkMessage message)
        {
            Console.Print("OnEnterTrackSelection");
            if(!NetworkServer.active)
                ShipSelector.EnterTrackSelection();
        }

        void OnExitTrackSelection(NetworkMessage message)
        {
            Console.Print("OnExitTrackSelection");
            if (!NetworkServer.active)
                TrackSelector.ExitTrackSelection();
        }        
    }
}

