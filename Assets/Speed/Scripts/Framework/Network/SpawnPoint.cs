﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class SpawnPoint : MonoBehaviour
    {
        bool _isTaken;
        public bool IsTaken 
        {
            get { return _isTaken; }
            set { _isTaken = value; }
        }
    }
}
