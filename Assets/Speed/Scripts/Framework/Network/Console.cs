﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace frantic
{
    public class Console : MonoBehaviour
    {
        public static bool IsVisible;

        public int MaxLines = 12;

        string[] _lines;
        int _currentLine = 0;
        static Console _instance;

        public static void Print(string line)
        {
            Debug.Log(line);
            if (!CheckInstance())
                return;

            _instance.InternalPrint(line);
        }

        public static void Clear()
        {
            if (!CheckInstance())
                return;

            _instance._lines = null;
            _instance.GetComponent<Text>().text = "";
        }

        public static void SetVisible(bool visible)
        {
            if (!CheckInstance())
               return;

            var text = _instance.GetComponent<Text>();
            text.enabled = visible;
            IsVisible = visible;
        }

        static bool CheckInstance()
        {
            if (_instance == null)
            {
                var go = GameObject.Find("Console");
                if (go)
                    _instance = go.GetComponent<Console>();
            }

            return _instance != null;
        }

        public void InternalPrint(string line)
        {
            if (_lines == null)
            {
                _lines = new string[MaxLines];
                _currentLine = 0;
            }

            if (_currentLine < _lines.Length)
            {
                _lines[_currentLine] = line;
            }
            else
            {
                for (int i = 0; i < _lines.Length - 1; ++i)
                    _lines[i] = _lines[i + 1];

                _lines[_lines.Length - 1] = line;
            }

            ++_currentLine;
            UpdateText();
        }

        void UpdateText()
        {
            var text = GetComponent<Text>();

            var lineIndex = _currentLine - MaxLines;
            if (lineIndex < 0)
                lineIndex = 0;

            var message = "" + (lineIndex++) + ". " + _lines[0];
            for (int i = 1; i < MaxLines; ++i)
                message = message + "\n" + "" + (lineIndex++) + ". " + _lines[i];

            text.text = message;
            text.enabled = IsVisible;
        }
    }
}
