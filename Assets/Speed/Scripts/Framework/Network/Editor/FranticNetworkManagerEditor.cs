﻿using UnityEditor;
using System.IO;

namespace frantic
{
    [CustomEditor(typeof(FranticNetworkManager))]
    public class FranticNetworkManagerEditor : Editor
    {
        string[] _scenes;
        void OnEnable()
        {
            _scenes = SceneInspector.Init();
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var manager = target as FranticNetworkManager;
            manager.ShipSelectionScene = SceneInspector.EditSceneProperty(manager, "ShipSelectionScene", _scenes, manager.ShipSelectionScene);
            manager.TrackSelectionScene = SceneInspector.EditSceneProperty(manager, "TrackSelectionScene", _scenes, manager.TrackSelectionScene);
        }
    }
}

