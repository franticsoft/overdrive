﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

namespace frantic
{
    public class FranticLobbyPlayer : NetworkBehaviour
    {
        [SyncVar(hook = "OnReadyChanged")]
        bool _isReady;

        FranticNetworkManager _manager;
        ShipSelector.ShipSelectionContext _context;
        GameSettings _gameSettings;
        GameState _gameState;
        ShipSelector _selector;
        GameObject _readyIndicator;
        GameObject _menuCamera;
        NavigationBar _navBar;

        public override void OnStartServer()
        {
            Console.Print(string.Format("FranticLobbyPlayer.OnStartServer({0})", netId.Value));
        }

        public override void OnStartClient()
        {
            Console.Print(string.Format("FranticLobbyPlayer.OnStartClient({0})", netId.Value));
            (NetworkManager.singleton as FranticNetworkManager).ShipSelector.SetPlayerLobbyDirty();
        }

        public override void OnNetworkDestroy()
        {
            Console.Print(string.Format("FranticLobbyPlayer.OnNetworkDestroy({0})", netId.Value));
            (NetworkManager.singleton as FranticNetworkManager).ShipSelector.SetPlayerLobbyDirty();
        }

        private void Start()
        {
            Object.DontDestroyOnLoad(gameObject);

            _manager = NetworkManager.singleton as FranticNetworkManager;
            _gameSettings = _manager.GetComponent<GameSettings>();
            _gameState = _manager.GetComponent<GameState>();
            _menuCamera = GameObject.Find("MenuCamera");

            if (_manager.IsSplitScreenGame)
                InitForSplitScreen();

            if (_manager.IsSinglePlayerGame)
                _isReady = true;
        }

        public void InitForSplitScreen()
        {
            _context = new ShipSelector.ShipSelectionContext();
            _context.screen = GameObject.Find("ShipSelectionScreen");
            var splitScreen = _context.screen.transform.FindChild("SplitScreen").gameObject;
            splitScreen.SetActive(_manager.IsSplitScreenGame);

            _context.playerIndex = playerControllerId;
            _selector = GameObject.Find("ShipSelector").GetComponent<ShipSelector>();
            var cameraGO = Instantiate(_selector.SplitScreenCamera);
            var isTop = (playerControllerId == 0);
            var camera = cameraGO.GetComponent<Camera>();
            camera.targetTexture = new RenderTexture(Screen.width, Screen.height / 2, 24);

            var layerToExclude = isTop ? "SplitScreenBottom" : "SplitScreenTop";
            var layerIndex = LayerMask.NameToLayer(layerToExclude);
            var exclusionMask = ~(1 << layerIndex);
            camera.cullingMask = camera.cullingMask & exclusionMask;

            var containerName = isTop ? "TopTarget" : "BottomTarget";
            var container = splitScreen.transform.FindChild(containerName);
            container.GetComponent<RawImage>().enabled = true;
            var rt = container.transform.FindChild("RenderTarget");
            rt.GetComponent<RawImage>().texture = camera.targetTexture;

            var spotsName = isTop ? "TopSpots" : "BottomSpots";
            _context.spots = _selector.transform.FindChild(spotsName);
            _context.middleSpot = _context.spots.GetChild(1);
            _context.selectedShipGO = _selector.CreateShip(_context.playerIndex, _gameState.SelectedShipIndex[_context.playerIndex]);
            _selector.AttachToSpot(_context.middleSpot, _context.selectedShipGO);

            _selector.InitStats(ref _context);
            _context.statsPanel.transform.SetParent(rt, false);
            var panelTransform = _context.statsPanel.GetComponent<RectTransform>();
            panelTransform.localScale = Vector3.one * 1.5f;
            _selector.UpdateStats(ref _context);

            _readyIndicator = GameObject.Instantiate(_selector.ReadyIndicator);
            _readyIndicator.transform.SetParent(rt, false);
            _readyIndicator.SetActive(false);

            _navBar = Instantiate(_gameSettings.NavigationBar).GetComponent<NavigationBar>();
            _navBar.transform.SetParent(rt, false);            
            _navBar.SetConfiguration(playerControllerId == 0 ? NavigationBar.ConfigType.Default : NavigationBar.ConfigType.MainMenuStart);

            _isReady = false;
        }
        
        void Update()
        {
            if (_gameState.GameContext != GameState.Context.ShipSelection)
                return;

            if (_manager.IsSplitScreenGame)
                UpdateSplitScreen();
        }

        void UpdateSplitScreen()
        {
            if(!_isReady)
            {
                _selector.UpdateInput(ref _context);

                if (!_selector.UpdateTransitions(ref _context))
                {
                    _selector.HandleLookInput(ref _context);

                    if (Input.GetButtonUp("P" + (playerControllerId + 1) + "_Submit"))
                    {
                        _readyIndicator.SetActive(true);
                        _gameState.SelectedShip[_context.playerIndex] = _selector.Ships[_gameState.SelectedShipIndex[_context.playerIndex]];
                        _isReady = true;

                        if (playerControllerId > 0)
                            _navBar.SetConfiguration(NavigationBar.ConfigType.Default);
                    }
                    else if (Input.GetButtonUp("P" + (playerControllerId + 1) + "_Cancel"))
                    {
                        if (playerControllerId == 0) // only P1 can cancel ship selection
                        {
                            _selector.enabled = false;
                            _gameState.GameContext = GameState.Context.MainMenu;
                            _manager.StopHost();
                        }
                    }
                }
            }
            else
            {
                _selector.UpdateTransitions(ref _context);
                if (Input.GetButtonUp("P" + (playerControllerId + 1) + "_Submit"))
                {
                    if (playerControllerId == 0) // only P1 can move to track selection
                    {
                        if (_manager.AreAllPlayersReady())
                        {
                            _selector.enabled = false;
                            _manager.ShipSelector.EnterTrackSelection();
                        }
                    }
                }
                else if (Input.GetButtonUp("P" + (playerControllerId + 1) + "_Cancel"))
                {
                    _readyIndicator.SetActive(false);
                    _isReady = false;

                    if (playerControllerId > 0)
                        _navBar.SetConfiguration(NavigationBar.ConfigType.MainMenuStart);
                }
            }           

            _selector.UpdateGageAnims(ref _context);
        }

        [Command]
        public void CmdReadyStatus(bool ready)
        {
            _isReady = ready;
        }

        public bool IsReady()
        {
            return _isReady;
        }

        [Client]
        void OnReadyChanged(bool value)
        {
            _isReady = value;
            _manager.ShipSelector.SetPlayerLobbyDirty();
        }

        private void OnGUI()
        {
            //var manager = NetworkManager.singleton as FranticNetworkManager;
            //if (manager)
            //{
            //    if (SceneManager.GetActiveScene().name != manager.offlineScene)
            //        return;
            //}

            //var players = GameObject.FindObjectsOfType<FranticLobbyPlayer>();
            //for (int i = 0; i < players.Length; ++i)
            //{
            //    var player = players[i];
            //    if (player.netId != this.netId)
            //        continue;

            //    Rect rect = new Rect((float)(100 + i * 100), 200f, 90f, 20f);
            //    if (player.isLocalPlayer)
            //    {
            //        var playerName = " [ You ]";
            //        if (manager.IsSplitScreenGame)
            //            playerName = " [ Player " + playerControllerId + " ]";

            //        GUI.Label(rect, playerName);
            //        if (player._isReady)
            //        {
            //            rect.y = rect.y + 25f;
            //            if (GUI.Button(rect, "Ready"))
            //            {
            //                player.CmdReadyStatus(false);
            //            }
            //        }
            //        else
            //        {
            //            rect.y = rect.y + 25f;
            //            if (GUI.Button(rect, "Not Ready"))
            //            {
            //                player.CmdReadyStatus(true);
            //            }
            //            rect.y = rect.y + 25f;
            //        }
            //    }
            //    else
            //    {
            //        GUI.Label(rect, "Player [" + player.netId + "]");
            //        rect.y = rect.y + 25f;
            //        GUI.Label(rect, "Ready [" + player._isReady + "]");
            //    }
            //}
        }
    }
}
