
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Collections.Generic;
using System.Linq;

namespace frantic
{
    [AddComponentMenu("Network/FranticNetworkHUD")]
	[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
	public class FranticNetworkHUD : MonoBehaviour
	{
        float _fpsCounter;
        float _time = 0.0f;
        float _fps;

        void Start()
        {
            var manager = NetworkManager.singleton as FranticNetworkManager;
            NetworkManager.networkSceneName = manager.offlineScene;
        }

        bool AreAllPlayersReady()
        {
            var players = GameObject.FindObjectsOfType<FranticLobbyPlayer>();
            if (!players.Any())
                return false;

            foreach(var player in players)
            {
                if (!player.IsReady())
                    return false;
            }

            return true;
        }

        void Update()
        {
            _fpsCounter += 1.0f;
            _time += Time.deltaTime;
            if(_time >= 1.0f)
            {
                var overTime = _time - 1.0f;
                _fps = _fpsCounter - (_fpsCounter * overTime);
                _fpsCounter = 0.0f;
                _time = 0.0f;
            }
        }

		void OnGUI()
		{
			int xpos = 10;
			int ypos = 40;
			int spacing = 24;
            var manager = NetworkManager.singleton as FranticNetworkManager;
            if (manager == null)
               return;

            GUI.Label(new Rect(xpos, ypos, 300, 20), "FPS: " + _fps);
            ypos += spacing;

			if (!NetworkClient.active && !NetworkServer.active)
			{
                if (manager.matchMaker == null)
                {
                    if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Single Player"))
                        manager.StartSinglePlayer();
                    ypos += spacing;

                    if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Local Split-Screen"))
                        manager.StartLocalSplitScreen();
                    ypos += spacing;

                    if (GUI.Button(new Rect(xpos, ypos, 200, 20), "LAN Host"))
                        manager.StartHost();
                    ypos += spacing;

                    if (GUI.Button(new Rect(xpos, ypos, 100, 20), "LAN Join"))
                        manager.StartClient();
                    manager.networkAddress = GUI.TextField(new Rect(xpos + 100, ypos, 100, 20), manager.networkAddress);
                    ypos += spacing;

                    if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Online Match-making"))
                        manager.StartMatchMaker();
                    ypos += spacing;
                }
                else
                {
                    if (manager.matchInfo == null)
					{
						if (manager.matches == null)
						{
                            if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Create Game"))
								manager.matchMaker.CreateMatch(manager.matchName, manager.matchSize, true, string.Empty, string.Empty, string.Empty, 0, 0, 
                                    new NetworkMatch.DataResponseDelegate<MatchInfo>(manager.OnMatchCreate));
                            ypos += spacing;

							GUI.Label(new Rect(xpos, ypos, 100, 20), "Room Name:");
							manager.matchName = GUI.TextField(new Rect(xpos + 100, ypos, 100, 20), manager.matchName);
							ypos += spacing;

                            if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Find Game"))
								manager.matchMaker.ListMatches(0, 20, string.Empty, true, 0, 0, new NetworkMatch.DataResponseDelegate<List<MatchInfoSnapshot>>(manager.OnMatchList));
							ypos += spacing;
						}
						else
						{
							foreach (MatchInfoSnapshot current in manager.matches)
							{
                                if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Online Join:" + current.name))
								{
									manager.matchName = current.name;
									manager.matchSize = (uint)current.currentSize;
									manager.matchMaker.JoinMatch(current.networkId, string.Empty, string.Empty, string.Empty, 0, 0, 
                                        new NetworkMatch.DataResponseDelegate<MatchInfo>(manager.OnMatchJoined));
								}
								ypos += spacing;
							}
						}
					}					
					
					GUI.Label(new Rect(xpos, ypos, 400, 20), "MM Uri: " + manager.matchMaker.baseUri);
					ypos += spacing;

					if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Exit Match-Making"))
						manager.StopMatchMaker();
					ypos += spacing;
                }
			}
            //else
            //{
            //    if (NetworkServer.active)
            //    {
            //        GUI.Label(new Rect(xpos, ypos, 300, 20), "Server: port=" + manager.networkPort);
            //        ypos += spacing;
            //    }
            //    if (NetworkClient.active)
            //    {
            //        GUI.Label(new Rect(xpos, ypos, 300, 20), "Client: address=" + manager.networkAddress + " port=" + manager.networkPort);
            //        ypos += spacing;
            //    }
            //}

			if (NetworkServer.active || NetworkClient.active)
			{
				if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Stop"))
                {
                    if (manager.matchMaker)
                        manager.StopMatchMaker();

                    manager.StopHost();
                }
				ypos += spacing;
			}

            //if (NetworkServer.active && NetworkManager.networkSceneName == manager.offlineScene)
            //{
            //    if (AreAllPlayersReady())
            //    {
            //        if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Start Game") || manager.IsSinglePlayerGame)
            //        {
            //            manager.ServerStartGame();
            //        }
            //        ypos += spacing;
            //    }
            //}

            if (GUI.Button(new Rect(xpos, ypos, 100, 20), Console.IsVisible ? "Console: On" : "Console: Off"))
                Console.SetVisible(!Console.IsVisible);

            if (Console.IsVisible && GUI.Button(new Rect(xpos + 100, ypos, 100, 20), "Clear"))
                Console.Clear();
            ypos += spacing;
		}
	}
}

