﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;
using System.Linq;
using System;

namespace frantic
{
    public class PlayerSync : NetworkBehaviour
    {
        [SyncVar]
        public float _netTraveledDistance;

        [SyncVar]
        public float _netLocalTraveledDistance;

        public struct NetState
        {
            public Vector3 Pos;
            public Vector2 CurvePos;
            public float CurveDist;
            public Vector3 Dir;
            public float Speed;
            public float Health;
        }

        [SyncVar]
        NetState _netState;

        [SyncVar]
        public Quaternion _netRotation;

        [SyncVar]
        int _netSegmentIndex;

        [SyncVar]
        int _netTrackIndex;

        [SyncVar]
        int _netCurrentLap;

        [SyncVar(hook = "OnShootingBulletsChanged")]
        bool _shootingBullets;

        [SyncVar]
        public bool _netRaceFinished;

        public Player Player { get { return _player; } }
        public PhysicBody PhysicBody { get { return _physicBody; } }

        GameObject _playerGO;
        Player _player;
        PhysicBody _physicBody;
        PlayerCamera _playerCamera;
        Prefabs _prefabs;
        GameSettings _gameSettings;
        Transform _minimapTr;
        Vector3 _mapAverage;
        Vector2 _mapMin;
        Vector2 _mapMax;
        Vector2 _minimapSize;
        Vector2 _minimapOffset;
        float _minimapRightAlignOffset;
        Func<Vector3, Vector2> _toMinimapCoord;
        FranticNetworkManager _manager;

        struct MinimapTargetInfo
        {
            public uint NetID;
            public MinimapTarget Target;
            public Transform Icon;
        }

        List<MinimapTargetInfo> _minimapTargets = new List<MinimapTargetInfo>();

        float _syncTimer = 0.0f;
        const float _syncFrequency = 0.1f;
        const float _lerpDuration = 0.1f;

        public override void OnStartServer()
        {
            Console.Print(string.Format("PlayerSync.OnStartServer({0})", netId.Value));
            _playerGO = transform.FindChild("Player").gameObject;
            _player = _playerGO.GetComponent<Player>();
            _player.CommonInit();
        }

        public override void OnStartClient()
        {
            Console.Print(string.Format("PlayerSync.OnStartClient({0})", netId.Value));
            _playerGO = transform.FindChild("Player").gameObject;
            _player = _playerGO.GetComponent<Player>();
            _player.CommonInit();
        }

        void Start()
        {
            Console.Print(string.Format("PlayerSync.Start() - netId: {0}", netId.Value));
            UnityEngine.Object.DontDestroyOnLoad(gameObject);
            _playerGO = transform.FindChild("Player").gameObject;
            _player = _playerGO.GetComponent<Player>();
            _physicBody = _player.Body.GetComponent<PhysicBody>();
            _netState.Pos = _playerGO.transform.position;
            _manager = FranticNetworkManager.singleton as FranticNetworkManager;
            _prefabs = _manager.LevelManager.Prefabs;
            _gameSettings = _manager.GetComponent<GameSettings>();

            if (!isLocalPlayer)
            {
                _player.enabled = false;
                _playerGO.transform.FindChild("TopLight").gameObject.SetActive(false);
                _playerGO.transform.FindChild("Spotlight").gameObject.SetActive(false);
            }
            else
            {
                var cameraGO = Instantiate(_manager.LevelManager.PlayerCamera);
                cameraGO.transform.SetParent(transform);

                // Setup render targets
                var screen = GameObject.Find("InGameScreen");
                var manager = NetworkManager.singleton as FranticNetworkManager;
                var splitScreen = screen.transform.FindChild("SplitScreen").gameObject;
                var singleScreen = screen.transform.FindChild("SingleScreen").gameObject;
                singleScreen.SetActive(!manager.IsSplitScreenGame);
                splitScreen.SetActive(manager.IsSplitScreenGame);
                var ui = GameObject.Instantiate(_prefabs.InGameUI);
                _player.InGameUI = ui;

                _playerCamera = cameraGO.GetComponent<PlayerCamera>();
                var effectsCamera = _playerCamera.EffectsCamera;
                var camera = _playerCamera.SceneCamera;
                if (manager.IsSplitScreenGame)
                {
                    var isTop = (playerControllerId == 0);
                    var targetTexture = isTop ? _gameSettings.SplitScreenTopTarget : _gameSettings.SplitScreenBottomTarget;
                    camera.targetTexture = targetTexture;
                    cameraGO.GetComponent<AudioListener>().enabled = (playerControllerId == 0);
                    cameraGO.GetComponent<Antialiasing>().enabled = false;

                    var containerName = isTop ? "TopTarget" : "BottomTarget";
                    var container = splitScreen.transform.FindChild(containerName);
                    container.GetComponent<RawImage>().enabled = true;
                    var rt = container.transform.FindChild("RenderTarget");
                    ui.transform.SetParent(rt, false);
                }
                else
                {
                    var sceneRT = new RenderTexture(Screen.width, Screen.height, 24);
                    sceneRT.name = "SceneRT";
                    var finalRT = new RenderTexture(Screen.width, Screen.height, 24);
                    finalRT.name = "FinalRT";
                    camera.targetTexture = sceneRT;
                    singleScreen.GetComponent<RawImage>().texture = finalRT;
                    effectsCamera.targetTexture = finalRT;
                    cameraGO.GetComponent<AudioListener>().enabled = true;
                    ui.transform.SetParent(screen.transform, false);

                    // dummy camera so editor doesn't display dark overlay "No camera found"
                    var dummyCamera = new GameObject("DummyCamera").AddComponent<Camera>();
                    dummyCamera.cullingMask = 0;
                    dummyCamera.clearFlags = CameraClearFlags.Nothing;
                }

                InitMiniMap();

                // FX
                //gameObject.SetLayerMask(LayerMask.NameToLayer("Player"));
            }
        }

        void InitMiniMap()
        {            
            _minimapTr = _player.InGameUI.transform.FindChild("Minimap");
            var minimap = _minimapTr.GetComponent<MiniMap>();
            var track = _manager.LevelManager.StartingTrack.GetComponent<BezierPath>();
            _mapMin = new Vector2(float.MaxValue, float.MaxValue);
            _mapMax = new Vector2(float.MinValue, float.MinValue);

            Action<Vector3> updateMinMax = (point) =>
            {
                if (point.x < _mapMin.x) _mapMin.x = point.x;
                if (point.x > _mapMax.x) _mapMax.x = point.x;
                if (point.z < _mapMin.y) _mapMin.y = point.z;
                if (point.z > _mapMax.y) _mapMax.y = point.z;
            };

            _mapAverage = Vector3.zero;
            var points = new List<Vector3>();
            var pointsThatSkipNextSegment = new List<int>();
            while (track != null)
            {
                for (int i = 0; i < track.Curves.Length; ++i)
                {
                    var hasHoleAtTheEnd = track.NextTrack != null && !track.IsAlignedToNextTrack() && i == track.Curves.Length-1;
                    var curveLength = track.GetLength(i);
                    int res = _gameSettings.MiniMapResolution;
                    int numPoints = res;
                    if (hasHoleAtTheEnd)
                        numPoints++;
                    for (int j = 0; j < numPoints; ++j)
                    {
                        var point = track.GetPoint(i, track.GetFactor(i, j * (curveLength/res)));
                        updateMinMax(point);
                        points.Add(point);
                        _mapAverage += point;

                        if (hasHoleAtTheEnd && j == numPoints - 1)
                            pointsThatSkipNextSegment.Add(points.Count-1);
                    }
                }

                track = (track.NextTrack != null) ? track.NextTrack.GetComponent<BezierPath>() : null;
                if (track && ReferenceEquals(track.gameObject, _manager.LevelManager.StartingTrack))
                    break;
            }
            _mapAverage /= points.Count;
            minimap.ClearPoints();

            // center map
            _mapMin -= _mapAverage.XZ();
            _mapMax -= _mapAverage.XZ();

            // preserve ratio
            var mapWidth = Mathf.Abs(_mapMax.x - _mapMin.x);
            var mapHeight = Mathf.Abs(_mapMax.y - _mapMin.y);
            _minimapSize = minimap.Size;
            _minimapRightAlignOffset = 0.0f;
            if (mapHeight > mapWidth)
            {
                _minimapSize.x = _minimapSize.y * (mapWidth / mapHeight);

                // make it align right
                _minimapRightAlignOffset = minimap.Size.x - _minimapSize.x;
            }
            else
            {
                _minimapSize.y = _minimapSize.x * (mapHeight / mapWidth);
            }

            var rect = minimap.GetComponent<RectTransform>();
            _minimapOffset = new Vector2(rect.pivot.x * minimap.Size.x, rect.pivot.y * minimap.Size.y);
            _toMinimapCoord = (point) =>
            {
                var centered = (point - _mapAverage).XZ();
                var deltaX = _mapMax.x - _mapMin.x;
                if(MathUtils.IsZero(deltaX))
                    deltaX = 1.0f;
                var deltaY = _mapMax.y - _mapMin.y;
                if (MathUtils.IsZero(deltaY))
                    deltaY = 1.0f;
                var minimapX = ((centered.x - _mapMin.x) / deltaX) * _minimapSize.x;
                var minimapY = ((centered.y - _mapMin.y) / deltaY) * _minimapSize.y;
                return new Vector2(minimapX + _minimapRightAlignOffset, minimapY) - _minimapOffset;
            };

            int pointThatSkipNextSegmentToConsider = 0;
            for(int i=0; i<points.Count; ++i)
            {
                bool skipNextSegment = false;
                if (pointThatSkipNextSegmentToConsider < pointsThatSkipNextSegment.Count
                 && pointsThatSkipNextSegment[pointThatSkipNextSegmentToConsider] == i)
                {
                    skipNextSegment = true;
                    ++pointThatSkipNextSegmentToConsider;
                }

                minimap.AddPoint(_toMinimapCoord(points[i]), skipNextSegment);
            }
            minimap.SetVerticesDirty();

            // Init targets
            var targets = GameObject.FindObjectsOfType<MinimapTarget>();
            foreach (var target in targets)
                AddMinimapTarget(target);
        }

        public void AddMinimapTarget(MinimapTarget target)
        {
            var isMine = false;
            var isPlayer = false;
            NetworkBehaviour netComponent = null;
            var ai = target.GetComponent<BasicAI>();
            if (ai)
            {
                netComponent = ai as NetworkBehaviour;
            }
            else
            {
                var missile = target.GetComponent<Missile>();
                if(missile)
                {
                    netComponent = missile as NetworkBehaviour;
                }
                else
                {
                    var player = target.transform.parent.GetComponent<PlayerSync>();
                    isPlayer = true;
                    isMine = ReferenceEquals(player.gameObject, gameObject);
                    netComponent = player as NetworkBehaviour;
                }
            }
            var indicator = isPlayer ? (isMine ? _prefabs.PlayerIndicator : _prefabs.OtherPlayersIndicator) : target.Icon;
            var indicatorGO = GameObject.Instantiate(indicator);
            indicatorGO.transform.SetParent(_minimapTr, false);
            if (!isMine)
                indicatorGO.transform.SetAsFirstSibling();

            MinimapTargetInfo info = new MinimapTargetInfo();
            info.NetID = netComponent.netId.Value;
            info.Target = target;
            info.Icon = indicatorGO.GetComponent<RectTransform>();
            _minimapTargets.Add(info);
        }

        public void RemoveMinimapTarget(MinimapTarget target)
        {
            var index = _minimapTargets.FindIndex(i => ReferenceEquals(i.Target, target));
            GameObject.Destroy(_minimapTargets[index].Icon.gameObject);            
            _minimapTargets.RemoveAt(index);
        }

        void UpdateMinimap()
        {            
            foreach(var target in _minimapTargets)
            {
                if (target.Target == null)
                    continue;

                target.Icon.localPosition = _toMinimapCoord(target.Target.transform.position);
                var rot = target.Target.transform.rotation.eulerAngles;
                target.Icon.localRotation = Quaternion.Euler(Vector3.forward * -rot.y);
            }
        }
       
        void Update()
        {
            if(!isLocalPlayer)
            {
                var lerpFactor = Time.deltaTime / _lerpDuration;                
                _playerGO.transform.position = Vector3.Lerp(_playerGO.transform.position, _netState.Pos, lerpFactor);
                _playerGO.transform.rotation = Quaternion.Lerp(_playerGO.transform.rotation, _netRotation, lerpFactor);
                _player.SetCurveCoords(_netState.CurvePos, _netState.CurveDist, _netTrackIndex, _netSegmentIndex);
                _physicBody.Direction = _netState.Dir;
                _physicBody.Speed = _netState.Speed;

                _player.UpdateBullets();
            }
            else
            {
                UpdateMinimap();
            }
        }       

        [Client]
        public void ClientSendTrackInfoToServer(int trackIndex, int segmentIndex)
        {
            CmdSendTrackInfoToServer(trackIndex, segmentIndex);
        }

        [Client]
        public void ClientSendStateToServer()
        {
            Debug.Assert(isLocalPlayer);

            // send critical data (TODO send less frequently?)
            NetState state;
            state.Pos = _playerGO.transform.position;
            int trackIndex;
            int segmentIndex;
            _player.GetCurveCoords(out state.CurvePos, out state.CurveDist, out trackIndex, out segmentIndex);

            //if (_physicBody.CollisionResponseTimer < 0.0f)
            {
                state.Dir = _physicBody.Direction;
                state.Speed = _physicBody.Speed;
            }
            //else
            //{
            //    state.Dir = _physicBody.PreCollisionDirection;
            //    state.Speed = _physicBody.PreCollisionSpeed;
            //}
            state.Health = _player.Health;
            CmdSendStateToServer(state);

            // send less critical data
            if (_syncTimer > _syncFrequency)
            {
                CmdSendSecondaryStateToServer(_playerGO.transform.rotation, _player.TraveledDistance, _player.LocalTraveledDistance, _player.CurrentLap, _playerCamera.RaceFinished);
                _syncTimer = 0.0f;
            }
            _syncTimer += Time.deltaTime;
        }

        [Client]
        public void ClientShoot(int cannonIndex)
        {
            if (!isServer)
            {
                var missile = SpawnMissile(cannonIndex);
                _manager.LevelManager.OnMissileLaunched(missile);
            }

            CmdShoot(cannonIndex);
        }

        [Client]
        public void ClientShootBullet(bool shoot)
        {
            if (isLocalPlayer)
                _player.ShootBullets(shoot);

            CmdShootBullet(shoot);
        }

        [Client]
        public void ClientDropMine()
        {
            CmdDropMine();
        }

        [Client]
        public void ClientDestroyNetObject(uint netId)
        {
            CmdDestroyNetObject(netId);
        }

        [Client]
        public void ClientCollideWithAI(uint netId, Vector3 postCollisionDir, float postCollisionSpeed)
        {
            CmdCollideWithAI(netId, postCollisionDir, postCollisionSpeed);
        }

        [Client]
        public void ClientObtainPowerUp(PowerUp pu)
        {
            _player.OnPowerUpCollected(pu.Type);
            ClientDestroyNetObject(pu.netId.Value);
        }

        [Command]
        void CmdDestroyNetObject(uint netId)
        {
            var obj = GameObject.FindObjectsOfType<NetworkBehaviour>().FirstOrDefault(o => o.netId.Value == netId);
            if (obj)
                NetworkServer.Destroy(obj.gameObject);
            else
                Console.Print("CmdDestroyNetObject - unknown ID: " + netId);
        }

        [Command]
        void CmdCollideWithAI(uint netId, Vector3 postCollisionDir, float postCollisionSpeed)
        {
            var ai = GameObject.FindObjectsOfType<BasicAI>().FirstOrDefault(o => o.netId.Value == netId);
            if (ai)
                ai.OnCollision(postCollisionDir, postCollisionSpeed);
            else
                Console.Print("CmdCollideWithAI - unknown ID: " + netId);
        }

        [Command]
        void CmdShoot(int cannonIndex)
        {
            var missile = SpawnMissile(cannonIndex);
            missile._netRotation = missile.transform.rotation; // FIX UNITY BUG - ONLY POSITION IS PROPAGATED ON SPAWN!!
            missile._netOwnerPlayerID = netId.Value;
            NetworkServer.Spawn(missile.gameObject);
        }

        [Command]
        void CmdShootBullet(bool shoot)
        {
            _shootingBullets = shoot;
        }

        [Command]
        void CmdDropMine()
        {
            var mineGO = Instantiate(_prefabs.Mine, _player.transform.position, _player.transform.rotation) as GameObject;
            var mine = mineGO.GetComponent<Mine>();
            mine._netRotation = _player.transform.rotation;
            mine._netPardonPlayerWithID = (int)netId.Value;
            NetworkServer.Spawn(mineGO);
        }

        [Command]
        void CmdSendStateToServer(NetState state)
        {
            _netState = state;
            _physicBody.Direction = state.Dir;
            _physicBody.Speed = state.Speed;
        }

        [Command]
        void CmdSendSecondaryStateToServer(Quaternion rot, float distance, float localDistance, int currentLap, bool raceFinished)
        {
            _netRotation = rot;
            _netTraveledDistance = distance;
            _netLocalTraveledDistance = localDistance;
            _netCurrentLap = currentLap;
            _netRaceFinished = raceFinished;
        }

        [Command]
        void CmdSendTrackInfoToServer(int trackIndex, int segmentIndex)
        {
            _netTrackIndex = trackIndex;
            _netSegmentIndex = segmentIndex;
        }

        [ClientRpc]
        public void RpcOnMissileHit()
        {
            Console.Print("RpcOnMissileHit, isLocalPlayer: " + isLocalPlayer);
            if (isLocalPlayer)
                _player.OnMissileHit();
        }

        [Client]
        void OnShootingBulletsChanged(bool value)
        {
            _shootingBullets = value;

            if(!isLocalPlayer)
                _player.ShootBullets(value);
        }

        Missile SpawnMissile(int cannonIndex)
        {
            var player = _playerGO.GetComponent<Player>();
            var missileGO = GameObject.Instantiate(_prefabs.Missile);
            var missile = missileGO.GetComponent<Missile>();
            missile.Init(player, null, cannonIndex, _physicBody.Speed);
            return missile;
        }
    }
}
