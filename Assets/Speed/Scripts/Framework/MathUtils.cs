﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public static class MathUtils
    {
        public const float Epsilon = 1e-5f;

	    public static bool Equals(float f1, float f2)
	    {
		    var diff = Mathf.Abs (f1 - f2);
            return diff < Epsilon;
	    }

	    public static bool IsZero(float f1)
	    {
		    return Equals (f1, 0.0f);
	    }

	    public static bool IsTurnSection(GameObject section)
	    {
		    var start = section.transform.FindChild ("StartPoint");
		    var exit = section.transform.FindChild ("ExitPoint");
		    var dot = Vector3.Dot (start.forward, exit.forward);
		    return IsZero (dot);
	    }

        public static bool GetBasis(Vector3 forward, out Vector3 up, out Vector3 right)
        {
            right = Vector3.Cross(Vector3.up, forward).normalized;
            if (right.sqrMagnitude < 0.9f)
            {
                up = Vector3.zero;
                return false;
            }

            up = Vector3.Cross(forward, right).normalized;
            return true;
        }

        public static Vector3 Damp(Vector3 src, Vector3 dest, float deltaTime, float duration)
	    {
		    duration = Mathf.Clamp(duration, Epsilon, duration);
		    var factor = Mathf.Clamp((deltaTime / duration), 0.0f, 1.0f);
		    return Vector3.Lerp(src, dest, factor);
	    }

		public static float Damp(float src, float dest, float deltaTime, float duration)
		{
            duration = Mathf.Clamp(duration, Epsilon, duration);
			var factor = Mathf.Clamp((deltaTime / duration), 0.0f, 1.0f);
			return Mathf.Lerp(src, dest, factor);
		}

        public static Vector2 XY(this Vector3 vec3)
        {
            return new Vector2(vec3.x, vec3.y);
        }

        public static Vector2 XZ(this Vector3 vec3)
        {
            return new Vector2(vec3.x, vec3.z);
        }

        public static Vector3 XYZ(this Vector2 vec2)
        {
            return new Vector3(vec2.x, vec2.y, 0.0f);
        }

        public static Vector2 Perpendicular(this Vector2 vec2)
        {
            return new Vector2(-vec2.y, vec2.x);
        }

        public static Vector3 DivideBy(this Vector3 vec, Vector3 other)
        {
            return new Vector3(vec.x / other.x, vec.y / other.y, vec.z / other.z);
        }

        public static Vector3 MultiplyBy(this Vector3 vec, Vector3 other)
        {
            return new Vector3(vec.x * other.x, vec.y * other.y, vec.z * other.z);
        }

        public static Matrix4x4 GetLocalMatrix(this Transform t)
        {
            return Matrix4x4.TRS(t.localPosition, t.localRotation, t.localScale);
        }

        public static void ResetLocally(this Transform t)
        {
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
            t.localScale = Vector3.one;
        }
    }
}

