﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace frantic
{
    public class Billboard : MonoBehaviour
    {
        public bool AxisAligned;
        public Camera CustomCamera;

        [HideInInspector]
        public Vector3 RotationAxis = Vector3.up;

        Plane _basePlane;

        void Start()
        {
            _basePlane = new Plane(RotationAxis, transform.parent.position);
        }

        public Quaternion UpdateRotation(Camera camera)
        {
            if (AxisAligned)
            {
                var toCamera = (camera.transform.position - transform.parent.position).normalized;
                var proj = Vector3.ProjectOnPlane(toCamera, _basePlane.normal);
                var rot = Quaternion.LookRotation(proj, RotationAxis);
                return rot;
            }
            else
            {
                return Quaternion.LookRotation(-camera.transform.forward, camera.transform.up);
            }
        }

        void OnWillRenderObject()
        {
            transform.parent.rotation = UpdateRotation(CustomCamera != null ? CustomCamera : Camera.current);
        }
    }
}

