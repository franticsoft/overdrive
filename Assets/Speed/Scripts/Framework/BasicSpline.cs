﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class BasicSpline : MonoBehaviour
    {
        public Vector3[] Points;

        void Awake()
        {
            if (Points == null)
                Reset();
        }

        void Reset()
        {
            var step = 1.0f;
            Points = new Vector3[4];
            Points[0] = Vector3.forward * step * 0.0f;
            Points[1] = Vector3.forward * step * 1.0f;
            Points[2] = Vector3.forward * step * 2.0f;
            Points[3] = Vector3.forward * step * 3.0f;
        }

        public Vector3 GetPoint(float f)
        {
            var numElements = Points.Length;
            var fIndex = f * (float)(numElements - 1);
            var srcIndex = Mathf.Clamp(Mathf.FloorToInt(fIndex), 0, numElements - 1);
            var destIndex = Mathf.Clamp(srcIndex + 1, 0, numElements - 1);
            var fFraction = fIndex - Mathf.Floor(fIndex);

            var preSrcIndex = Mathf.Clamp(srcIndex - 1, 0, numElements - 1);
            var postDestIndex = Mathf.Clamp(destIndex + 1, 0, numElements - 1);

            var p1 = Points[srcIndex];
            var p2 = Points[destIndex];
            var p0 = Points[preSrcIndex];
            var p3 = Points[postDestIndex];

            var f2 = fFraction * fFraction;
            var a0 = p0 * -0.5f + p1 * 1.5f - p2 * 1.5f + p3 * 0.5f;
            var a1 = p0 - p1 * 2.5f + p2 * 2.0f - p3 * 0.5f;
            var a2 = p0 * -0.5f + p2 * 0.5f;
            var a3 = p1;
            var p = (a0 * fFraction * f2 + a1 * f2 + a2 * fFraction + a3);
            return p;
        }
    }
}
