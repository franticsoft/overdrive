﻿using UnityEngine;
using System.Collections;
using System;

namespace frantic
{
    public class TrackSegment : MonoBehaviour
    {
        [Serializable]
        public enum Type
        {
            Flat,
            Tunnel
        }

        [Serializable]
        public enum DecoratorType
        {
            None,
            Left,
            Right,
            Both
        }

        [Serializable]
        public enum TurnIndicadorType
        {
            None,
            Left,
            Right,
        }

        public Type PathType = Type.Flat;
        //public float Radius = 1.0f;
        public bool EnterLookingUpSlope;
        public bool ExitLookingUpSlope;

        [HideInInspector]
        public DecoratorType Boost = DecoratorType.None;

        [HideInInspector]
        public DecoratorType Fences = DecoratorType.None;

        [HideInInspector]
        public DecoratorType Panels = DecoratorType.None;

        [HideInInspector]
        public TurnIndicadorType TurnIndicador = TurnIndicadorType.None;

        [HideInInspector]
        public int MeshIndex = 0;

        [HideInInspector]
        public float TunnelFlatRatio = .65f;
        [HideInInspector]
        public float TunnelRadiusRatio = .75f;
        [HideInInspector]
        public float TurnIndicatorMin = 0.0f;
        [HideInInspector]
        public float TurnIndicatorMax = 1.0f;

        [NonSerialized]
        public PowerUp PowerUp;

        public int IndexInTrack = -1;
    }
}
