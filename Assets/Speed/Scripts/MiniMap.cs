﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace frantic
{
    [AddComponentMenu("UI/MiniMap")]
    public class MiniMap : MaskableGraphic
    {
        public float LineWidth = 10.0f;
        public Vector2 Size 
        { 
            get
            {
                var rect = GetComponent<RectTransform>();
                return rect.sizeDelta;
            }
        }

        [SerializeField]
        private Texture m_Texture;

        public Texture texture
        {
            get
            {
                return this.m_Texture;
            }
            set
            {
                if (this.m_Texture == value)
                {
                    return;
                }
                this.m_Texture = value;
                this.SetVerticesDirty();
                this.SetMaterialDirty();
            }
        }

        public override Texture mainTexture
        {
            get
            {
                if (!(this.m_Texture == null))
                {
                    return this.m_Texture;
                }
                if (this.material != null && this.material.mainTexture != null)
                {
                    return this.material.mainTexture;
                }
                return Graphic.s_WhiteTexture;
            }
        }

        List<Vector2> _points = new List<Vector2>();
        List<int> _segmentsToSkip = new List<int>();

        protected MiniMap()
        {
            base.useLegacyMeshGeneration = false;
        }

        public void ClearPoints()
        {
            _points.Clear();
            _segmentsToSkip.Clear();
        }

        public void AddPoint(Vector2 point, bool skipNextSegment)
        {
            if (skipNextSegment)
                _segmentsToSkip.Add(_points.Count);

            _points.Add(point);
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();

            if (_points.Count < 3)
                return;

            //int currentSegmentToConsiderForSkipping = 0;
            //var uv0 = new Vector2(0.0f, 0.0f);
            //var uv1 = new Vector2(1.0f, 0.0f);
            //var uv2 = new Vector2(1.0f, 1.0f);
            //var uv3 = new Vector2(0.0f, 1.0f);
            //for (int i = 0; i < _points.Count; ++i)
            //{
            //    if (currentSegmentToConsiderForSkipping < _segmentsToSkip.Count
            //    && _segmentsToSkip[currentSegmentToConsiderForSkipping] == i)
            //    {
            //        ++currentSegmentToConsiderForSkipping;
            //        continue;
            //    }

            //    var v1 = _points[i];
            //    var v2 = (i == _points.Count - 1) ? _points[0] : _points[i + 1];
            //    var dir1 = (v2 - v1).normalized;
            //    var dir2 = dir1;
            //    if (i == _points.Count - 1)
            //    {
            //        var v3 = _points[1];
            //        dir2 = (v3 - v2).normalized;
            //    }
            //    else if (i + 2 < _points.Count)
            //    {
            //        var v3 = _points[i + 2];
            //        dir2 = (v3 - v2).normalized;
            //    }
            //    var vLateralAxe1 = dir1.Perpendicular();
            //    var vLateralAxe2 = dir2.Perpendicular();

            //    var w = LineWidth / 2.0f;
            //    vh.AddVert((v1 - vLateralAxe1 * w), base.color, uv0);
            //    vh.AddVert((v1 + vLateralAxe1 * w), base.color, uv1);
            //    vh.AddVert((v2 + vLateralAxe2 * w), base.color, uv2);
            //    vh.AddVert((v2 - vLateralAxe2 * w), base.color, uv3);

            //    var idx = vh.currentVertCount-4;
            //    vh.AddTriangle(idx, idx + 1, idx + 2);
            //    vh.AddTriangle(idx, idx + 2, idx + 3);
            //}

            var uv0 = Vector2.zero;
            var uv1 = new Vector2(1.0f, 0.0f);
            for (int i = 0; i < _points.Count; ++i)
            {
                var v1 = _points[i];
                var v2 = (i == _points.Count - 1) ? _points[0] : _points[i + 1];
                var dir1 = (v2 - v1).normalized;
                var vLateralAxe1 = dir1.Perpendicular();

                var w = LineWidth / 2.0f;
                vh.AddVert((v1 - vLateralAxe1 * w), base.color, uv0);
                vh.AddVert((v1 + vLateralAxe1 * w), base.color, uv1);
            }

            int currentSegmentToConsiderForSkipping = 0;
            for (int i = 0; i < _points.Count; ++i)
            {
                if (currentSegmentToConsiderForSkipping < _segmentsToSkip.Count
                && _segmentsToSkip[currentSegmentToConsiderForSkipping] == i)
                {
                    ++currentSegmentToConsiderForSkipping;
                    continue;
                }

                var idx = i * 2;
                var next2 = idx + 3;
                var next1 = idx + 2;
                if (i == _points.Count - 1)
                {
                    next2 = 1;
                    next1 = 0;
                }
                vh.AddTriangle(idx, idx + 1, next2);
                vh.AddTriangle(idx, next2, next1);
            }
        }
    }
}

