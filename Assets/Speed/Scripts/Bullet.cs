﻿using UnityEngine;

namespace frantic
{
    public class Bullet : MonoBehaviour
    {
        void Start()
        {
        }

        Vector2 _curvePos;
        float _curveDist;
        BezierPath _track;
        int _curveIndex;
        float _speed;
        Transform _cannon;
        Vector3 _forward;

        Prefabs _prefabs;
        Player _ownerPlayer;
        BasicAI _ownerAI;
        FranticNetworkManager _manager;
        GameSettings _gameSettings;
        float _life;
        float _maxSpeed;
        float _height;

        public void Init(Player player, BasicAI ai, int cannonIndex, float speed)
        {
            _manager = FranticNetworkManager.singleton as FranticNetworkManager;
            _life = 0.0f;
            _gameSettings = _manager.GetComponent<GameSettings>();
            _ownerPlayer = player;
            _ownerAI = ai;
            var ownerTransform = player != null ? player.transform : ai.transform;
            var ownerBodyTransform = player != null ? player.Body.transform : ai.transform;
            var bodyGO = ownerBodyTransform.gameObject;
            var weapon = bodyGO.GetComponent<Weapon>();
            var cannon = cannonIndex == 0 ? weapon.LeftCannon : weapon.RightCannon;
            _cannon = cannon;
            var cannonParent = cannon.parent;
            var body = bodyGO.transform;
            _height = body.parent.localPosition.y;

            if (player)
            {
                player.GetCurveCoords(out _curvePos, out _curveDist, out _track, out _curveIndex);
            }                
            else
                ai.GetCurveCoords(out _curvePos, out _curveDist, out _track, out _curveIndex);

            var cannonAbsPos = cannon.position;
            var playerAbsPos = ownerTransform.position + ownerTransform.up * _height;
            Missile.GetCurvePos(_track, _curveIndex, _curvePos, _curveDist, playerAbsPos, cannonAbsPos, out _curvePos, out _curveDist, out _curveIndex, out _track);

            var currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();
            var tangent = _track.GetTangent(_curveIndex, _curvePos.y).normalized;
            var up = _track.Curves[_curveIndex].GetUp(_curvePos.y);
            var right = Vector3.Cross(up, tangent);
            var banking = Quaternion.AngleAxis(_track.GetBanking(_curveIndex, _curvePos.y), tangent);
            right = banking * right;
            up = banking * up;

            // update transform
            var curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
            var curveRadius = _manager.LevelManager.TrackRadius;
            var lateral0 = curvePoint - right * curveRadius;
            _forward = ownerBodyTransform.forward;
            transform.position = lateral0 + right * _curvePos.x + up * _height;
            //transform.rotation = Quaternion.LookRotation(_forward, up);

            _maxSpeed = _gameSettings.BulletMaxSpeed;
            _speed = speed;
            if (speed > _maxSpeed)
                _maxSpeed = speed;

            _prefabs = _manager.LevelManager.Prefabs;
        }

        public bool CollidesWith(Vector3 position, float radius)
        {
            if (Vector3.Distance(transform.position, position) < radius)
                return true;
            return false;
        }

        public void Kill()
        {
            Destroy(gameObject);
        }

        void Update()
        {
            if (_manager.LevelManager.IsPaused)
                return;

            _life += Time.deltaTime;
            if (_life > _gameSettings.BulletLife)
            {
                Kill();
                return;
            }

            _speed += _gameSettings.BulletAccel;
            if (_speed > _maxSpeed)
                _speed = _maxSpeed;

            var tangent = _track.GetTangent(_curveIndex, _curvePos.y).normalized;
            var fwdFactor = Vector3.Dot(tangent, _forward);
            _curveDist += _speed * fwdFactor * _manager.LevelManager.AverageDT;
            var curveLength = _track.GetLength(_curveIndex);
            if (_curveDist > curveLength)
            {
                if (_curveIndex == (_track.Curves.Length - 1) && _track.NextTrack != null)
                {
                    var thisTrack = _track;
                    _track = _track.NextTrack.GetComponent<BezierPath>();
                    _curveIndex = 0;

                    if (!thisTrack.IsAlignedToNextTrack())
                    {
                        // TODO bullet free-fly
                        Debug.Assert(false);
                        return;
                    }
                    else
                    {
                        _curveDist = _curveDist - curveLength;
                    }
                }
                else
                {
                    _curveDist = _curveDist - curveLength;
                    _curveIndex = (_curveIndex + 1) % _track.Curves.Length;
                }
            }
            else if (_curveDist < 0.0f)
            {
                if (_track.GetPreviousTrack() != null)
                {
                    if (_track.IsAlignedToPreviousTrack())
                    {
                        _track = _track.GetPreviousTrack().GetComponent<BezierPath>();
                        _curveIndex = _track.Curves.Length - 1;
                        curveLength = _track.GetLength(_curveIndex);
                        _curveDist = curveLength + _curveDist;
                    }
                    else
                    {
                        // TODO bullet free-fly
                        Debug.Assert(false);
                        return;
                    }
                }
                else
                {
                    _curveIndex--;
                    if (_curveIndex < 0)
                        _curveIndex = _track.Curves.Length - 1;
                    curveLength = _track.GetLength(_curveIndex);
                    _curveDist = curveLength + _curveDist;
                }
            }

            var factor = _track.GetFactor(_curveIndex, _curveDist);
            _curvePos.y = factor;

            var curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
            var up = _track.Curves[_curveIndex].GetUp(_curvePos.y);
            var right = Vector3.Cross(up, tangent);
            var banking = Quaternion.AngleAxis(_track.GetBanking(_curveIndex, _curvePos.y), tangent);
            right = banking * right;
            up = banking * up;

            // get tip curve coords and check collision
            Vector2 bulletTipPos;
            var bulletTipIndex = _curveIndex;
            var bulletTipTrack = _track;
            var bulletTipDist = _curveDist;
            var absPos = transform.position;
            var bulletLength = 1.0f; // GetComponent<MeshFilter>().sharedMesh.bounds.size.y;
            var tipAbsPos = absPos + _forward * bulletLength;
            Missile.GetCurvePos(_track, _curveIndex, _curvePos, _curveDist, absPos, tipAbsPos, out bulletTipPos, out bulletTipDist, out bulletTipIndex, out bulletTipTrack);
            var currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();
            var lateralFactor = Vector3.Dot(right, _forward);
            var curveRadius = _manager.LevelManager.TrackRadius;
            var collisionDistToBorder = 0.0f;
            var maxX = (curveRadius * 2.0f) - collisionDistToBorder;
            var deltaX = _speed * lateralFactor * _manager.LevelManager.AverageDT;
            if (bulletTipPos.x + deltaX >= 0.0f && bulletTipPos.x + deltaX <= maxX)
            {
                _curvePos.x += deltaX;
                var body = _cannon.parent;

                // adjust forward depending on up vector, by projecting forward onto base plane
                var plane = new Plane(up, transform.position);
                var rayOrigin = transform.position + _forward;
                var rayDir = plane.GetSide(rayOrigin) ? -up : up;
                var ray = new Ray(rayOrigin, rayDir);
                float toPlane = 0.0f;
                if (plane.Raycast(ray, out toPlane))
                {
                    var intersection = ray.origin + ray.direction * toPlane;
                    _forward = (intersection - transform.position).normalized;
                }             

                var lateral0 = curvePoint - right * curveRadius;
                if (body.parent.localPosition.y < _height)
                    _height = body.parent.localPosition.y;
                transform.position = lateral0 + right * _curvePos.x + up * _height;
                //transform.rotation = Quaternion.LookRotation(_forward, up);
            }
            else
            {
                Kill();
                return;
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            var parentGo = collision.transform.parent;
            var ai = parentGo != null ? parentGo.GetComponent<BasicAI>() : null;
            if(ai)
            {
                ai.TakeDamage(_gameSettings.BulletDamage);
                Kill();
            }
        }
    }
}

