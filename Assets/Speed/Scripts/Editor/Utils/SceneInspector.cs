﻿using UnityEditor;
using System.IO;

namespace frantic
{
    public class SceneInspector
    {
        public static string[] Init()
        {
            var guids = AssetDatabase.FindAssets("t:Scene", new string[] { "Assets/Speed/Scenes" });
            var scenes = new string[guids.Length];
            for (int i = 0; i < guids.Length; ++i)
                scenes[i] = Path.GetFileNameWithoutExtension(AssetDatabase.GUIDToAssetPath(guids[i]));
            return scenes;
        }

        public static string EditSceneProperty(UnityEngine.Object obj, string name, string[] scenes, string currentScene)
        {
            int selected = 0;
            for (int i = 0; i < scenes.Length; ++i)
                if (scenes[i].Equals(currentScene))
                {
                    selected = i;
                    break;
                }
            EditorGUI.BeginChangeCheck();
            var index = EditorGUILayout.Popup(name, selected, scenes);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(obj, "Edit scene property");
                selected = index;
            }
            return scenes[selected];
        }
    }
}

