﻿using System;
using UnityEditor;
using UnityEngine;

namespace frantic
{
    [InitializeOnLoad]
    public static class PrefabSaver
    {
        private static bool _waiting;
        private static DateTime _lastTime;

        static PrefabSaver()
        {
            PrefabUtility.prefabInstanceUpdated -= PrefabInstanceUpdated;
            PrefabUtility.prefabInstanceUpdated += PrefabInstanceUpdated;
            EditorApplication.update -= Update;
            EditorApplication.update += Update;
        }

        private static void PrefabInstanceUpdated(GameObject instance)
        {
            _waiting = true;
            _lastTime = DateTime.Now;
        }

        private static void Update()
        {
            if (!_waiting || DateTime.Now - _lastTime < TimeSpan.FromSeconds(0.25))
                return;

            AssetDatabase.SaveAssets();
            Debug.Log("Prefab change detected. Project saved.");
            _waiting = false;
        }
    }
}
