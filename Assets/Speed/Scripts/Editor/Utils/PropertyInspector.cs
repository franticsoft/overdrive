﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq;

namespace frantic
{
    class PropertyInspector
    {
        public static void EditValueVariables(UnityEngine.Object obj, string[] names)
        {
            EditorGUI.BeginChangeCheck();
            var results = new List<object>();
            var fields = new List<FieldInfo>();
            for(int i=0; i<names.Length; ++i)
            {
                try
                {
                    var field = obj.GetType().GetField(names[i]);
                    var methods = typeof(EditorGUILayout).GetMethods().Where(
                        m => m.GetParameters().Length == 3
                        && m.GetParameters()[0].ParameterType == typeof(string)
                        && m.GetParameters()[1].ParameterType == field.FieldType
                        && m.GetParameters()[2].ParameterType == typeof(GUILayoutOption[]));
                    var result = methods.First().Invoke(null, new object[] { names[i], field.GetValue(obj), null });
                    fields.Add(field);
                    results.Add(result);
                }
                catch(Exception)
                {
                    Debug.LogWarning("Can't handle variable: " + names[i]);
                }
            }
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(obj, "Edit " + obj.GetType().ToString());
                for (int i = 0; i < fields.Count; ++i)
                    fields[i].SetValue(obj, results[i]);
                EditorUtility.SetDirty(obj);
            }
        }
    }
}

