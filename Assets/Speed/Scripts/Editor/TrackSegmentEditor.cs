﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;

namespace frantic
{
    [CustomEditor(typeof(TrackSegment))]
    public class TrackSegmentEditor : Editor
    {
        string[] _segments;
        void OnEnable()
        {
            var segment = target as TrackSegment;
            if (segment.transform.parent == null)
                return;
            var track = segment.transform.parent.GetComponent<BezierPath>();
            var types = track.Meshes;
            _segments = new string[types.Length];
            for (int i = 0; i < types.Length; ++i)
                _segments[i] = (types[i] != null) ? types[i].name : "null";
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            var segment = target as TrackSegment;

            if (segment.transform.parent == null)
                return;

            var track = segment.transform.parent.GetComponent<BezierPath>();

            // decorators
            EditorGUI.BeginChangeCheck();
            var boost = EditorGUILayout.EnumPopup("Boost", segment.Boost);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(segment, "Change boost");
                segment.Boost = (TrackSegment.DecoratorType)boost;
                track.UpdateSegment(segment);
            }

            EditorGUI.BeginChangeCheck();
            var fences = EditorGUILayout.EnumPopup("Fences", segment.Fences);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(segment, "Change fences");
                segment.Fences = (TrackSegment.DecoratorType)fences;
                track.UpdateSegment(segment);
            }

            EditorGUI.BeginChangeCheck();
            var panels = EditorGUILayout.EnumPopup("Panels", segment.Panels);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(segment, "Change panels");
                segment.Panels = (TrackSegment.DecoratorType)panels;
                track.UpdateSegment(segment);
            }

            EditorGUI.BeginChangeCheck();
            var turnIndicator = EditorGUILayout.EnumPopup("TurnIndicador", segment.TurnIndicador);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(segment, "Change TurnIndicador");
                segment.TurnIndicador = (TrackSegment.TurnIndicadorType)turnIndicator;

                // always make sure there is a fence behind a turn indicator
                if (segment.TurnIndicador != TrackSegment.TurnIndicadorType.None)
                {
                    if (segment.Fences == TrackSegment.DecoratorType.None)
                        segment.Fences = segment.TurnIndicador == TrackSegment.TurnIndicadorType.Left ? TrackSegment.DecoratorType.Left : TrackSegment.DecoratorType.Right;
                    else
                    {
                        var leftTurnIndicator = segment.TurnIndicador == TrackSegment.TurnIndicadorType.Left;
                        var leftFence = segment.Fences == TrackSegment.DecoratorType.Left;
                        if (leftTurnIndicator != leftFence)
                            segment.Fences = TrackSegment.DecoratorType.Both;
                    }
                }

                track.UpdateSegment(segment);
            }

            if(segment.TurnIndicador != TrackSegment.TurnIndicadorType.None)
            {
                EditorGUI.BeginChangeCheck();
                var turnIndicatorMin = EditorGUILayout.FloatField("TurnIndicadorMin", segment.TurnIndicatorMin);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(segment, "Change TurnIndicadorMin");
                    segment.TurnIndicatorMin = turnIndicatorMin;
                    track.UpdateSegment(segment);
                }

                EditorGUI.BeginChangeCheck();
                var turnIndicatorMax = EditorGUILayout.FloatField("TurnIndicatorMax", segment.TurnIndicatorMax);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(segment, "Change TurnIndicatorMax");
                    segment.TurnIndicatorMax = turnIndicatorMax;
                    track.UpdateSegment(segment);
                }
            }

            // show type selector
            EditorGUI.BeginChangeCheck();
            var index = EditorGUILayout.Popup("Segment Type", segment.MeshIndex, _segments);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(segment, "Change segment");
                segment.MeshIndex = index;
                track.UpdateSegment(segment);
            }

            if (segment.PathType == TrackSegment.Type.Tunnel)
            {
                frantic.PropertyInspector.EditValueVariables(segment, new string[] 
                {
                    "TunnelFlatRatio", 
                    "TunnelRadiusRatio"
                });
            }
        }
    }
}

