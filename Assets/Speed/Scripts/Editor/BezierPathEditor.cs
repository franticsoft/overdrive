﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System;

namespace frantic
{
    [CustomEditor(typeof(BezierPath))]
    public class BezierPathEditor : Editor
    {
        const float handleSize = 0.04f;
        const float pickSize = 0.06f;
        const float segmentsPerCurve = 40;

        bool _lockTangents = true;
        bool _lockTangentLengths = false;
        bool _showTangents = false;
        int _selectedCurveIndex = -1;
        int _selectedPointIndex = -1;
        int _otherCurveToUpdate = -1;
        BezierPath _otherTrackToUpdate;

        void OnSceneGUI()
        {
            var path = target as BezierPath;
            if (path.Curves == null)
            {
                return;
            }

            for (int i = 0; i < path.Curves.Length; ++i)
            {
                DrawCurve(i);

                if (path.Curves[i].Points == null)
                    continue;

                for (int j = 0; j < path.Curves[i].Points.Length; ++j)
                    HandlePoint(i, j);
            }
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var path = target as BezierPath;

            GUILayout.BeginHorizontal();

            if (path.Meshes != null)
                GUILayout.Label("Segment types: " + path.Meshes.Length);

            if (GUILayout.Button("+"))
                path.AddSegmentType();

            if (path.Meshes != null && path.Meshes.Length > 0 && GUILayout.Button("-"))
                path.RemoveSegmentType();

            GUILayout.EndHorizontal();

            if (path.Meshes == null)
                return;

            for (int i = 0; i < path.Meshes.Length; ++i)
            {
                GUILayout.BeginHorizontal();
                EditorGUI.BeginChangeCheck();
                var mesh = EditorGUILayout.ObjectField(path.Meshes[i], typeof(GameObject), false) as GameObject;
                if (EditorGUI.EndChangeCheck())
                {
                    if (mesh != null)
                    {
                        var wasEmpty = !path.Meshes.Any(m => m != null);
                        path.Meshes[i] = mesh;
                        if (wasEmpty)
                            path.Clear();
                        path.UpdateCurveFactors(i);
                    }
                }

                if (!path.IsLooping && !path.IsAlignedToNextTrack())
                {
                    if (path.Meshes[i] != null && GUILayout.Button("+"))
                    {
                        Undo.RecordObject(path, "Edit Path");
                        path.AddPiece(i);
                    }                        
                }
                GUILayout.EndHorizontal();
            }

            if (path.transform.childCount > 1 && !(path.IsLooping || path.IsAlignedToNextTrack()))
            {
                if (GUILayout.Button("RemoveLastSegment"))
                    path.RemoveLast();
            }

            GUILayout.BeginHorizontal();
            if (path.NextTrack == null && path.Curves != null)
            {
                if (path.IsLooping)
                {
                    if (GUILayout.Button("BreakLoop"))
                        path.BreakLoop();
                }
                else
                {
                    if (path.Curves.Length > 2)
                    {
                        if (GUILayout.Button("MakeLooping"))
                            path.MakeLooping();
                    }
                }
            }
            else
            {
                if (!path.IsAlignedToNextTrack())
                {
                    if (GUILayout.Button("AlignToNext"))
                    {
                        Undo.RecordObject(path.Curves, "Edit Path");
                        Undo.RecordObject(path, "Edit Path");
                        path.AlignToNext();
                    }
                }
                else
                {
                    if (GUILayout.Button("BreakFromNext"))
                    {
                        Undo.RecordObject(path.Curves, "Edit Path");
                        Undo.RecordObject(path, "Edit Path");
                        path.BreakFromNext();
                    }
                }
            }

            if (GUILayout.Button("Reset"))
                path.Clear();

            if (GUILayout.Button("UpdateMesh"))
            {
                Undo.RecordObject(path, "Update Meshes");
                path.UpdateMeshes();
            }

            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();

            if (path.NextTrack == null && path.Curves != null && !path.IsLooping)
            {
                if (GUILayout.Button("CreateNewTrack"))
                    path.CreateNext();
            }

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Tangents:");
            _lockTangents = GUILayout.Toggle(_lockTangents, "Lock");
            _lockTangentLengths = GUILayout.Toggle(_lockTangentLengths, "Lock Lengths");
            var showTangents = GUILayout.Toggle(_showTangents, "Show");
            if (showTangents != _showTangents)
            {
                _showTangents = showTangents;
                SceneView.RepaintAll();
            }
            GUILayout.EndHorizontal();

            if (_selectedCurveIndex >= 0 && _selectedPointIndex >= 0 && _selectedCurveIndex < path.Curves.Length)
            {
                var point = path.Curves[_selectedCurveIndex].Points[_selectedPointIndex];
                GUILayout.Label("Selected Point");
                EditorGUI.BeginChangeCheck();
                var pointPos = EditorGUILayout.Vector3Field("Position", point.Position);
                if (EditorGUI.EndChangeCheck())
                {
                    UpdatePointPosition(_selectedCurveIndex, _selectedPointIndex, pointPos);
                }

                var isControlPoint = _selectedPointIndex % 3 == 0;
                if (isControlPoint)
                {
                    EditorGUI.BeginChangeCheck();
                    var banking = EditorGUILayout.FloatField("Banking", point.Banking);
                    var flatBanking = EditorGUILayout.Toggle("FlatBanking", point.FlatBanking);
                    var jumpSpot = EditorGUILayout.Toggle("JumpSpot", point.JumpSpot);                    
                    if (EditorGUI.EndChangeCheck())
                    {
                        UpdatePoint(_selectedCurveIndex, _selectedPointIndex, banking, flatBanking, jumpSpot);
                    }
                }
            }
        }

        void DrawCurve(int curveIndex)
        {
            var path = target as BezierPath;
            var curve = path.Curves[curveIndex];
            var curPos = path.GetPoint(curveIndex, 0.0f);
            Vector3 curTangent = Vector3.up;
            Vector3 curUp = Vector3.up;
            if (_showTangents)
            {
                curTangent = path.GetTangent(curveIndex, 0.0f).normalized;
                curUp = Quaternion.AngleAxis(curve.GetBanking(0.0f), curTangent) * curve.InitialUp;
            }                
            var radius = path.transform.localScale.x;
            for (int i = 1; i <= segmentsPerCurve; ++i)
            {
                var t = (float)i / segmentsPerCurve;
                var nextPos = path.transform.TransformPoint(curve.GetPoint(t));
                Handles.color = Color.red;
                Handles.DrawLine(curPos, nextPos);

                if (_showTangents)
                {
                    Handles.color = Color.green;
                    //Handles.DrawLine(curPos, curPos + curTangent * 5.0f);
                    //Handles.DrawLine(curPos, curPos + curUp * 5.0f);
                    var right = Vector3.Cross(curUp, curTangent);
                    var lateral0 = curPos - right * radius;
                    Handles.DrawLine(lateral0, lateral0 + right * radius * 2.0f);

                    curTangent = path.GetTangent(curveIndex, t).normalized;
                    curUp = Quaternion.AngleAxis(curve.GetBanking(t), curTangent) * curve.GetUp(t);
                }

                curPos = nextPos;
            }

            if (_showTangents)
            {
                Handles.DrawLine(curPos, curPos + curTangent * 5.0f);
                Handles.DrawLine(curPos, curPos + curUp * 5.0f);
            }
        }

        void HandlePoint(int curveIndex, int pointIndex)
        {
            var path = target as BezierPath;
            var isStartPoint = pointIndex == 0;
            var curve = path.Curves[curveIndex];
            var point = curve.Points[pointIndex];

            // draw tangents
            var isTangent = pointIndex % 3 != 0;
            if (!isTangent)
            {
                Handles.color = Color.gray;
                var tangent = isStartPoint ? curve.Points[1] : curve.Points[2];
                Handles.DrawLine(path.transform.TransformPoint(point.Position), path.transform.TransformPoint(tangent.Position));
            }

            // don't show start points of curves above the first one
            if (curveIndex > 0 && isStartPoint)
                return;

            // hide start point of first curve when curves are connected
            if ((path.IsLooping || path.IsAlignedToPreviousTrack()) && (curveIndex == 0 && isStartPoint))
                return;

            // hide fist tangent of first curve, and last tangent of last curve if the track is looping
            var isFirstTangentOfFirstCurve = (curveIndex == 0 && pointIndex == 1);
            var isLastTangentOfLastCurve = (path.IsLooping && (curveIndex == path.Curves.Length-1) && pointIndex == 2);
            if (isFirstTangentOfFirstCurve || isLastTangentOfLastCurve)
                return;

            if (isTangent)
                Handles.color = Color.blue;
            else
                Handles.color = Color.red;

            var local = point.Position;
            var absolute = path.transform.TransformPoint(local);
            float size = HandleUtility.GetHandleSize(absolute);
            if (Handles.Button(absolute, Quaternion.identity, handleSize * size, pickSize * size, Handles.DotCap))
            {
                _selectedCurveIndex = curveIndex;
                _selectedPointIndex = pointIndex;
                Repaint();
            }

            if (_selectedCurveIndex == curveIndex && _selectedPointIndex == pointIndex)
            {
                DoPosition(curveIndex, pointIndex);
            }
        }

        void UpdatePoint(int curveIndex, int pointIndex, float banking, bool flatBanking, bool jumpSpot)
        {
            var path = target as BezierPath;
            Undo.RecordObject(path.Curves, "Edit Curve");
            Undo.RecordObject(path, "Edit Curve");
            var curve = path.Curves[curveIndex];
            var point = curve.Points[pointIndex];
            point.Banking = banking;
            point.FlatBanking = flatBanking;
            point.JumpSpot = jumpSpot;

            // update surrounding curves
            if (pointIndex == 3)
            {                
                if(curveIndex + 1  < path.Curves.Length)
                {
                    var otherCurveIndex = (curveIndex + 1);
                    var forceZeroBanking = MathUtils.Equals(Mathf.Abs(banking), 360.0f);
                    var nextBanking = forceZeroBanking ? 0.0f : banking;
                    path.Curves[otherCurveIndex].Points[0].Banking = nextBanking;
                    path.Curves[otherCurveIndex].Points[0].FlatBanking = flatBanking;
                    path.Curves[otherCurveIndex].Points[0].JumpSpot = jumpSpot;
                }
            }

            DetermineOtherCurveToUpdate(_selectedCurveIndex, _selectedPointIndex);
            path.UpdateMesh(curveIndex);
            if (_otherCurveToUpdate >= 0)
            {
                Undo.RecordObject(_otherTrackToUpdate, "Edit Curve");
                _otherTrackToUpdate.UpdateMesh(_otherCurveToUpdate);
            }                
        }

        void DoPosition(int curveIndex, int pointIndex)
        {
            var path = target as BezierPath;
            var curve = path.Curves[curveIndex];
            var point = curve.Points[pointIndex];
            var local = point.Position;
            var absolute = path.transform.TransformPoint(local);
            var factor = (float)pointIndex / 3.0f;
            var curveDir = curve.GetTangent(factor).normalized;
            Vector3 up, right;
            MathUtils.GetBasis(curveDir, out up, out right);
            up = Quaternion.AngleAxis(point.Banking, curveDir) * up;
            right = Quaternion.AngleAxis(point.Banking, curveDir) * right;
            var lookAt = Quaternion.LookRotation(curveDir, up);
            EditorGUI.BeginChangeCheck();
            absolute = Handles.DoPositionHandle(absolute, lookAt);
            if (EditorGUI.EndChangeCheck())
            {
                UpdatePointPosition(curveIndex, pointIndex, path.transform.InverseTransformPoint(absolute));
            }
        }

        void UpdatePointPosition(int curveIndex, int pointIndex, Vector3 localPos)
        {
            var path = target as BezierPath;
            var curve = path.Curves[curveIndex];
            var point = curve.Points[pointIndex];
            var isEndPoint = (pointIndex == 3);
            Undo.RecordObject(path.Curves, "Translate Point");
            var oldPosition = point.Position;
            point.Position = localPos;
            EditorUtility.SetDirty(path);

            DetermineOtherCurveToUpdate(curveIndex, pointIndex);
            BezierCurve otherCurve = null;
            if (_otherCurveToUpdate >= 0)
                otherCurve = path.Curves[_otherCurveToUpdate];

            // constrain tangents            
            var isTangent = pointIndex % 3 != 0;
            if (isTangent)
            {
                var isTangentIn = (pointIndex == 1);
                if (isTangentIn)
                {
                    Debug.Assert(curveIndex > 0);
                    var pointIn = curve.Points[0];
                    var previousTangentOut = path.Curves[_otherCurveToUpdate].Points[2];
                    ConstraintTangent(pointIn.Position, ref point, ref previousTangentOut, oldPosition);

                    var oldInialUp = curve.InitialUp;
                    curve.InitialUp = curve.GetNonStableUp(0.0f);
                    if (Vector3.Dot(oldInialUp, curve.InitialUp) < 0.0f)
                        curve.InitialUp *= -1.0f;
                }
                else
                {
                    if (curveIndex < path.Curves.Length - 1)
                    {
                        var pointOut = curve.Points[3];
                        var nextTangentIn = path.Curves[_otherCurveToUpdate].Points[1];
                        ConstraintTangent(pointOut.Position, ref point, ref nextTangentIn, oldPosition);
                    }

                    if (path.Curves.Length > 1 && curveIndex == path.Curves.Length - 1)
                    {
                        Debug.Assert(!path.IsLooping);
                        if (path.IsAlignedToNextTrack())
                        {
                            Undo.RecordObject(_otherTrackToUpdate.Curves, "Translate Point");
                            var pointOut = curve.Points[3];
                            var nextTangentIn = _otherTrackToUpdate.Curves[_otherCurveToUpdate].Points[1];
                            ConstraintTangent(pointOut.Position, ref point, ref nextTangentIn, oldPosition);

                            // convert to local pos
                            nextTangentIn.Position = _otherTrackToUpdate.ToLocalPos(nextTangentIn.Position, path);
                        }
                    }

                    if (otherCurve != null)
                    {
                        var oldInialUp = otherCurve.InitialUp;
                        otherCurve.InitialUp = otherCurve.GetNonStableUp(0.0f);
                        if (Vector3.Dot(oldInialUp, otherCurve.InitialUp) < 0.0f)
                            otherCurve.InitialUp *= -1.0f;
                    }
                }
            }
            else
            {
                if (isEndPoint)
                {
                    // Move previous tangent
                    var previousTangentOut = path.Curves[curveIndex].Points[2];
                    var oldToPreviousTangentOut = previousTangentOut.Position - oldPosition;
                    var newPos = point.Position + oldToPreviousTangentOut;
                    path.Curves[curveIndex].Points[2].Position = newPos;

                    if (!ReferenceEquals(_otherTrackToUpdate, path))
                        Undo.RecordObject(_otherTrackToUpdate.Curves, "Translate Point");

                    if (_otherCurveToUpdate >= 0)
                    {
                        var nextPoint = _otherTrackToUpdate.Curves[_otherCurveToUpdate].Points[0];
                        oldPosition = nextPoint.Position;
                        nextPoint.Position = _otherTrackToUpdate.ToLocalPos(point.Position, path);

                        // Move first tangent of next curve
                        var nextTangentIn = _otherTrackToUpdate.Curves[_otherCurveToUpdate].Points[1];
                        var oldToNextTangentIn = nextTangentIn.Position - oldPosition;
                        newPos = nextPoint.Position + oldToNextTangentIn;
                        _otherTrackToUpdate.Curves[_otherCurveToUpdate].Points[1].Position = newPos;
                    }
                }
                else
                {
                    // Move first tangent
                    var tangentIn = path.Curves[curveIndex].Points[1];
                    var oldToTangentIn = tangentIn.Position - oldPosition;
                    var newPos = point.Position + oldToTangentIn;
                    path.Curves[curveIndex].Points[1].Position = newPos;
                }
            }

            Undo.RecordObject(path, "Translate Point");
            path.UpdateMesh(curveIndex);
            if (_otherCurveToUpdate >= 0)
            {
                Undo.RecordObject(_otherTrackToUpdate, "Translate Point");
                _otherTrackToUpdate.UpdateMesh(_otherCurveToUpdate);
            }
        }

        void DetermineOtherCurveToUpdate(int curveIndex, int pointIndex)
        {
            var path = target as BezierPath;
            _otherCurveToUpdate = -1;
            _otherTrackToUpdate = path;
            var isTangent = pointIndex % 3 != 0;
            if (isTangent)
            {
                var isTangentIn = (pointIndex == 1);
                if (isTangentIn)
                {
                    if (curveIndex > 0)
                    {
                        _otherCurveToUpdate = curveIndex - 1;
                    }
                    else if (curveIndex == 0)
                    {
                        if (path.IsLooping)
                        {
                            _otherCurveToUpdate = path.Curves.Length - 1;
                        }
                        else if (path.IsAlignedToPreviousTrack())
                        {
                            _otherTrackToUpdate = path.GetPreviousTrack().GetComponent<BezierPath>();
                            _otherCurveToUpdate = _otherTrackToUpdate.Curves.Length - 1;
                        }
                    }
                }
                else
                {
                    if (curveIndex < path.Curves.Length - 1)
                    {
                        _otherCurveToUpdate = curveIndex + 1;
                    }
                    else if (path.Curves.Length > 1 && curveIndex == path.Curves.Length - 1)
                    {
                        if (path.IsLooping)
                        {
                            _otherCurveToUpdate = 0;
                        }
                        else if (path.IsAlignedToNextTrack())
                        {
                            _otherTrackToUpdate = path.NextTrack.GetComponent<BezierPath>();
                            _otherCurveToUpdate = 0;
                        }
                    }
                }
            }
            else
            {
                var isEndPoint = (pointIndex == 3);
                if (isEndPoint)
                {
                    if (curveIndex + 1 < path.Curves.Length)
                        _otherCurveToUpdate = curveIndex + 1;
                    else if (curveIndex == path.Curves.Length - 1)
                    {
                        if (path.IsLooping)
                            _otherCurveToUpdate = 0;
                        else if (path.IsAlignedToNextTrack())
                        {
                            _otherCurveToUpdate = 0;
                            _otherTrackToUpdate = path.NextTrack.GetComponent<BezierPath>();
                        }
                    }
                }
            }
        }

        void ConstraintTangent(Vector3 controlPointPos, ref BezierPoint thisTangent, ref BezierPoint otherTangent, Vector3 oldTangent)
        {
            var oldTangentLength = (oldTangent - controlPointPos).magnitude;
            var newTangentDir = thisTangent.Position - controlPointPos;

            if (_lockTangentLengths)
                thisTangent.Position = controlPointPos + newTangentDir.normalized * oldTangentLength;

            var toPoint = controlPointPos - thisTangent.Position;

            if (_lockTangents)
                otherTangent.Position = controlPointPos + toPoint;
        }

        void OnEnable()
        {
            Undo.undoRedoPerformed += OnUndoRedoCallback;
        }

        void OnDisable()
        {
            Undo.undoRedoPerformed -= OnUndoRedoCallback;
        }

        void OnUndoRedoCallback()
        {
            var path = target as BezierPath;

            if (_selectedCurveIndex >= 0)
                path.UpdateMesh(_selectedCurveIndex);

            if (_otherCurveToUpdate >= 0)
                _otherTrackToUpdate.UpdateMesh(_otherCurveToUpdate);
        }
    }
}

