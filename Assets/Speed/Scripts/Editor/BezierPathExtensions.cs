﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public static class BezierPathExtensions
    {
        public static Vector3 ToLocalPos(this BezierPath path, Vector3 point, BezierPath other)
        {
            var absolute = other.transform.TransformPoint(point);
            return path.transform.InverseTransformPoint(absolute);
        }
    }
}

