﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour
{
    public float LoopDuration = 1.0f;
    public float SizeMin = 1.0f;
    public float SizeMax = 1.0f;
    public float Duration = 1.5f;

    [HideInInspector]
    public float ScaleCurve = 1.0f;

    float _targetScale = 1.0f;

	void Start () 
    {
        _targetScale = Random.Range(SizeMin, SizeMax);
        Destroy(gameObject, Duration);
	}
	
	void Update () 
    {
        float r = Mathf.Sin((Time.time / LoopDuration) * (2 * Mathf.PI)) * 0.5f + 0.25f;
        float g = Mathf.Sin((Time.time / LoopDuration + 0.33333333f) * 2 * Mathf.PI) * 0.5f + 0.25f;
        float b = Mathf.Sin((Time.time / LoopDuration + 0.66666667f) * 2 * Mathf.PI) * 0.5f + 0.25f;
        var mr = GetComponent<MeshRenderer>();
        mr.sharedMaterial.SetVector("_ChannelFactor", new Vector4(r, g, b, 0.0f));
        transform.localScale = Vector3.one * _targetScale * ScaleCurve;
	}   
}
