﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace frantic
{
    public class Mine : NetworkBehaviour
    {
        public float BurstFactor = 20.0f;
        public float SpeedLostFactor = 0.7f;
        public float PardonDuration = 2.0f;

        [SyncVar]
        public Quaternion _netRotation;

        [SyncVar]
        public int _netPardonPlayerWithID;

        float _pardonTimer;

        public override void OnStartServer()
        {
            //Console.Print(string.Format("OnStartServer, Mine {0} pardoning player {1}", netId.Value, _netPardonPlayerWithID));
            _pardonTimer = PardonDuration;
        }

        public override void OnStartClient()
        {
            //Console.Print(string.Format("OnStartClient, Mine {0} pardoning player {1}", netId.Value, _netPardonPlayerWithID));
            transform.rotation = _netRotation;
            _pardonTimer = PardonDuration;
        }

        void Update()
        {
            if (_pardonTimer >= 0.0f)
            {
                _pardonTimer -= Time.deltaTime;
                if (_pardonTimer < 0.0f)
                {
                    //Console.Print(string.Format("Mine {0} no longer pardoning player {1}", netId.Value, _netPardonPlayerWithID));
                    _netPardonPlayerWithID = -1;
                }
            }
        }
    }
}

