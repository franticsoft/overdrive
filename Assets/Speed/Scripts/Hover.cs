﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class Hover : MonoBehaviour
    {
        public float VerticalAmplitude = 0.004f;
        public float HorizontalAmplitude = 0.001f;
        public float VecticalFrequency = 4.0f;
        public float HorizontalFrequency = 6.0f;
        public float OscillateFrequency = 6.0f;
        public float OscillateDuration = 0.6f;
        public float OscillateAmplitudeMax = 1.0f;
        public float OscillateAmplitudeMin = 0.2f;

        float _angleVert = 0.0f;
        float _angleHoriz = 0.0f;
        Vector3 _initialPosition;
        float _oscillateAmplitude;
        float _oscillateTimer = -1.0f;
        PhysicBody _physicBody;
        Player _player;
        float _verticalSpeed;
        FranticNetworkManager _manager;

        public enum State
        {
            Stopped,
            TransitionToMove,
            Moving,
            Oscillating,
            MiniFly
        }

        State _state = State.Stopped;

        void Start()
        {
            _initialPosition = transform.localPosition;
            _physicBody = GetComponent<PhysicBody>();
            _manager = FranticNetworkManager.singleton as FranticNetworkManager;
            _player = transform.parent.parent.GetComponent<Player>();
        }

        float _transitionToMoveTimer;
        Vector3 _transitionToMoveStart;
        void Update()
        {
            var levelMgr = _manager != null ? _manager.LevelManager : null;
            if (levelMgr != null && levelMgr.IsPaused)
                return;

            var dt = levelMgr != null ? levelMgr.AverageDT : Time.smoothDeltaTime;
            switch (_state)
            {
                case State.Stopped:
                    {
                        if (MathUtils.IsZero(_physicBody.Speed))
                        {
                            // idle anim
                            var deltaZ = VerticalAmplitude * Mathf.Cos(_angleVert);
                            _angleVert += (Mathf.PI * 2.0f * (Time.smoothDeltaTime / VecticalFrequency));

                            var deltaX = HorizontalAmplitude * Mathf.Sin(_angleHoriz);
                            _angleHoriz += (Mathf.PI * 2.0f * (Time.smoothDeltaTime / HorizontalFrequency));

                            transform.Translate(transform.up * deltaZ);
                            transform.Translate(transform.right * deltaX);
                        }
                        else
                        {
                            SwitchState(State.TransitionToMove);
                        }
                    }
                    break;

                case State.TransitionToMove:
                    {
                        if(_transitionToMoveTimer < .3f)
                        {
                            transform.localPosition = Vector3.Lerp(_transitionToMoveStart, _initialPosition, _transitionToMoveTimer/.3f);
                            _transitionToMoveTimer += dt;
                        }
                        else
                        {
                            SwitchState(State.Moving);
                        }                        
                    }
                    break;

                case State.Oscillating:
                    {
                        if (_oscillateTimer >= 0.0f)
                        {
                            var t = OscillateDuration - _oscillateTimer;
                            var amplitude = Mathf.Lerp(_oscillateAmplitude, 0.0f, t / OscillateDuration);
                            var angle = t * 360.0f * OscillateFrequency;
                            var y = _initialPosition.y + amplitude * Mathf.Sin(-angle * Mathf.Deg2Rad);
                            transform.localPosition = Vector3.up * y;
                            _oscillateTimer -= dt;
                        }
                        else
                        {
                            SwitchState(State.Moving);
                        }
                    }
                    break;

                case State.MiniFly:
                    {
                        var height = transform.localPosition.y;
                        height += _verticalSpeed * dt;
                        _verticalSpeed += (-_player.Gravity);
                        transform.localPosition = Vector3.up * height;

                        if (_verticalSpeed < 0.0f)
                        {
                            if (transform.localPosition.y < _initialPosition.y)
                            {
                                transform.localPosition = _initialPosition;
                                Oscillate(1.0f);
                            }
                        }
                    }
                    break;

                case State.Moving:
                    if (MathUtils.IsZero(_physicBody.Speed))
                        SwitchState(State.Stopped);
                    break;
            }

            _player.UpdateShooting();
        }

        public void Oscillate(float oscillateFactor)
        {
            if (oscillateFactor < MathUtils.Epsilon)
                return;

            _oscillateAmplitude = Mathf.Lerp(OscillateAmplitudeMin, OscillateAmplitudeMax, oscillateFactor);
            _oscillateTimer = OscillateDuration;
            _state = State.Oscillating;
        }

        public void Fly(float speed)
        {
            _verticalSpeed = speed;
            _state = State.MiniFly;
            _player.ResetGroundState();
        }

        public State GetState()
        {
            return _state;
        }

        void SwitchState(State state)
        {
            switch (state)
            {
                case State.TransitionToMove:
                    _transitionToMoveStart = transform.localPosition;
                    _transitionToMoveTimer = 0.0f;
                    break;

                case State.Moving:
                    transform.localPosition = _initialPosition;
                    break;
            }

            _state = state;
        }
    }
}
