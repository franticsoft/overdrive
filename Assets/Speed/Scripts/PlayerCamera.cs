﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace frantic
{
    public class PlayerCamera : MonoBehaviour
    {
        //public float FollowDist = 1.0f;
        //public float MaxFollowSpeed = 400.0f;
        public float FollowFactorPosition = 100.0f;
        public float FollowFactorLookAt = 50.0f;
        //public float LookAtFactor = 1.0f;
        public float RaceAnimDuration = 3.0f;
        public Camera SceneCamera;
        public Camera EffectsCamera;
        public Camera LookingUpCamera;

        public bool RaceFinished { get { return _raceFinished; } }

        Transform _followTarget;
        Transform _lookAtTarget;
        Vector3 _toCamera;
        //Vector3 _lookAtOffset;
        FranticNetworkManager _manager;
        Player _player;
        GameSettings _gameSettings;
        Transform _skyDome;
        //Camera _customFXCamera;
        //GameObject _customFXCameraGO;

        void Start()
        {
            _manager = FranticNetworkManager.singleton as FranticNetworkManager;
            _manager.PlayerCamera = this;

            _gameSettings = _manager.GetComponent<GameSettings>();
            var player = transform.parent.GetComponent<PlayerSync>();
            _lookAtTarget = player.transform.Find("Player/BodyHolder/Body/LookAtTarget");
            _player = player.Player;
            _followTarget = _lookAtTarget;
            var skyDome = GameObject.FindObjectOfType<TOD_Sky>();
            if (skyDome)
                _skyDome = skyDome.transform;

            if (_followTarget == null)
                return;

            _toCamera = transform.position - _player.transform.position; // _followTarget.position;
            //_lookAtOffset = _followTarget.position;

            //_customFXCameraGO = GameObject.Find("CustomFXCamera");
            //_customFXCamera = _customFXCameraGO.GetComponent<Camera>();
            //UpdateFXCamera();
        }

        Vector3 SmoothApproach(Vector3 pastPosition, Vector3 pastTargetPosition, Vector3 targetPosition, float speed)
        {
            float t = Time.smoothDeltaTime * speed;
            Vector3 v = (targetPosition - pastTargetPosition) / t;
            Vector3 f = pastPosition - pastTargetPosition + v;
            return targetPosition - v + f * Mathf.Exp(-t);
        }

        Vector3 pastFollowerPosition, pastTargetPosition;

        Vector3 _toLookAtTarget = Vector3.forward;
        Vector3 _pastToLookAtFollower = Vector3.forward, _pastToLookAtTarget = Vector3.forward;

        float _localPos = 0.0f;
        float _trailingAngle = 0.0f;
        float _anticipationAngle;
        void Update()
        {
            if (_followTarget == null || _lookAtTarget == null || _manager.LevelManager.IsPaused)
                return;

            //var toCamera = _followTarget.forward * _toCamera.z + _followTarget.up * _toCamera.y;
            var toCamera = _player.TrackForward * _toCamera.z + _player.TrackUp * _toCamera.y;
            var trackPos = (_player.TrackPos); // + _player.TrackForward * _lookAtOffset.z + _player.TrackUp * _lookAtOffset.y);            
            var targetPos = trackPos + toCamera; //_followTarget.position + toCamera;
            targetPos += _player.TrackUp * _player.BodyElevation;

            //var localPos = FollowDist * Mathf.Clamp01(_player.Speed / MaxFollowSpeed);
            //_localPos = Mathf.Lerp(_localPos, localPos, Time.smoothDeltaTime * FollowFactor);

            //transform.position = targetPos - _followTarget.forward * _localPos; // Vector3.SmoothDamp(transform.position, targetPos, ref velocity, FollowFactor);
            //transform.position = Vector3.Lerp(transform.position, targetPos, 1.0f - Mathf.Clamp01(Mathf.Min(_player.Speed, MaxFollowSpeed/FollowFactor) / MaxFollowSpeed));            

            transform.position = SmoothApproach(pastFollowerPosition, pastTargetPosition, targetPos, FollowFactorPosition);
            pastFollowerPosition = transform.position;
            pastTargetPosition = targetPos;

            var factor = _player.TurnAccel / _player.TurnAccelMax;
            ////_anticipationAngle = Mathf.Lerp(_anticipationAngle, _gameSettings.CameraAnticipationAngle * factor, _manager.LevelManager.AverageDT / _gameSettings.CameraAnticipationDuration);
            ////var turnAnticipationRot = Quaternion.AngleAxis(_anticipationAngle, _lookAtTarget.up);

            var lookAtUpFactor = !MathUtils.IsZero(factor) ? 1.0f : 0.0f;
            var lookAtOffset = (_lookAtTarget.up * lookAtUpFactor * _gameSettings.CameraAnticipationUpOffset);
            // var toLookAtTarget = _player.TrackForward; //((_lookAtTarget.position + lookAtOffset) - transform.position).normalized;
            var toLookAtTarget = ((_lookAtTarget.position + lookAtOffset) - transform.position).normalized;
            //var targetRot = Quaternion.LookRotation(toLookAtTarget, _player.TrackUp); // Vector3.Lerp(_lookAtTarget.up, Vector3.up, .22f).normalized);

            //var toLookAtTarget = (_lookAtTarget.position - transform.position).normalized;            
            //var targetAngle = Mathf.Atan2(toLookAtTarget.x, toLookAtTarget.z);
            //var trailingAngle = FollowAngle * Mathf.Deg2Rad * Mathf.Clamp01(Mathf.Abs(_player.TurnAccel) / _player.TurnAccelMax) * Mathf.Sign(_player.TurnAccel);
            //_trailingAngle = Mathf.Lerp(_trailingAngle, trailingAngle, Time.smoothDeltaTime * LookAtFactor);
            //var trailingDirection = -Mathf.Sign(_player.TurnAccel);
            //Debug.Log("_trailingAngle: " + _trailingAngle * Mathf.Rad2Deg);
            //var lookAt = Quaternion.AngleAxis(trailingDirection * _trailingAngle, _followTarget.up) * toLookAtTarget;
            //var targetRot = Quaternion.LookRotation(lookAt.normalized, _followTarget.up);
            //transform.rotation = targetRot;
            _toLookAtTarget = SmoothApproach(_pastToLookAtFollower, _pastToLookAtTarget, toLookAtTarget, FollowFactorLookAt);
            _pastToLookAtFollower = _toLookAtTarget;
            _pastToLookAtTarget = toLookAtTarget;

            transform.rotation = Quaternion.LookRotation(_toLookAtTarget, _player.TrackUp);
            //transform.rotation = Quaternion.Slerp(transform.rotation, targetRot /** turnAnticipationRot*/, Time.smoothDeltaTime * LookAtFactor);

            if (_skyDome)
                _skyDome.position = transform.position;

            if (_raceFinishedTimer > 0.0f)
            {
                _raceFinishedTimer -= _manager.LevelManager.AverageDT;
                if (_raceFinishedTimer <= 0.0f)
                    _raceFinished = true;
            }

            //UpdateFXCamera();
        }

        public void UpdateCamera(float fov, bool useLookingUpCamera)
        {
            SceneCamera.fieldOfView = fov;
            EffectsCamera.fieldOfView = fov;

            var targetPosition = Vector3.zero;
            var targetRotation = Quaternion.identity;
            if (useLookingUpCamera)
            {
                targetPosition = LookingUpCamera.transform.localPosition;
                targetRotation = LookingUpCamera.transform.localRotation;
            }

            if (SceneCamera.transform.localPosition != targetPosition)
                SceneCamera.transform.localPosition = Vector3.Lerp(SceneCamera.transform.localPosition, targetPosition, _manager.LevelManager.AverageDT / _gameSettings.LookingUpCameraTransitionDuration);

            if (SceneCamera.transform.localRotation != targetRotation)
                SceneCamera.transform.localRotation = Quaternion.Lerp(SceneCamera.transform.localRotation, targetRotation, _manager.LevelManager.AverageDT / _gameSettings.LookingUpCameraTransitionDuration);
        }

        public void OnRespawn()
        {
            var toCamera = _followTarget.forward * _toCamera.z + _followTarget.up * _toCamera.y;
            var targetPos = _followTarget.position + toCamera;
            transform.position = targetPos;

            var toLookAtTarget = (_lookAtTarget.position - transform.position).normalized;
            var targetRot = Quaternion.LookRotation(toLookAtTarget, _lookAtTarget.up);
            transform.rotation = targetRot;
        }

        bool _raceFinished;
        float _raceFinishedTimer = -1.0f;
        public void OnRaceFinished()
        {
            _raceFinishedTimer = RaceAnimDuration;
        }

        void OnPreCull()
        {
            OnPreRender();
        }

        void OnPreRender()
        {
            if (_manager != null && _manager.IsSplitScreenGame)
            {
                // Make sure only the light of the current player is active during rendering
                var myPlayer = transform.parent.gameObject.GetComponent<PlayerSync>();
                var players = GameObject.FindObjectsOfType<PlayerSync>();
                foreach (var player in players)
                {
                    player.Player.TopLight.enabled = ReferenceEquals(player, myPlayer);
                }
            }
        }
    }
}
