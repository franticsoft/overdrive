﻿using UnityEngine;
using System.Collections;

public class Prefabs : MonoBehaviour
{
    public GameObject MuzzleLight;
    public GameObject MuzzleFlash;
    public GameObject Sparks;
    public GameObject Explosion;
    public GameObject WallExplosion;
    public GameObject ElectricBeam;
    public GameObject Shield;
    public GameObject Missile;
    public GameObject Bullet;
    public GameObject Mine;
    public GameObject BasicAI;
    public GameObject[] PowerUps;
    public GameObject InGameUI;
    public GameObject PlayerIndicator;
    public GameObject OtherPlayersIndicator;
    public GameObject CollisionSparks;
    public GameObject PropulsionLight;
}

