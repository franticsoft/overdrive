﻿using UnityEngine;

namespace frantic
{
    public class ShipStats : MonoBehaviour
    {
        public int Speed = 5;
        public int Acceleration = 5;
        public int Steering = 5;
        public int Combat = 5;
    }
}

