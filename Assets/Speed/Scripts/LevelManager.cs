﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace frantic
{
    public class LevelManager : NetworkBehaviour
    {
        public GameObject StartingTrack;        
        public GameObject OfflineCamera;
        public GameObject PlayerCamera;
        public Prefabs Prefabs;
        public GameObject InGameCanvas;
        public GameObject PauseCanvas;
        public GameObject SpawningPoints;
        public GameObject ExploderPrefab;
        public float DeltaTimeSamples = 10;
        public Color InventoryExhaustedColor = Color.gray;
        public float TrackRadius = 30.0f;
        public float TackDistFromWall = 3.0f;

        public struct RaceInfo
        {
            public MonoBehaviour Actor;
            public float TraveledDistance;
            public bool IsPlayer;
        }

        public List<RaceInfo> Players { get { return _playersWinnerToLoser; } }
        public bool IsPaused { get { return _paused; } }
        public bool CountDownInProgress { get; set; }
        public IntRange LaneRange { get; set; }

        public float AverageDT { get { return Time.smoothDeltaTime; } }

        LinkedList<float> _deltaTimes = new LinkedList<float>();
        float _powerUpSpawnTimer;
        List<RaceInfo> _playersWinnerToLoser = new List<RaceInfo>();
        GameSettings _gameSettings;
        FranticNetworkManager _manager;
        NavigationBar _tutorialNavBar;
        GameObject _exploder;

        public override void OnStartServer()
        {
            Console.Print(string.Format("LevelManager.OnStartServer({0})", netId.Value));
            _manager = NetworkManager.singleton as FranticNetworkManager;
            _manager.LevelManager = this;
        }

        public override void OnStartClient()
        {
            Console.Print(string.Format("LevelManager.OnStartClient({0})", netId.Value));
            _manager = NetworkManager.singleton as FranticNetworkManager;
            _manager.LevelManager = this;
        }

        void Start()
        {
            Console.Print(string.Format("LevelManager.Start"));
            _gameSettings = _manager.GetComponent<GameSettings>();
            Console.Print(string.Format("LevelManager.Start() - netId: {0}", netId.Value));
            var inGameCanvas = GameObject.Instantiate(InGameCanvas);
            var canvasScaler = inGameCanvas.GetComponent<CanvasScaler>();
            canvasScaler.referenceResolution = _manager.IsSplitScreenGame ? new Vector2(1360.0f, 768.0f) : new Vector2(1024.0f, 768.0f);            
            OfflineCamera.SetActive(_manager.IsSplitScreenGame);

            // count down
            var screen = GameObject.Find("InGameScreen");
            //var countDown = Instantiate(_gameSettings.CountDownPanel);
            //countDown.transform.SetParent(screen.transform, false);
            //CountDownInProgress = true;

            // tutorial
            if (!_manager.IsSplitScreenGame)
            {
                _tutorialNavBar = Instantiate(_gameSettings.NavigationBar).GetComponent<NavigationBar>();
                _tutorialNavBar.transform.SetParent(screen.transform, false);
                _tutorialNavBar.SetConfiguration(NavigationBar.ConfigType.Tutorial);
            }

            _powerUpSpawnTimer = _gameSettings.PowerUpSpawnFrequency;

            // initialize track indices
            int trackIndex = 0;
            var trackGO = StartingTrack;
            while (trackGO != null && !ReferenceEquals(trackGO, StartingTrack))
            {
                var track = trackGO.GetComponent<BezierPath>();
                track.TrackIndex = trackIndex;
                ++trackIndex;
                trackGO = track.NextTrack;
            }

            _exploder = GameObject.Instantiate(ExploderPrefab);
        }

        bool _paused;
        GameObject _pauseGO;
        void Update()
        {
            // sort players
            var players = GameObject.FindObjectsOfType<PlayerSync>();
            var ais = GameObject.FindObjectsOfType<BasicAI>();
            _playersWinnerToLoser.Clear();
            foreach(var player in players)
            {
                RaceInfo info;
                info.Actor = player.Player;
                info.TraveledDistance = player._netTraveledDistance;
                info.IsPlayer = true;
                _playersWinnerToLoser.Add(info);
            }
            foreach (var ai in ais)
            {
                RaceInfo info;
                info.Actor = ai;
                info.TraveledDistance = ai._netTraveledDistance;
                info.IsPlayer = false;
                _playersWinnerToLoser.Add(info);
            }
            _playersWinnerToLoser = _playersWinnerToLoser.OrderByDescending(p => p.TraveledDistance).ToList();

            // update pause
            if (!_paused)
            {
                if (Input.GetButtonUp("Pause"))
                {
                    if (_manager.IsLocalGame)
                        Time.timeScale = 0.0f;

                    if (_pauseGO == null)
                        _pauseGO = CreatePausePanel();
                    _pauseGO.SetActive(true);
                    _paused = true;
                }
            }
            else
            {
                if (Input.GetButtonUp("Pause"))
                {
                    _pauseGO.SetActive(false);
                    Time.timeScale = 1.0f;
                    _paused = false;
                }
                else if (Utils.IsInputCancel())
                {
                    Time.timeScale = 1.0f;
                    if (_manager.IsLocalGame)
                        _manager.StopHost();
                    else
                        _manager.ExitOnlineGame();
                }
            }

            if (isServer)
            {
                // if all players finished, quit race
                var numPlayersThatFinished = players.Where(p => p._netRaceFinished).Count();
                if(numPlayersThatFinished == players.Length)
                {
                    _manager.StopHost();
                    return;
                }

                // power ups
                _powerUpSpawnTimer -= Time.deltaTime;
                if(_powerUpSpawnTimer < 0.0f)
                {
                    var index = Random.Range(0, Prefabs.PowerUps.Length);
                    var winner = _playersWinnerToLoser.FirstOrDefault(p => p.Actor is Player).Actor as Player;
                    var segmentIndex = winner.SegmentIndex + 2;
                    if (segmentIndex < winner.CurrentTrack.transform.childCount)
                    {
                        var segment = winner.CurrentTrack.transform.GetChild(segmentIndex).GetComponent<TrackSegment>();
                        if(segment.PowerUp == null)
                        {
                            var pos = winner.CurrentTrack.GetPoint(segmentIndex, 0.0f);
                            var powerUpGO = GameObject.Instantiate(Prefabs.PowerUps[index], pos, Quaternion.identity) as GameObject;
                            var tangent = winner.CurrentTrack.GetTangent(segmentIndex, 0.0f).normalized;
                            var up = winner.CurrentTrack.GetUp(segmentIndex, 0.0f).normalized;
                            up = Quaternion.AngleAxis(winner.CurrentTrack.GetBanking(segmentIndex, 0.0f), tangent) * up;                            
                            var powerUp = powerUpGO.GetComponent<PowerUp>();
                            powerUp.RotationAxis = up;
                            powerUp.TrackSegment = segment;
                            segment.PowerUp = powerUp;
                            NetworkServer.Spawn(powerUpGO);
                            _powerUpSpawnTimer = _gameSettings.PowerUpSpawnFrequency;
                        }
                    }
                }

                // AI
                foreach (var ai in ais)
                {
                    ai.ServerUpdate();

                    ai.UpdateClient();
                }
            }

            // bullets - TODO: optimize
            //var toKill = new List<Bullet>();
            //foreach (var player in players)
            //{
            //    foreach (var ai in ais)
            //    {
            //        var bullets = player.Player.Bullets;
            //        foreach (var bullet in bullets.Where(b => b.Key.enabled))
            //        {
            //            if (bullet.Key.CollidesWith(ai.transform.position, 10.0f))
            //            {
            //                bullet.Key.enabled = false;
            //                toKill.Add(bullet.Key);

            //                if (isServer)
            //                {
            //                    ai.Health = ai.Health - _gameSettings.BulletDamage;
            //                    if (ai.Health < 0)
            //                    {
            //                        // explode
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            //foreach (var bullet in toKill)
            //    bullet.Kill();
        }

        GameObject CreatePausePanel()
        {
            var pauseGO = Instantiate(PauseCanvas);
            var navBar = Instantiate(_gameSettings.NavigationBar).GetComponent<NavigationBar>();
            navBar.transform.SetParent(pauseGO.transform, false);
            navBar.SetConfiguration(NavigationBar.ConfigType.Pause);
            return pauseGO;
        }

        public GameObject GetMissileTarget(Player myPlayer, BasicAI myAI, Transform me)
        {
            var smallestAngleToTarget = float.MaxValue;
            GameObject mostInFrontTarget = null;

            IEnumerable<BasicAI> ais;
            if (myAI)
                ais = GameObject.FindObjectsOfType<BasicAI>().Where(p => p.netId.Value != myAI.netId.Value);
            else
                ais = GameObject.FindObjectsOfType<BasicAI>();

            foreach (var ai in ais)
            {
                var toTarget = (ai.transform.position - me.position).normalized;
                var dir = me.forward;
                var dot = Vector3.Dot(toTarget, dir);
                if(dot > 0.0f)
                {
                    var angle = Mathf.Acos(dot);
                    if (angle < smallestAngleToTarget)
                    {
                        smallestAngleToTarget = angle;
                        mostInFrontTarget = ai.gameObject;
                    }
                }                
            }

            IEnumerable<PlayerSync> players;
            if (myPlayer != null)
            {
                var myPlayerSync = myPlayer.transform.parent.GetComponent<PlayerSync>();
                players = GameObject.FindObjectsOfType<PlayerSync>().Where(p => p.netId.Value != myPlayerSync.netId.Value);
            }
            else
            {
                players = GameObject.FindObjectsOfType<PlayerSync>();
            }

            foreach (var playerSync in players)
            {
                var player = playerSync.Player;
                var toTarget = (player.transform.position - me.position).normalized;
                var dir = me.forward;
                var dot = Vector3.Dot(toTarget, dir);
                if (dot > 0.0f)
                {
                    var angle = Mathf.Acos(dot);
                    if (angle < smallestAngleToTarget)
                    {
                        smallestAngleToTarget = angle;
                        mostInFrontTarget = player.gameObject;
                    }
                }                
            }

            return mostInFrontTarget;
        }

        public void ClearTutorial()
        {
            if(_tutorialNavBar != null)
            {
                Destroy(_tutorialNavBar.gameObject);
                _tutorialNavBar = null;
            }
        }

        public void OnMissileLaunched(Missile missile)
        {
            var localPlayers = GameObject.FindObjectsOfType<PlayerSync>().Where(p => p.isLocalPlayer);
            foreach (var player in localPlayers)
                player.AddMinimapTarget(missile.GetComponent<MinimapTarget>());
        }

        public void OnMissileDestroyed(Missile missile)
        {
            var localPlayers = GameObject.FindObjectsOfType<PlayerSync>().Where(p => p.isLocalPlayer);
            foreach (var player in localPlayers)
                player.RemoveMinimapTarget(missile.GetComponent<MinimapTarget>());
        }
    }
}
