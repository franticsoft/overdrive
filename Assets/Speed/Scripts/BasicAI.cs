﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace frantic
{
    public class BasicAI : NetworkBehaviour
    {
        // AI properties
        public float CurvePosX
        {
            get { return _curvePos.x; }
            set { _curvePos.x = value; }
        }

        [SyncVar]
        public float _netTraveledDistance;

        [SyncVar]
        public float _netLocalTraveledDistance;

        //public float Health { get { return _health; } set { _health = value; } }
        //public GameObject HealthBar;

        public float Accel = 0.05f;
        //public float MaxSpeed = 1.0f;
        public float SpeedChangeDuration = 2.0f;
        public FloatRange MaxSpeedTweakRange = new FloatRange(150.0f, 400.0f);
        //public frantic.Range LayMineFrequency = new frantic.FloatRange(3.0f, 6.0f);
        public float DistanceToPlayer = 3000.0f;
        public FloatRange ShootFrequency = new FloatRange(5.0f, 10.0f);

        int _curveIndex = 0;
        Vector2 _curvePos = Vector2.zero;
        float _forwardDist = 0.0f;
        float _maxSpeed;

        BezierPath _track;
        TrackSegment _currentSegment;
        Vector3 _initialPos;
        PhysicBody _physicBody;
        //SphereCollider _collider;
        //float _originalRadius = 0.0f;
        float _targetXPos;
        //float _mineTimer;
        float _shootTimer;
        GameSettings _gameSettings;
        GameObject _body;
        FranticNetworkManager _manager;

        //float _health = 100.0f;
        //HealthBar _healthBar;

        [SyncVar]
        Vector3 _netPos;

        [SyncVar]
        Quaternion _netRotation;

        [SyncVar]
        public float _netHealth;

        float _syncTimer = 0.0f;
        const float _syncFrequency = 0.1f;
        const float _lerpDuration = 0.1f;

        public override void OnStartServer()
        {
            //Console.Print(string.Format("BasicAI.OnStartServer({0})", netId.Value));
            _netTraveledDistance = transform.position.z;
            _netLocalTraveledDistance = transform.position.z;
        }

        public override void OnStartClient()
        {
            //_healthBar = HealthBar.GetComponent<HealthBar>();
        }

        void Start()
        {
            _body = transform.GetChild(0).gameObject;
            _physicBody = _body.GetComponent<PhysicBody>();
            //_collider = body.GetComponent<SphereCollider>();
            //_originalRadius = _collider.radius;
            _manager = FranticNetworkManager.singleton as FranticNetworkManager;
            _track = _manager.LevelManager.StartingTrack.GetComponent<BezierPath>();
            _currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();
            _initialPos = transform.position;

            // initialize position
            _curvePos.x = transform.position.x + _manager.LevelManager.TrackRadius;
            _targetXPos = _curvePos.x;
            _forwardDist = transform.position.z;

            //_mineTimer = UnityEngine.Random.Range(LayMineFrequency.Min, LayMineFrequency.Max);
            _shootTimer = UnityEngine.Random.Range(ShootFrequency.Min, ShootFrequency.Max);
            _gameSettings = FranticNetworkManager.singleton.GetComponent<GameSettings>();

            // randomize accel and speed
            var _percentAccel = Accel * 0.1f;            
            Accel += Random.Range(-_percentAccel, _percentAccel);
            //var _percentSpeed = MaxSpeed * 0.1f;
            //MaxSpeed += Random.Range(-_percentSpeed, _percentSpeed);
            var percentSpeedMin = MaxSpeedTweakRange.Min * _gameSettings.AIPercentVariation;
            MaxSpeedTweakRange.Min += Random.Range(-percentSpeedMin, percentSpeedMin);
            var percentSpeedMax = MaxSpeedTweakRange.Max * _gameSettings.AIPercentVariation;
            MaxSpeedTweakRange.Max += Random.Range(-percentSpeedMax, percentSpeedMax);
            _maxSpeed = (MaxSpeedTweakRange.Max + MaxSpeedTweakRange.Min) / 2.0f;
        }

        int _shootingCannon = -1;
        void Update()
        {
            if (_manager.LevelManager.IsPaused || _manager.LevelManager.CountDownInProgress)
                return;

            // update health bar
            //if(_healthBar)
            //    _healthBar.SetProgress(_health / 100.0f);

            if (!isServer)
            {
                var lerpFactor = Time.deltaTime / _lerpDuration;
                transform.position = Vector3.Lerp(transform.position, _netPos, lerpFactor);
                transform.rotation = Quaternion.Lerp(transform.rotation, _netRotation, lerpFactor);
                //_health = _netHealth;
                return;
            }
        }

        public void ServerUpdate()
        {
            //steering
            //if (_physicBody.CollisionResponseTimer < 0.0f
            //&& _health > 0.0f)
            {
                // adjust max speed depending on position
                var players = _manager.LevelManager.Players;
                var firstPlayerTraveledDistance = -1.0f;
                for (int i = 0; i < players.Count; ++i)
                {
                    if (players[i].IsPlayer)
                    {
                        var dist = players[i].TraveledDistance;
                        if (firstPlayerTraveledDistance < 0.0f || dist > firstPlayerTraveledDistance)
                            firstPlayerTraveledDistance = dist;
                    }
                }
                              
                // Tweak speed depending on speed from player  
                var distToPlayer = firstPlayerTraveledDistance - _netTraveledDistance;
                distToPlayer = Mathf.Clamp(distToPlayer, -DistanceToPlayer, DistanceToPlayer);
                var normalizedDist = distToPlayer / DistanceToPlayer; // convert to [-1, 1]
                var speedFactor = (normalizedDist + 1.0f) * 0.5f; // convert to [0, 1]
                var maxSpeed = Mathf.Lerp(MaxSpeedTweakRange.Min, MaxSpeedTweakRange.Max, speedFactor);
                _maxSpeed = Mathf.Lerp(_maxSpeed, maxSpeed, Time.deltaTime / SpeedChangeDuration);

                _physicBody.Speed += Accel * _manager.LevelManager.AverageDT;
                _physicBody.Speed = Mathf.Clamp(_physicBody.Speed, 0.0f, _maxSpeed);
            }

            // determine local basis
            var tangent = _track.GetTangent(_curveIndex, _curvePos.y).normalized;
            var up = _track.Curves[_curveIndex].GetUp(_curvePos.y);
            var right = Vector3.Cross(up, tangent);
            var banking = Quaternion.AngleAxis(_track.GetBanking(_curveIndex, _curvePos.y), tangent);
            right = banking * right;
            up = banking * up;

            // collision response
            //if (_physicBody.CollisionResponseTimer < 0.0f)
            _physicBody.Direction = tangent;
            //else
            //{
            //    var colFactor = 1.0f - (_physicBody.CollisionResponseTimer / _gameSettings.CollisionResponseDuration);
            //    _physicBody.Direction = Vector3.Lerp(_physicBody.PostCollisionDir, tangent, colFactor);
            //    _curvePos.x = Mathf.Lerp(_curvePos.x, _targetXPos, colFactor);
            //    _physicBody.CollisionResponseTimer -= Time.deltaTime;
            //}

            // forward motion
            var fwdFactor = Vector3.Dot(tangent, _physicBody.Direction);
            var deltaDist = _physicBody.Speed * fwdFactor * _manager.LevelManager.AverageDT;
            _forwardDist += deltaDist;
            _netTraveledDistance += deltaDist;
            _netLocalTraveledDistance += deltaDist;

            var curveLength = _track.GetLength(_curveIndex);
            if (_forwardDist > curveLength)
            {
                if (_curveIndex == (_track.Curves.Length - 1) && _track.NextTrack != null)
                {
                    var thisTrack = _track;
                    _track = _track.NextTrack.GetComponent<BezierPath>();
                    _curveIndex = 0;
                    if (!thisTrack.IsAlignedToNextTrack())
                    {
                        // TODO: proper jump
                        _forwardDist = 0.0f;
                    }
                    else
                    {
                        _forwardDist = _forwardDist - curveLength;
                    }
                }
                else
                {
                    _curveIndex = (_curveIndex + 1) % _track.Curves.Length;
                    _forwardDist = _forwardDist - curveLength;
                }

                if (_curveIndex == 0 && ReferenceEquals(_track.gameObject, _manager.LevelManager.StartingTrack))
                    _netLocalTraveledDistance = 0.0f;

                _currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();
                //var oldRadius = _curveRadius;
                //_curveRadius = _track.transform.localScale.x * _currentSegment.Radius;
                //// adjust x position depending on radius change
                //var deltaRadius = _curveRadius - oldRadius;
                //_curvePos.x += deltaRadius;
            }

            var curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
            var lateral0 = curvePoint - right * _manager.LevelManager.TrackRadius;

            // lateral motion
            //var lateralFactor = Vector3.Dot(right, _physicBody.Direction);
            //var deltaX = _physicBody.Speed * lateralFactor * _manager.LevelManager.AverageDT;
            //var collisionDistToBorder = 0.3f;
            //var maxX = (_manager.LevelManager.TrackRadius * 2.0f) - collisionDistToBorder;
            //var towardsLeftWall = deltaX < 0.0f && _curvePos.x + deltaX < collisionDistToBorder;
            //var towardsRightWall = deltaX > 0.0f && _curvePos.x + deltaX > maxX;
            //if (!(towardsLeftWall || towardsRightWall))
            //    _curvePos.x += deltaX;
            //else
            //    _curvePos.x = towardsLeftWall ? collisionDistToBorder : maxX;

            // update transform
            _curvePos.y = _track.GetFactor(_curveIndex, _forwardDist);
            curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
            lateral0 = curvePoint - right * _manager.LevelManager.TrackRadius;
            //var oldPos = transform.position;
            transform.position = lateral0 + right * _curvePos.x;
            transform.rotation = Quaternion.LookRotation(tangent, up);

            // make collider bigger than delta pos to avoid tunnelling
            //var deltaPos = transform.position - oldPos;
            //_collider.radius = Mathf.Max(deltaPos.magnitude, _originalRadius);

            // Handle mines
            //_mineTimer -= Time.deltaTime;
            //if (_mineTimer < 0.0f)
            //{
            //    var mineGO = Instantiate(_levelMgr.Prefabs.Mine, transform.position, transform.rotation) as GameObject;
            //    var mine = mineGO.GetComponent<Mine>();
            //    mine._netRotation = transform.rotation;
            //    _mineTimer = Random.Range(LayMineFrequency.Min, LayMineFrequency.Max);
            //    NetworkServer.Spawn(mineGO);
            //}

            // shooting
            //_shootTimer -= Time.deltaTime;
            //if (_shootTimer < 0.0f)
            //{
            //    var missileGO = Instantiate(_manager.LevelManager.Prefabs.Missile) as GameObject;
            //    var missile = missileGO.GetComponent<Missile>();
            //    if (_shootingCannon < 0)
            //        _shootingCannon = Random.Range(0, 2);
            //    else
            //        _shootingCannon = 1 - _shootingCannon;
            //    missile.Init(null, this, _shootingCannon, _physicBody.Speed);
            //    missile._netRotation = missile.transform.rotation; // FIX UNITY BUG - ONLY POSITION IS PROPAGATED ON SPAWN!!
            //    _shootTimer = UnityEngine.Random.Range(ShootFrequency.Min, ShootFrequency.Max);
            //    NetworkServer.Spawn(missileGO);
            //}
        }

        public void UpdateClient()
        {
            if (_syncTimer > _syncFrequency)
            {
                _netPos = transform.position;
                _netRotation = transform.rotation;
                //_netHealth = _health;
                _syncTimer = 0.0f;
            }
            _syncTimer += Time.deltaTime;
        }

        public void OnCollision(Vector3 postCollisionDir, float postCollisionSpeed)
        {
            var oldSpeed = _physicBody.Speed;
            _physicBody.PostCollisionDir = postCollisionDir;
            _physicBody.Speed = postCollisionSpeed;
            _physicBody.CollisionResponseTimer = _gameSettings.CollisionResponseDuration;
            if (oldSpeed > 0.0f)
                _physicBody.GetComponent<Hover>().Oscillate(1.0f - (_physicBody.Speed / oldSpeed));
        }

        public int GetCurrentCurve()
        {
            return _curveIndex;
        }

        public Vector2 GetCurvePos()
        {
            return _curvePos;
        }

        public void Respawn()
        {
            transform.position = _initialPos;
            _curveIndex = 0;
            _track = _manager.LevelManager.StartingTrack.GetComponent<BezierPath>();
            _currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();

            _forwardDist = 0.0f;
            _curvePos.x = _targetXPos;
            _physicBody.CollisionResponseTimer = -1.0f;
            _physicBody.Speed = 0.0f;
            //_mineTimer = Random.Range(LayMineFrequency.Min, LayMineFrequency.Max);
        }

        public void TakeDamage(float damage)
        {
            if (!isServer)
                return;

            //_health -= damage;
            //if (_health < 0.0f)
            {
                // Explode
                var explosion = Instantiate(_manager.LevelManager.Prefabs.Explosion, transform.position, transform.rotation) as GameObject;
                NetworkServer.Spawn(explosion);
                if (_manager.IsSplitScreenGame)
                    explosion.transform.FindChild("ExplosionTrails").gameObject.SetActive(false);

                // shred into pieces
                _body.tag = "Exploder";
                Exploder.Utils.ExploderSingleton.ExploderInstance.transform.position = _body.transform.position;
                Exploder.Utils.ExploderSingleton.ExploderInstance.ExplodeObject(_body);

                // TODO: destroy AI and respawn it at explosion location
            }
        }

        public void OnMissileHit()
        {
            _physicBody.Speed *= _gameSettings.MissileHitFactor;
            TakeDamage(_gameSettings.MissileDamage);
        }

        public void GetCurveCoords(out Vector2 pos, out float dist, out BezierPath path, out int index)
        {
            pos = _curvePos;
            dist = _forwardDist;
            path = _track;
            index = _curveIndex;
        }
    }
}
