﻿using UnityEngine;
using System;

namespace frantic
{
    public class BezierPathData : MonoBehaviour
    {        
        [HideInInspector]
        [SerializeField]
        BezierCurve[] _curves;

        public BezierCurve this[int index]
        {
            get { return _curves[index]; }
            set { _curves[index] = value; }
        }

        public int Length
        {
            get { return _curves.Length; }
        }

        public void SetCurve(BezierCurve curve, int index)
        {
            if (_curves == null)
                _curves = new BezierCurve[1];

            if (index < _curves.Length)
                _curves[index] = curve;
            else
            {
                Debug.Assert(index == _curves.Length);
                Array.Resize(ref _curves, _curves.Length + 1);
                _curves[_curves.Length - 1] = curve;
            }
        }

        public void RemoveLastCurve()
        {
            Array.Resize(ref _curves, _curves.Length - 1);
        }
    }
}

