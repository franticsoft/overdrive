﻿using UnityEngine;
using System.Collections;

namespace frantic
{
    public class Electricity : MonoBehaviour
    {
        public Transform Target;
        public Material Coil;
        public Material Beam;
        public int CoilSegments = 10;
        public int BeamSegments = 10;
        public int Coils = 10;
        public FloatRange CoilWidth = new FloatRange(0.2f, 0.2f);
        public FloatRange BeamWidth = new FloatRange(0.2f, 0.2f);
        public FloatRange Damping = new FloatRange(0.0f, 1.0f);        
        public FloatRange Amplitude = new FloatRange(0.3f, 0.3f);
        public FloatRange ChargeDuration = new FloatRange(0.2f, 0.2f);

        BasicSpline _spline;
        LineRenderer _beam;

        struct CoilInfo
        {
            public Vector3[] laterals;
            public Vector3[] verticals;
            public float timer;
        }

        CoilInfo[] _coils;

        void Start()
        {
            _spline = gameObject.AddComponent<BasicSpline>();
            _beam = gameObject.AddComponent<LineRenderer>();
            _beam.sharedMaterial = Beam;
            _beam.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            _beam.receiveShadows = false;
            _beam.SetWidth(BeamWidth.Min, BeamWidth.Max);
            _beam.SetVertexCount(BeamSegments + 1);

            _coils = new CoilInfo[Coils];

            UpdateBeam();
            UpdateCoils();
        }

        void Update()
        {
            UpdateBeam();
            UpdateCoils();
        }

        void UpdateCoils()
        {
            for(int i=0; i<_coils.Length; ++i)
            {
                if (_coils[i].laterals == null)
                {
                    _coils[i].laterals = new Vector3[CoilSegments];
                    _coils[i].verticals = new Vector3[CoilSegments];
                    _coils[i].timer = Random.Range(ChargeDuration.Min, ChargeDuration.Max);
                    GenerateCoil(i, true);
                }

                if (_coils[i].timer < 0.0f)
                {
                    GenerateCoil(i, false);
                    _coils[i].timer = Random.Range(ChargeDuration.Min, ChargeDuration.Max);
                }
                else
                {
                    // update
                    var child = transform.GetChild(i);
                    var lineRenderer = child.GetComponent<LineRenderer>();
                    var cur = transform.TransformPoint(_spline.GetPoint(0.0f));
                    lineRenderer.SetPosition(0, cur);
                    for (int j = 1; j < CoilSegments + 1; ++j)
                    {
                        var f = (float)j / (CoilSegments - 1);
                        var next = transform.TransformPoint(_spline.GetPoint(f));
                        next = next + _coils[i].laterals[j - 1] + _coils[i].verticals[j - 1];
                        lineRenderer.SetPosition(j, next);
                    }

                    _coils[i].timer -= Time.deltaTime;
                }
            }
        }

        void UpdateBeam()
        {
            // update spline
            var target = (Target != null) ? Target.position : (transform.position + Vector3.forward * 100.0f);
            var points = _spline.Points.Length;
            var fDampingFactor = Damping.Max;
            var fDampingStep = (Damping.Max - Damping.Min) / (points - 1);
            var toTarget = (target - transform.position);
            var fDistToTarget = toTarget.magnitude;
            var fDistStep = fDistToTarget / (points - 1);
            toTarget /= fDistToTarget;
            var vElemPos = transform.position; 
            for(int i=0; i<points; ++i)
            {
                var absPos = transform.TransformPoint(_spline.Points[i]);
                var vPosition = MathUtils.Damp(absPos, vElemPos, Time.deltaTime, fDampingFactor);
                _spline.Points[i] = transform.InverseTransformPoint(vPosition);
				fDampingFactor -= fDampingStep;
                vElemPos += toTarget * fDistStep;
            }

            // update beam
            var cur = transform.TransformPoint(_spline.GetPoint(0.0f));
            _beam.SetPosition(0, cur);
            for (int i = 1; i < BeamSegments + 1; ++i)
            {
                var f = (float)i / (BeamSegments - 1);
                var next = transform.TransformPoint(_spline.GetPoint(f));
                _beam.SetPosition(i, next);
            }
        }

        void GenerateCoil(int index, bool createNew)
	    {
            LineRenderer lineRenderer = null;
            if(createNew)
            {
                var child = new GameObject("Coil");
                lineRenderer = child.AddComponent<LineRenderer>();
                lineRenderer.sharedMaterial = Coil;
                lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                lineRenderer.receiveShadows = false;
                lineRenderer.SetWidth(CoilWidth.Min, CoilWidth.Max);
                lineRenderer.SetVertexCount(CoilSegments + 1);
                child.transform.parent = transform;
            }
            else
            {
                var child = transform.GetChild(index);
                lineRenderer = child.GetComponent<LineRenderer>();
            }                

            var cur = transform.TransformPoint(_spline.GetPoint(0.0f));
            lineRenderer.SetPosition(0, cur);            
            for (int i = 1; i < CoilSegments + 1; ++i)
		    {
                var f = (float)i / (CoilSegments - 1);
                var next = transform.TransformPoint(_spline.GetPoint(f));
                var vDirection = (next - cur).normalized;
                var amplitude = Random.Range(Amplitude.Min, Amplitude.Max);
			    var fLateralFactor = Random.Range(-amplitude, amplitude);
			    var fVerticalFactor = Random.Range(-amplitude, amplitude);
                Vector3 vertical, lateral;
                MathUtils.GetBasis(vDirection, out vertical, out lateral);
                lateral *= fLateralFactor;
                vertical *= fVerticalFactor;

                _coils[index].laterals[i - 1] = lateral;
                _coils[index].verticals[i - 1] = vertical;

                next = next + lateral + vertical;
                lineRenderer.SetPosition(i, next);
                cur = next;
		    }
	    }
    }
}
