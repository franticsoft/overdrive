﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;

namespace frantic
{
    public class Player : MonoBehaviour
    {
        public float BoostDuration = 1.2f;
        public float SpeedChangeDuration = 0.5f;
        public float BreakAccel = 0.2f;
        public float TurnAccelMin = 60.0f;
        public float TurnAccelMax = 100.0f;
        public float TurnAccelCollisionFactor = 2.0f;
        public float TurnAccelNonSteerFactor = 2.0f;
        public float TurnAccelDampDuration = 1.0f;
        public float MaxTurnDuration = 1.0f;
        public float Decel = 0.1f;
        public float MaxBanking = 30.0f;
        public float BankingFactor = 1.0f;
        public float FOVChangeDuration = 0.3f;
        public float MinFOV = 60.0f;
        public float MaxFOV = 100.0f;
        public float Gravity = 2.5f;
        public float MaxAirTime = 2.0f;
        public float GroundEscapeSpeed = 50.0f;
        public float MaxMiniLandSpeed = 50.0f;
        public float GroundFlyGravity = 10.0f;
        public float AccelDecrFactor = 0.8f;
        public float MaxSpeedIncr = 52.0f;
        public float BaseAccel = 1.0f;
        public float BaseMaxSpeed = 150.0f;
        public float AbsoluteMaxSpeed = 350.0f;
        public float BoostAccelFactor = 2.0f;
        public float BoostSpeedFactor = 1.3f;
        public float BoostFOVFactor = 1.3f;
        public float BoostSpeedReductionFactor = .6f;
        public float AirSteerAccel = 1.0f;
        public GameObject PropulsionBoost;
        public FloatRange DriftDurationRange = new FloatRange(0.2f, 1.0f);
        public float MaxAccelDurationForDrift = 2.0f;
        public Vector3 CollisionSparkOffset = new Vector3(1.8f, 0.1f, 4.0f);
        public float TurnExaggerationFactor = 1.5f;
        public float TurnExaggerationRestoreDuration = .3f;
        public FloatRange LateralReductionFactor = new FloatRange(0.2f, 1.0f);        

        public float TraveledDistance { get { return _traveledDistance; } }
        public float LocalTraveledDistance { get { return _localTraveledDistance; } }
        public int CurrentLap { get { return _currentLap; } }
        public bool RaceFinished { get { return _currentLap == _gameSettings.MaxLaps; } }
        public bool NotReadyToRace { get { return RaceFinished || _manager.LevelManager.CountDownInProgress; } }
        public BezierPath CurrentTrack { get { return _track; } }
        public int SegmentIndex { get { return _curveIndex; } }
        public GameObject InGameUI { get; set; }
        public Transform Body { get { return _body; } }
        public float BodyElevation { get { return _bodyHolder.transform.localPosition.y - _groundLandHeight; } }        
        public float TurnAccel { get { return _turnAccel; } }        
        public float Health { get { return _health; } set { _health = value; } }
        public Light TopLight { get { return _topLight; } }
        public float Speed { get { return _physicBody.Speed; } }
        public float CurvePosX { get { return _curvePos.x; } }
        public Vector3 TrackUp { get { return _up; } }
        public Vector3 TrackForward { get { return _tangent; } }
        public Vector3 TrackPos { get { return _lateral0 + _right * _curvePos.x; } }
        public float MaxSpeed
        {
            get
            {
                var maxSpeed = AbsoluteMaxSpeed;
                var boostMode = (_boostTimer > 0.0f);
                if (boostMode)
                    maxSpeed *= BoostSpeedFactor;
                return maxSpeed;
            }
        }

        private bool IsRespondingToCollision { get { return _physicBody.CollisionResponseTimer >= 0.0f; } }

        PlayMakerFSM _gameFSM;
        PlayerSync _playerSync;
        PowerUp _powerUp;
        float _traveledDistance;
        float _localTraveledDistance;
        int _currentLap;
        int _positionInRace;
        float _health = 100.0f;

        enum State
        {
            Ground,
            Air,
            Falling
        }

        int _curveIndex = 0;
        TrackSegment _currentSegment;
        Vector2 _curvePos; // x is lateral, y is forward
        float _forwardDist;
        bool _onTunnel;
        float _tunnelAngle;
        float _banking = 0.0f;
        float _turnAngle = 0.0f;
        float _maxSpeed = 0.0f;
        float _accel = 0.0f;
        float _boostTimer = -1.0f;
        float _boostLength;
        bool _collision = false;
        bool _previousCollision = false;
        float _targetForwardMotion = 0.0f;
        float _targetBanking = 0.0f;
        float _fwdFactor = 0.0f;
        float _lateralFactor = 0.0f;
        Vector3 _tangent;
        Vector3 _forward;
        Vector3 _rotationForward;
        Vector3 _up;
        Vector3 _right;
        Vector3 _lateral0;
        Vector3 _previousPos;
        State _state = State.Ground;
        Vector3 _airSpeed;
        float _airTime;
        bool _forceFall;
        Vector3 _driftDirection;
        float _driftTimer = -1.0f;
        float _driftDuration;
        //ParticleSystem _leftPropulsion;
        //ParticleSystem _rightPropulsion;
        ParticleSystem _leftPropulsionBoost;
        ParticleSystem _rightPropulsionBoost;
        //GameObject _leftPropulsionBoost;
        //GameObject _rightPropulsionBoost;
        //JetFX _leftJetFX;
        //JetFX _rightJetFX;
        GameObject _shield;
        PhysicBody _physicBody;
        Text _uiLapStatus;
        Text _uiPositionStatus;
        Light _topLight;
        float _normalTopLightIntensity;

        bool _shootingBullets;
        float _bulletsTimer = -1.0f;

        PlayerCamera _playerCamera;
        FranticNetworkManager _manager;
        Transform _body;
        Transform _bodyHolder;
        BezierPath _track;
        Hover _hover;
        Prefabs _prefabs;
        GameSettings _gameSettings;

        // Inventory
        int _boostCount;
        int _attackCount;
        GameObject _boostExhausted;
        GameObject _attackExhausted;
        RawImage _boostIcon;
        RawImage _attackIcon;
        Text _boostText;
        Text _attackText;
        Color _boostColor;
        Color _attackColor;

        enum GroundState
        {
            Normal,
            MiniFly
        }
        GroundState _groundState = GroundState.Normal;
        Vector3 _groundTakeOffPos;
        Vector3 _groundTakeOffUp;
        float _groundLandHeight;
        float _groundMiniFallSpeed;
        float _groundFallAmount;

        GameObject _debugText;

        delegate void updateLocalRotation(Vector3 up);
        delegate void updateLateralMotion(Vector3 up, Vector3 right);
        delegate void updateTransform(Vector3 up, Vector3 right);

        updateLocalRotation _updateLocalRotation;
        updateLateralMotion _updateLateralMotion;
        updateTransform _updateTransform;
        ParticleSystem _envParticleEmitter;
        Transform _envParticlesContainer;
        Transform _envParticlesRotator;
        GameObject _propulsionLight;

        public void CommonInit()
        {
            _manager = NetworkManager.singleton as FranticNetworkManager;
            _gameSettings = _manager.GetComponent<GameSettings>();
            _bodyHolder = transform.FindChild("BodyHolder");
            _body = _bodyHolder.FindChild("Body");            
            _physicBody = _body.GetComponent<PhysicBody>();

            if(transform.parent != null)
                _playerSync = transform.parent.GetComponent<PlayerSync>();

            if(_manager.LevelManager != null)
                _prefabs = _manager.LevelManager.Prefabs;

            _topLight = transform.FindChild("TopLight").GetComponent<Light>();
            _normalTopLightIntensity = _topLight.intensity;
        }

        public void Start()
        {
            CommonInit();
            
            _track = _manager.LevelManager.StartingTrack.GetComponent<BezierPath>();
            
            var cameraGO = transform.parent.FindChildWithTag("MainCamera").gameObject;
            _playerCamera = cameraGO.GetComponent<PlayerCamera>();
            _hover = _body.GetComponent<Hover>();

            var envParticles = Instantiate(_prefabs.CollisionSparks);
            _envParticlesContainer = envParticles.transform;
            _envParticlesRotator = _envParticlesContainer.GetChild(0);
            _envParticleEmitter = _envParticlesRotator.GetChild(0).GetComponent<ParticleSystem>();
            _envParticleEmitter.StopParticles();
            _envParticleEmitter.transform.GetChild(0).gameObject.SetActive(false);

            _propulsionLight = Instantiate(_prefabs.PropulsionLight);
            _propulsionLight.transform.SetParent(_body, false);
            _propulsionLight.SetActive(false);

            var leftProp = _body.FindChild("PropulsionLeft");
            var rightProp = _body.FindChild("PropulsionRight");

            if (PropulsionBoost)
            {
                //_leftPropulsionBoost = leftProp.AddChild(PropulsionBoost);
                _leftPropulsionBoost = leftProp.AddChild(PropulsionBoost).GetComponent<ParticleSystem>();
                _leftPropulsionBoost.StopParticles();
                //_leftPropulsion = leftProp.AddChild(Propulsion).GetComponent<ParticleSystem>();

                if (rightProp)
                {
                    //_rightPropulsionBoost = rightProp.AddChild(PropulsionBoost);
                    _rightPropulsionBoost = rightProp.AddChild(PropulsionBoost).GetComponent<ParticleSystem>();
                    _rightPropulsionBoost.StopParticles();
                    //_rightPropulsion = rightProp.AddChild(Propulsion).GetComponent<ParticleSystem>();
                }                
            }
            //else
            //{
            //    _leftJetFX = leftProp.AddChild(Propulsion).GetComponent<JetFX>();
            //    _rightJetFX = rightProp.AddChild(Propulsion).GetComponent<JetFX>();
            //}

            var shieldAnchor = _body.FindChild("ShieldAnchor");
            _shield = shieldAnchor.AddChild(_prefabs.Shield);
            _shield.SetActive(false);

#if UNITY_EDITOR
            _curveIndex = _track.StartingCurveIndex;
#endif

            _currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();
            _curvePos = Vector2.zero;
            _curvePos.x = transform.position.x + _manager.LevelManager.TrackRadius; // initialize position

            SetUpdateMethods();

            _previousPos = transform.position;
            _forward = _rotationForward = _track.GetTangent(_curveIndex, 0.0f).normalized;
            _maxSpeed = BaseMaxSpeed;
            _accel = BaseAccel;
            _groundLandHeight = _bodyHolder.localPosition.y;

            _gameFSM = _manager.LevelManager.GetComponent<PlayMakerFSM>();            

            // Inventory
            //_uiPowerUp = InGameUI.transform.FindChild("UIPowerUp").GetComponent<RawImage>();
            //_uiRaceStatus = InGameUI.transform.FindChild("UIPosition").GetComponent<Text>();
            //_uiPowerUp.gameObject.SetActive(false);

            _boostCount = _gameSettings.InitialInventoryItemCount;
            _attackCount = _gameSettings.InitialInventoryItemCount;
            var inventory = InGameUI.transform.FindChild("Inventory");
            var boost = inventory.FindChild("Boost");
            var attack = inventory.FindChild("Attack");
            _boostIcon = boost.FindChild("Icon").GetComponent<RawImage>();
            _attackIcon = attack.FindChild("Icon").GetComponent<RawImage>();
            _boostText = boost.FindChild("Text").GetComponent<Text>();
            _attackText = attack.FindChild("Text").GetComponent<Text>();
            _boostExhausted = boost.FindChild("Exhausted").gameObject;
            _attackExhausted = attack.FindChild("Exhausted").gameObject;
            _boostColor = _boostIcon.color;
            _attackColor = _attackIcon.color;
            UpdateInventory();

            _uiLapStatus = InGameUI.transform.FindChild("Lap").FindChild("Text").GetComponent<Text>();
            _uiPositionStatus = InGameUI.transform.FindChild("Position").FindChild("Text").GetComponent<Text>();
            UpdateUI();

            _debugText = GameObject.Find("DebugText");
        }

        void OnDrawGizmos()
        {
            if (_track == null)
                return;

            Gizmos.color = Color.red;
            Gizmos.DrawSphere(transform.position + _up * 5.0f, .2f);
            Gizmos.DrawLine(transform.position, transform.position + _up * 5.0f);
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(transform.position + _tangent * 5.0f, .2f);
            Gizmos.DrawLine(transform.position, transform.position + _tangent * 5.0f);
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position + _forward * 5.0f, .2f);
            Gizmos.DrawLine(transform.position, transform.position + _forward * 5.0f);
        }

        void Update()
        {
            if (_manager.LevelManager.IsPaused)
                return;

            // update position
            _positionInRace = 1;
            foreach (var player in _manager.LevelManager.Players)
            {
                if (ReferenceEquals(player.Actor, this))
                    break;
                _positionInRace++;
            }

            if (_state == State.Ground)
                UpdateGround();

            if (_state == State.Air)
                UpdateAir();

            if (_state == State.Falling)
            {
                _bodyHolder.transform.position += _airSpeed * _manager.LevelManager.AverageDT;
                _airSpeed += -Vector3.up * Gravity;
            }

            //var useShield = Input.GetButton("P" + (_playerSync.playerControllerId + 1) + "_Jump");
            //_shield.SetActive(useShield);

            // Debug
            if(_debugText)
            {
                var text = _debugText.GetComponent<Text>();
                text.text = "TurnAccel: " + TurnAccel + " speed: " + _physicBody.Speed + ", maxSpeed: " + _maxSpeed + ", _turnDuration: " + _turnDuration + ", _accelDuration: " + _accelDuration;
            }

            // update server
            if (_playerSync.isLocalPlayer)
                _playerSync.ClientSendStateToServer();

            UpdateUI();
        }

        bool hadPowerUp;
        void UpdateUI()
        {
            _uiPositionStatus.text = string.Format("POS {0}/{1}", _positionInRace, _manager.LevelManager.Players.Count);

            // Lap
            if (_currentLap + 1 >= _gameSettings.MaxLaps)
                _uiLapStatus.text = "FINAL LAP";
            else
                _uiLapStatus.text = string.Format("LAP {0}/{1}", _currentLap + 1, _gameSettings.MaxLaps);
        }

        public void UpdateBullets()
        {
            if (!_shootingBullets)
                return;

            if (_bulletsTimer < 0.0f)
            {
                if (_shootingCannon < 0)
                    _shootingCannon = Random.Range(0, 2);
                else
                    _shootingCannon = 1 - _shootingCannon;

                // spawn bullet
                var weapon = _body.GetComponent<Weapon>();
                var cannon = _shootingCannon == 0 ? weapon.LeftCannon : weapon.RightCannon;
                var bulletGO = GameObject.Instantiate(_prefabs.Bullet, cannon.position, cannon.rotation) as GameObject;
                var bullet = bulletGO.GetComponent<Bullet>();
                bullet.Init(this, null, _shootingCannon, _physicBody.Speed);
                _bulletsTimer = _gameSettings.BulletsFrequency;
            }
            else
                _bulletsTimer -= Time.deltaTime;
        }

        bool _wasColliding;
        void UpdateGround()
        {
            UpdateSteering();

            Vector3 up, right;
            DetermineLocalBasis(out up, out right);

            _updateLocalRotation(up);

            var isColliding = !(_physicBody.CollisionResponseTimer < 0.0f);
            if(isColliding && !_wasColliding)
            {
                Console.Print(string.Format("Player - Dir: {0} - Speed: {1}", _physicBody.Direction, _physicBody.Speed));
            }

            // collision response & drift
            var isDrifting = _driftTimer >= 0.0f;
            if (IsRespondingToCollision)
            {
                var factor = 1.0f - (_physicBody.CollisionResponseTimer / _gameSettings.CollisionResponseDuration);
                _physicBody.Direction = Vector3.Lerp(_physicBody.PostCollisionDir, _forward, factor);
                
            }
            else if(isDrifting)
            {
                var factor = 1.0f - (_driftTimer / _driftDuration);
                _physicBody.Direction = Vector3.Lerp(_driftDirection, _forward, factor);
            }
            else
            {
                _physicBody.Direction = _forward;
            }

            if (IsRespondingToCollision)
                _physicBody.CollisionResponseTimer -= _manager.LevelManager.AverageDT;

            if (isDrifting)
                _driftTimer -= _manager.LevelManager.AverageDT;

            UpdateForwardMotion();
            if (_state != State.Ground)
            {
                ResetGroundState();
                return;
            }

            //_right = right;
            DetermineLocalBasis(out up, out right);
            //var interpolatedRight = Vector3.Slerp(_right, right, 0.5f).normalized;

            _updateLateralMotion(up, right);
            _previousPos = transform.position;
                        
            _updateTransform(up, right);

            UpdateBoost();

            // collision sparks
            var offset = CollisionSparkOffset;
            offset.x *= _collisionTowardsRight ? 1.0f : -1.0f;
            offset.x *= _fwdFactor;
            _envParticlesContainer.transform.position = _bodyHolder.transform.position + _bodyHolder.transform.TransformVector(offset); // equivalent of (_body.transform.right* offset.x + _body.transform.up*offset.y + _body.transform.forward*offset.z);
            _envParticlesRotator.transform.localRotation = Quaternion.LookRotation(_tangent, _up);
            var sparkHeightThreshold = 2.0f;
            var canSpark = (_bodyHolder.localPosition.y - _groundLandHeight) <= sparkHeightThreshold;
            if (_collision != _previousCollision)
            {
                if (_collision && canSpark)
                {
                    if (!_envParticleEmitter.isPlaying)
                        _envParticleEmitter.PlayParticles();

                    _envParticleEmitter.transform.GetChild(0).gameObject.SetActive(true);
                }
                else
                {
                    _envParticleEmitter.StopParticles();
                    _envParticleEmitter.transform.GetChild(0).gameObject.SetActive(false);
                }
            }
            else
            {
                if(_collision)
                {
                    if(!canSpark)
                    {
                        if (_envParticleEmitter.isPlaying)
                            _envParticleEmitter.StopParticles();

                        _envParticleEmitter.transform.GetChild(0).gameObject.SetActive(false);
                    }
                    else
                    {
                        if (!_envParticleEmitter.isPlaying)
                            _envParticleEmitter.PlayParticles();

                        _envParticleEmitter.transform.GetChild(0).gameObject.SetActive(true);
                    }
                }
            }
            _previousCollision = _collision;

            var maxSpeed = MaxSpeed;
            if (_physicBody.Speed > maxSpeed)
                _physicBody.Speed = maxSpeed;

            // ground fly
            if (_hover.GetState() == Hover.State.Moving)
            {
                switch (_groundState)
                {
                    case GroundState.MiniFly:
                        {
                            var heightFromTakeOffPoint = Mathf.Abs(Vector3.Dot(transform.position - _groundTakeOffPos, _groundTakeOffUp));
                            heightFromTakeOffPoint -= _groundFallAmount;
                            //var localHeight = heightFromTakeOffPoint / Vector3.Dot(_groundTakeOffUp, _up);
                            var targetPos = Vector3.up * (heightFromTakeOffPoint + _groundLandHeight);
                            _bodyHolder.transform.localPosition = Vector3.Lerp(_bodyHolder.transform.localPosition, targetPos, _manager.LevelManager.AverageDT / _gameSettings.MiniFlyDampDuration);
                            if (_bodyHolder.transform.localPosition.y > _groundLandHeight)
                            {
                                _groundMiniFallSpeed += GroundFlyGravity;
                                _groundFallAmount += _groundMiniFallSpeed * _manager.LevelManager.AverageDT;
                            }
                            else
                            {
                                _bodyHolder.transform.localPosition = Vector3.up * _groundLandHeight;
                                _groundState = GroundState.Normal;
                                _hover.Oscillate(Mathf.Clamp01(Mathf.Abs(_groundMiniFallSpeed) / MaxMiniLandSpeed));
                            }
                        }
                        break;
                }
            }

            UpdateBanking();
            UpdateCamera();
            UpdateIdle();
        }

        void UpdateAir()
        {
            UpdateCamera();

            _airTime += _manager.LevelManager.AverageDT;

            var deltaDist = _airSpeed * _manager.LevelManager.AverageDT;
            _traveledDistance += deltaDist.magnitude;
            _localTraveledDistance += deltaDist.magnitude;

            transform.position += deltaDist;
            _airSpeed += -Vector3.up * Gravity;

            if (!_forceFall)
            {
                // air steering
                var lateral = Input.GetAxis("P" + (_playerSync.playerControllerId + 1) + "_Horizontal");
                if (!MathUtils.IsZero(lateral))
                {
                    var turnAngle = TurnAccelMin * lateral * _gameSettings.AirRotationAccelFactor * _manager.LevelManager.AverageDT;
                    _forward = (Quaternion.AngleAxis(turnAngle, _up) * _forward).normalized;
                    _rotationForward = (Quaternion.AngleAxis(turnAngle * TurnExaggerationFactor, _up) * _rotationForward).normalized;
                    transform.rotation = Quaternion.LookRotation(_rotationForward, _up);
                }

                var point0 = _track.GetPoint(_curveIndex, 0.0f);
                var up0 = _track.GetUp(_curveIndex, 0.0f).normalized;
                var plane = new Plane(up0, point0 + up0 * _groundLandHeight);
                if (!plane.GetSide(transform.position))
                {
                    var tangent0 = _track.GetTangent(_curveIndex, 0.0f).normalized;
                    Vector3 up, right;                    
                    up = _track.Curves[_curveIndex].GetUp(0.0f);
                    right = Vector3.Cross(up, tangent0);
                    var banking = Quaternion.AngleAxis(_track.GetBanking(_curveIndex, 0.0f), tangent0);
                    right = banking * right;
                    up = banking * up;

                    var toAbs = transform.position;
                    var fromAbs = point0;
                    var toDest = toAbs - fromAbs;
                    var projOnTangent = Vector3.Project(toDest, tangent0);
                    var projOnRight = Vector3.Project(toDest, right);
                    var fromDist = 0.0f;
                    var forwardDist = fromDist;
                    var fromPos = new Vector2(_manager.LevelManager.TrackRadius, 0.0f);
                    var curvePos = fromPos;
                    forwardDist += projOnTangent.magnitude * Mathf.Sign(Vector3.Dot(projOnTangent.normalized, tangent0));
                    curvePos.x += projOnRight.magnitude * Mathf.Sign(Vector3.Dot(projOnRight.normalized, right));

                    //var toBorder = 0.0f; // CollisionDistToBorder;
                    //if (curvePos.x > toBorder && curvePos.x < (_curveRadius * 2.0f) - toBorder)
                    {
                        _forwardDist = forwardDist;
                        _curvePos = curvePos;
                        _state = State.Ground;
                        _hover.Oscillate(1.0f);
                    }
                }
            }

            if (_state == State.Air && _airTime > MaxAirTime)
            {
                _gameFSM.SendEvent("FallOfDeath");
                _state = State.Falling;
                _forceFall = false;
            }
        }

        void StartBoost()
        {
            if (_boostTimer < 0.0f)
            {
                _preBoostAccel = _accel;
                _accel *= BoostAccelFactor;
            }
            else
            {
                _accel = _preBoostAccel * BoostAccelFactor;
            }
            _boostTimer = BoostDuration;

            //if (_leftPropulsion && _leftPropulsion.isPlaying)
            //    _leftPropulsion.StopParticles();

            //if (_rightPropulsion && _rightPropulsion.isPlaying)
            //    _rightPropulsion.StopParticles();

            if (_leftPropulsionBoost)
                _leftPropulsionBoost.PlayParticles();

            if (_rightPropulsionBoost)
                _rightPropulsionBoost.PlayParticles();

            _propulsionLight.SetActive(true);
            _shield.SetActive(true);
            //_leftPropulsionBoost.SetActive(true);
            //if(_rightPropulsionBoost)
            //    _rightPropulsionBoost.SetActive(true);
        }

        void ApplyBreaks()
        {
            if (_physicBody.Speed > 0.0f)
                _physicBody.Speed -= BreakAccel * _manager.LevelManager.AverageDT;
            else
                _physicBody.Speed -= BaseAccel * _manager.LevelManager.AverageDT;
        }

        float _preBoostAccel;
        bool _triggeredBoostInSegment;
        float _accelDuration = -1.0f;
        float _turnAccel;
        void UpdateSteering()
        {
            float rawAxis = Input.GetAxis("P" + (_playerSync.playerControllerId+1) + "_Accelerate");
            float rawForward = 0.0f;
            if (!MathUtils.IsZero(rawAxis))
                rawForward = -Mathf.Sign(rawAxis);
            var canAccelerate = (_hover.GetState() != Hover.State.MiniFly) 
                             && (_groundState == GroundState.Normal) 
                             && !NotReadyToRace
                             && !IsRespondingToCollision;
            if (!canAccelerate)
                rawForward = 0.0f;

            var fwd = rawForward;
            var oldAccelDuration = _accelDuration;
            bool wasAccelerating = _accelDuration > 0.0f;
            if (!MathUtils.IsZero(fwd))
            {
                if(fwd > 0.0f)
                {
                    if (_accelDuration < 0.0f)
                        _accelDuration = 0.0f;
                    _accelDuration += _manager.LevelManager.AverageDT;
                }
                else
                    _accelDuration = -1.0f;
            }
            else
                _accelDuration = -1.0f;

            // update drift
            var isAccelerating = _accelDuration > 0.0f;
            if(!isAccelerating && wasAccelerating && _driftTimer < 0.0f)
            {
                _driftDirection = _forward;
                var driftFactor = oldAccelDuration / MaxAccelDurationForDrift;
                _driftDuration = Mathf.Lerp(DriftDurationRange.Min, DriftDurationRange.Max, Mathf.Clamp01(driftFactor));
                _driftTimer = _driftDuration;
            }

            // handle boost
            if (_currentSegment.Boost != TrackSegment.DecoratorType.None)
            {
                var canBoost = false;
                var curveLength = _track.GetLength(_curveIndex);
                var closeToBoost = (_forwardDist < curveLength / 4.0f);
                if (_currentSegment.Boost == TrackSegment.DecoratorType.Left)
                    canBoost = (_curvePos.x < _manager.LevelManager.TrackRadius) && closeToBoost;
                else if (_currentSegment.Boost == TrackSegment.DecoratorType.Right)
                    canBoost = (_curvePos.x > _manager.LevelManager.TrackRadius) && closeToBoost;
                else
                    canBoost = closeToBoost;

                if (canBoost 
                && !_triggeredBoostInSegment
                && !NotReadyToRace)
                {
                    StartBoost();
                    _triggeredBoostInSegment = true;
                }
            }

            // force steer when in boost
            var boostMode = (_boostTimer > 0.0f);
            if (boostMode)
            {
                _boostTimer -= _manager.LevelManager.AverageDT;
                fwd = 1.0f;
                if (_boostTimer <= 0.0f)
                {
                    _accel = _preBoostAccel;
                    _boostTimer = -1.0f;
                    boostMode = false;

                    //_leftPropulsionBoost.SetActive(false);
                    //if (_rightPropulsionBoost)
                    //    _rightPropulsionBoost.SetActive(false);

                    if (_leftPropulsionBoost)
                        _leftPropulsionBoost.StopParticles();

                    if (_rightPropulsionBoost)
                        _rightPropulsionBoost.StopParticles();

                    _propulsionLight.SetActive(false);
                    _shield.SetActive(false);

                    //if (_rightPropulsion)                                            
                    //    _rightPropulsion.PlayParticles();

                    //if (_leftPropulsion)
                    //    _leftPropulsion.PlayParticles();
                }
            }

            // Stop acceleration when moving towards the wall!
            if (_collision)
                fwd = 0.0f;

            var lateral = Input.GetAxis("P" + (_playerSync.playerControllerId + 1) + "_Horizontal");
            var previousSpeed = _physicBody.Speed;
            if (!MathUtils.IsZero(fwd))
            {
                if (fwd > 0.0f)
                {
                    // Unity particle bug
                    //if (_boostTimer < 0.0f)
                    //{
                    //    if(_previousPropulsionTime >= 0.0f && MathUtils.Equals(_leftPropulsion.time, _previousPropulsionTime))
                    //    {
                    //        Debug.Log("Correcting Propulsion Bug");
                    //        if(_leftPropulsion)
                    //            _leftPropulsion.PlayParticles();

                    //        if(_rightPropulsion)
                    //            _rightPropulsion.PlayParticles();
                    //    }
                    //    _previousPropulsionTime = _leftPropulsion.time;
                    //}

                    if (_physicBody.Speed >= 0.0f)
                        _physicBody.Speed += _accel * _manager.LevelManager.AverageDT;
                    else
                        _physicBody.Speed += BreakAccel * _manager.LevelManager.AverageDT;

                    // make sure breaks are applied even in boost mode
                    if (rawForward < 0.0f)
                        ApplyBreaks();
                }
                else
                {
                    ApplyBreaks();                    
                }
            }
            else
            {
                if (!MathUtils.IsZero(_physicBody.Speed))
                {
                    if (_physicBody.Speed > 0.0f)
                    {
                        _physicBody.Speed -= Decel * _manager.LevelManager.AverageDT;
                        if (_physicBody.Speed < 0.0f)
                            _physicBody.Speed = 0.0f;
                    }
                    else
                    {
                        _physicBody.Speed += Decel * _manager.LevelManager.AverageDT;
                        if (_physicBody.Speed > 0.0f)
                            _physicBody.Speed = 0.0f;
                    }
                }
            }

            // collision decel
            if(_collision)
            {
                _physicBody.Speed -= _gameSettings.CollisionDecel * _manager.LevelManager.AverageDT;
                if (_physicBody.Speed < 0.0f)
                    _physicBody.Speed = 0.0f;
            }

            // gradually decrease speed
            if (_boostTimer > 0.0f)
            {
                if (_boostTimer < SpeedChangeDuration)
                {
                    _accel = Mathf.Lerp(_accel, _preBoostAccel, 1.0f - (_boostTimer / SpeedChangeDuration));
                    _physicBody.Speed = Mathf.Lerp(_physicBody.Speed, _physicBody.Speed*BoostSpeedReductionFactor, 1.0f - (_boostTimer / SpeedChangeDuration));
                }
            }
            else if (_physicBody.Speed < BaseMaxSpeed)
            {
                _maxSpeed = BaseMaxSpeed;
                _accel = BaseAccel;
            }
            else if (_physicBody.Speed > previousSpeed + MathUtils.Epsilon)
            {
                var wasLessThanMax = previousSpeed <= _maxSpeed;
                var isLessThanMax = _physicBody.Speed <= _maxSpeed;
                if (wasLessThanMax && !isLessThanMax)
                {
                    _maxSpeed += MaxSpeedIncr;
                    _accel *= AccelDecrFactor;
                }
            }

            if (!MathUtils.IsZero(lateral))
            {
                var factor = Mathf.Abs(_physicBody.Speed) / _maxSpeed;
                var turnAccel = Mathf.Lerp(TurnAccelMax, TurnAccelMin, Mathf.Clamp01(factor));

                // turn more when in collision
                if (_collision)                
                    turnAccel *= TurnAccelCollisionFactor;

                // turn gradually more when non-accelerating
                if (_accelDuration < 0.0f)
                    turnAccel *= Mathf.Lerp(1.0f, TurnAccelNonSteerFactor, Mathf.Clamp01(_turnDuration / MaxTurnDuration));

                // damp turn accel
                _turnAccel = Mathf.Lerp(_turnAccel, turnAccel * lateral, _manager.LevelManager.AverageDT/TurnAccelDampDuration);

                _turnAngle = _turnAccel * _manager.LevelManager.AverageDT;
                _targetBanking = MaxBanking * -Mathf.Sign(lateral);
            }
            else
            {
                _turnAccel = Mathf.Lerp(_turnAccel, 0.0f, _manager.LevelManager.AverageDT / TurnAccelDampDuration);

                _targetBanking = 0.0f;
                _turnAngle = 0.0f;
            }

            // update jet FX
            //if(_leftJetFX)
            //{
            //    var budget = boostMode ? 1.0f : 0.5f;
            //    var speedFactor = _physicBody.Speed / _maxSpeed;
            //    _leftJetFX.SetIntensity(speedFactor * budget);
            //    _rightJetFX.SetIntensity(speedFactor * budget);
            //}
        }

        void UpdateLocalRotationTunnel(Vector3 up)
        {
            if (!_onTunnel)
            {
                UpdateLocalRotationFlat(up);
            }
            else
            {
                var toSurface = (Quaternion.AngleAxis(_tunnelAngle, _tangent) * -up).normalized;
                UpdateLocalRotationFlat(-toSurface);
            }
        }

        float _turnDuration;
        void UpdateLocalRotationFlat(Vector3 up)
        {
            _up = up;

            // adjust forward depending on up vector, by projecting forward onto base plane
            var plane = new Plane(_up, transform.position);
            var rayOrigin = transform.position + _forward;
            var rayDir = plane.GetSide(rayOrigin) ? -_up : _up;
            var ray = new Ray(rayOrigin, rayDir);
            float toPlane = 0.0f;
            if (plane.Raycast(ray, out toPlane))
            {
                var intersection = ray.origin + ray.direction * toPlane;
                _forward = (intersection - transform.position).normalized;
            }

            rayOrigin = transform.position + _rotationForward;
            rayDir = plane.GetSide(rayOrigin) ? -_up : _up;
            ray = new Ray(rayOrigin, rayDir);
            toPlane = 0.0f;
            if (plane.Raycast(ray, out toPlane))
            {
                var intersection = ray.origin + ray.direction * toPlane;
                _rotationForward = (intersection - transform.position).normalized;
            }

            // rotate forward
            if (!MathUtils.IsZero(_turnAngle))
            {
                _turnDuration += _manager.LevelManager.AverageDT;
                _forward = (Quaternion.AngleAxis(_turnAngle, _up) * _forward).normalized;
                _rotationForward = (Quaternion.AngleAxis(_turnAngle * TurnExaggerationFactor, _up) * _rotationForward).normalized;
            }
            else
            {
                _turnDuration = 0.0f;
                _forward = Vector3.Lerp(_forward, _rotationForward, _manager.LevelManager.AverageDT / TurnExaggerationRestoreDuration);
            }
        }

        void CheckGroundFly()
        {
            if (_hover.GetState() == Hover.State.Moving && _groundState == GroundState.Normal)
            {
                // going down after a hill
                if (_physicBody.Speed > GroundEscapeSpeed)
                {
                    _groundTakeOffPos = transform.position;
                    _groundTakeOffUp = _up;
                    _groundFallAmount = 0.0f;
                    _groundMiniFallSpeed = 0.0f;
                    _groundState = GroundState.MiniFly;
                }
            }
        }

        bool _useLookingUpCamera;
        void UpdateForwardMotion()
        {
            var previousDist = _forwardDist;
            _fwdFactor = Vector3.Dot(_tangent, _physicBody.Direction);
            var deltaDist = _physicBody.Speed * _fwdFactor * _manager.LevelManager.AverageDT;
            _forwardDist += deltaDist;

            // update traveled distance
            _traveledDistance += deltaDist;
            _localTraveledDistance += deltaDist;

            var isColliding = !(_physicBody.CollisionResponseTimer < 0.0f);
            if(isColliding && !_wasColliding)
            {
                Console.Print(string.Format("previousDist {0} - _forwardDist {1} (_tangent {2}, dir {3}, _fwdFactor {4})", previousDist, _forwardDist, _tangent, _physicBody.Direction, _fwdFactor));
            }
            _wasColliding = isColliding;

            var curveLength = _track.GetLength(_curveIndex);
            var previousSegment = _currentSegment;
            var reachedTrackEnd = false;
            if (_forwardDist > curveLength)
            {
                if (_track.Curves[_curveIndex].Points[3].JumpSpot)
                    CheckGroundFly();

                if (_curveIndex == (_track.Curves.Length - 1) && _track.NextTrack != null)
                {
                    var thisTrack = _track;
                    _track = _track.NextTrack.GetComponent<BezierPath>();
                    _curveIndex = 0;

                    if (!thisTrack.IsAlignedToNextTrack())
                    {
                        // Fly to next track
                        _forwardDist = 0.0f;
                        _airTime = 0.0f;
                        _state = State.Air;
                        _airSpeed = Vector3.Project((transform.position - _previousPos), _tangent) / _manager.LevelManager.AverageDT;
                    }
                    else
                    {
                        _forwardDist = _forwardDist - curveLength;
                    }
                }
                else
                {
                    _forwardDist = _forwardDist - curveLength;
                    _curveIndex = (_curveIndex + 1) % _track.Curves.Length;
                }

                if (_curveIndex == 0 && ReferenceEquals(_track.gameObject, _manager.LevelManager.StartingTrack))
                {
                    var travelThreshold = 9.0f;
                    if(_localTraveledDistance > travelThreshold)
                    {
                        _localTraveledDistance = 0.0f;
                        ++_currentLap;
                    }                    
                    if(RaceFinished)
                    {
                        _playerCamera.OnRaceFinished();

                        // finished panel
                        var finishedPanel = Instantiate(_gameSettings.RaceFinishedPanel);
                        finishedPanel.transform.SetParent(InGameUI.transform, false);
                        var finishingPositionText = finishedPanel.transform.FindChild("Panel/FinishingPosition").GetComponent<Text>();                        
                        finishingPositionText.text = string.Format("POS {0}/{1}", _positionInRace, _manager.LevelManager.Players.Count);
                    }
                }

                reachedTrackEnd = true;
            }
            else if (_forwardDist < 0.0f)
            {
                if (_track.Curves[_curveIndex].Points[0].JumpSpot)
                    CheckGroundFly();

                if (_curveIndex == 0 && _track.GetPreviousTrack() != null)
                {
                    if (!_track.IsAlignedToPreviousTrack())
                    {
                        // fall down
                        _airTime = 0.0f;
                        _state = State.Air;
                        _airSpeed = (transform.position - _previousPos) / _manager.LevelManager.AverageDT;
                        _forceFall = true;
                    }
                    else
                    {
                        _track = _track.GetPreviousTrack().GetComponent<BezierPath>();
                        _curveIndex = _track.Curves.Length - 1;
                        curveLength = _track.GetLength(_curveIndex);
                        _forwardDist = curveLength + _forwardDist;
                    }
                }
                else
                {
                    _curveIndex--;
                    if (_curveIndex < 0)
                        _curveIndex = _track.Curves.Length - 1;
                    curveLength = _track.GetLength(_curveIndex);
                    _forwardDist = curveLength + _forwardDist;
                }

                reachedTrackEnd = true;
            }

            if (reachedTrackEnd)
            {
                _currentSegment = _track.transform.GetChild(_curveIndex).GetComponent<TrackSegment>();
                //var oldRadius = _curveRadius;
                //_curveRadius = _track.transform.localScale.x * _currentSegment.Radius;
                _triggeredBoostInSegment = false;

                if (_currentSegment.PathType != previousSegment.PathType)
                {
                    if (previousSegment.PathType == TrackSegment.Type.Tunnel)
                    {
                        if (_onTunnel)
                        {
                            // air time
                            _onTunnel = false;
                            _airTime = 0.0f;
                            _state = State.Air;
                            _airSpeed = (transform.position - _previousPos) / _manager.LevelManager.AverageDT;

                            // if out of the on-track angle range, fall out
                            //var minAngle = Mathf.Atan(previousSegment.TunnelFlatRatio / previousSegment.TunnelRadiusRatio) * Mathf.Rad2Deg;
                            //var rangeMin = 180.0f - minAngle;
                            //var rangeMax = 180.0f + minAngle;
                            //if (_tunnelAngle < rangeMin || _tunnelAngle > rangeMax)
                            //{
                            //    Debug.Break();
                            //    // fall out
                            //    _forceFall = true;
                            //}
                            _onTunnel = false;
                        }
                    }

                    SetUpdateMethods();
                }

                //if (_state == State.Ground)
                //{
                //    // adjust x position depending on radius change
                //    var deltaRadius = _curveRadius - oldRadius;
                //    _curvePos.x += deltaRadius;
                //}

                // update server
                if (_playerSync.isLocalPlayer)
                    _playerSync.ClientSendTrackInfoToServer(_track.TrackIndex, _curveIndex);

                // update camera type
                if(_fwdFactor > 0.0f)
                {
                    if (_currentSegment.EnterLookingUpSlope)
                        _useLookingUpCamera = true;
                    else if (previousSegment.ExitLookingUpSlope)
                        _useLookingUpCamera = false;
                }
                else if(_fwdFactor < 0.0f)
                {
                    if (_currentSegment.ExitLookingUpSlope)
                        _useLookingUpCamera = true;
                    else if (previousSegment.EnterLookingUpSlope)
                        _useLookingUpCamera = false;
                }
            }

            _curvePos.y = _track.GetFactor(_curveIndex, _forwardDist);

            // TopLight should be brighter when racing on an upside down track
            // convert from dot product to [0 - 1] (same direction = 0, opposite = 1)
            var lightFactor = 1.0f - ((Vector3.Dot(_up, Vector3.up) + 1.0f) / 2.0f);
            _topLight.intensity = Mathf.Lerp(_normalTopLightIntensity, 1.0f, lightFactor);
        }

        bool _collisionTowardsRight;
        void UpdateLateralMotionFlat(Vector3 up, Vector3 right)
        {
            var curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
            _lateral0 = curvePoint - right * _manager.LevelManager.TrackRadius;
            _lateralFactor = Vector3.Dot(right, _physicBody.Direction);

            var deltaX = _physicBody.Speed * _lateralFactor * _manager.LevelManager.AverageDT;
            if (_groundState == GroundState.MiniFly)
            {
                deltaX = 0.0f;
            }

            var hasZeroSpeed = MathUtils.IsZero(deltaX);
            var maxX = (_manager.LevelManager.TrackRadius * 2.0f) - _manager.LevelManager.TackDistFromWall;
            var towardsLeftWall = deltaX < 0.0f && _curvePos.x + deltaX < _manager.LevelManager.TackDistFromWall;
            var towardsRightWall = deltaX > 0.0f && _curvePos.x + deltaX > maxX;
            var willCollide = towardsLeftWall || towardsRightWall;
            if (hasZeroSpeed || !willCollide)
            {
                var factor = Mathf.Clamp01(Mathf.Abs(_physicBody.Speed) / MaxSpeed);
                var reduction = Mathf.Lerp(LateralReductionFactor.Max, LateralReductionFactor.Min, factor * factor);
                _curvePos.x += deltaX * reduction;
                _collision = false;
            }
            else
            {
                _collisionTowardsRight = towardsRightWall;
                _curvePos.x = towardsLeftWall ? _manager.LevelManager.TackDistFromWall : maxX;
                _collision = true;
            }

            HandleEnvCollision();
        }

        void UpdateLateralMotionTunnel(Vector3 up, Vector3 right)
        {
            var flatRatio = _currentSegment.TunnelFlatRatio;
            var minAngle = Mathf.Atan(flatRatio / _currentSegment.TunnelRadiusRatio) * Mathf.Rad2Deg;
            var maxAngle = 360.0f - minAngle;
            var minX = _manager.LevelManager.TrackRadius * (1.0f - flatRatio);
            var maxX = (2.0f * _manager.LevelManager.TrackRadius) - minX;

            var curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
            _lateral0 = curvePoint - right * _manager.LevelManager.TrackRadius; // only used when on flat surface

            if (!_onTunnel)
            {
                _lateralFactor = Vector3.Dot(right, _physicBody.Direction);
                var deltaX = _physicBody.Speed * _lateralFactor * _manager.LevelManager.AverageDT;
                if (_curvePos.x + deltaX >= minX && _curvePos.x + deltaX <= maxX)
                {
                    _curvePos.x += deltaX;
                }
                else
                {
                    // ride tunnel
                    _onTunnel = true;
                    if (_curvePos.x + deltaX < minX)
                        _tunnelAngle = maxAngle;
                    else
                        _tunnelAngle = minAngle;

                    _updateLocalRotation(up);
                }
            }
            else
            {
                var surfaceTangent = Vector3.Cross(_up, _tangent).normalized;
                _lateralFactor = Vector3.Dot(_physicBody.Direction, surfaceTangent);
                var deltaX = _physicBody.Speed * _lateralFactor * _manager.LevelManager.AverageDT;
                deltaX *= 2.0f; // hack to make speed uniform on tunnel

                if (_tunnelAngle + deltaX <= maxAngle && _tunnelAngle + deltaX >= minAngle)
                {
                    _tunnelAngle += deltaX;
                }
                else
                {
                    // back to flat track
                    _onTunnel = false;
                    if (_tunnelAngle + deltaX > maxAngle)
                        _curvePos.x = minX;
                    else
                        _curvePos.x = maxX;

                    _updateLocalRotation(up);
                }
            }
        }

        void UpdateTransformFlat(Vector3 up, Vector3 right)
        {
            //var oldPos = transform.position;
            transform.position = _lateral0 + right * _curvePos.x;
            transform.rotation = Quaternion.LookRotation(_rotationForward, up);

            // make collider bigger than delta pos to avoid tunnelling
            //var deltaPos = transform.position - oldPos;
            //_collider.radius = Mathf.Max(deltaPos.magnitude, _originalRadius);
        }

        void UpdateTransformTunnel(Vector3 up, Vector3 right)
        {
            if (!_onTunnel)
            {
                transform.position = _lateral0 + right * _curvePos.x;
                transform.rotation = Quaternion.LookRotation(_rotationForward, up);
            }
            else
            {
                var curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
                var centerPoint = curvePoint + up * _manager.LevelManager.TrackRadius * _currentSegment.TunnelRadiusRatio;
                var toSurface = (Quaternion.AngleAxis(_tunnelAngle, _tangent) * -up).normalized;
                transform.position = centerPoint + toSurface * _manager.LevelManager.TrackRadius;
                transform.rotation = Quaternion.LookRotation(_rotationForward, _up);
            }
        }

        float _shootingTimer = -1.0f;
        int _shootingCannon = -1;
        public void UpdateShooting()
        {
            if (!_playerSync.isLocalPlayer)
                return;

            // Missiles
            //if (_shootingTimer < 0.0f)
            //{
            //    if (_attackCount > 0 && !NotReadyToRace && Input.GetButtonDown("P" + (_playerSync.playerControllerId + 1) + "_Fire1"))
            //    {
            //        if (_shootingCannon < 0)
            //            _shootingCannon = Random.Range(0, 2);
            //        else
            //            _shootingCannon = 1 - _shootingCannon;
            //        _playerSync.ClientShoot(_shootingCannon);

            //        _shootingTimer = _gameSettings.ShootingFrequency;
            //        --_attackCount;
            //        UpdateInventory();
            //    }
            //}
            //else
            //    _shootingTimer -= Time.deltaTime;

            // Bullets
            if (!NotReadyToRace)
            {
                if (Input.GetButton("P" + (_playerSync.playerControllerId + 1) + "_Fire1"))
                    _playerSync.ClientShootBullet(true);
                else
                    _playerSync.ClientShootBullet(false);
            }
            UpdateBullets();

            // Old logic
            //if (Input.GetButtonDown("P" + (_playerSync.playerControllerId + 1) + "_Fire1"))
            //{
            //    if (_powerUp != null)
            //    {
            //        switch (_powerUp.Type)
            //        {
            //            case PowerUp.EType.Missile:
            //                {
            //                    var cannon = Random.Range(0, 2);
            //                    _playerSync.ClientShoot(cannon);
            //                }
            //                break;

            //            case PowerUp.EType.Mine:
            //                {
            //                    _playerSync.ClientDropMine();
            //                }
            //                break;

            //            case PowerUp.EType.Shield:
            //                {

            //                }
            //                break;
            //        }

            //        // clear power up
            //        _powerUp = null;
            //        _uiPowerUp.gameObject.SetActive(false);
            //    }
            //}
        }

        void UpdateBoost()
        {
            if (_boostCount > 0 && !NotReadyToRace && Input.GetButtonDown("P" + (_playerSync.playerControllerId + 1) + "_Boost"))
            {
                if (_boostTimer < 0.0f)
                {
                    StartBoost();
                    --_boostCount;
                    UpdateInventory();
                }
            }
        }

        void DetermineLocalBasis(out Vector3 up, out Vector3 right)
        {
            _tangent = _track.GetTangent(_curveIndex, _curvePos.y).normalized;            
            up = _track.Curves[_curveIndex].GetUp(_curvePos.y).normalized;
            right = Vector3.Cross(up, _tangent).normalized;

            var banking = Quaternion.AngleAxis(_track.GetBanking(_curveIndex, _curvePos.y), _tangent);
            up = (banking * up).normalized;
            right = (banking * right).normalized;

            _right = right;
        }

        void UpdateBanking()
        {
            _banking = Mathf.Lerp(_banking, _targetBanking, _manager.LevelManager.AverageDT * BankingFactor);
            _body.localRotation = Quaternion.AngleAxis(_banking, Vector3.forward);
        }

        float _targetFOV;
        void UpdateCamera()
        {
            _targetFOV = MinFOV;
            if (_boostTimer < 0.0f)
            {
                var factor = Mathf.Abs(_physicBody.Speed) / _maxSpeed;
                var target = Mathf.Lerp(MinFOV, MaxFOV, factor * factor * factor);
                _targetFOV = MathUtils.Damp(_targetFOV, target, _manager.LevelManager.AverageDT, FOVChangeDuration);
            }
            else
            {
                _targetFOV = MathUtils.Damp(_targetFOV, MaxFOV * BoostFOVFactor, _manager.LevelManager.AverageDT, FOVChangeDuration);
            }

            _playerCamera.UpdateCamera(_targetFOV, _useLookingUpCamera);
        }

        void UpdateIdle()
        {
        }

        void HandleEnvCollision()
        {
            var speedFactor = Mathf.Abs(_physicBody.Speed * _fwdFactor);
            if (_collision)
            {
                if (!_previousCollision)
                    _targetForwardMotion = speedFactor; // keep only forward speed component
                else
                    _targetForwardMotion = Mathf.Min(_targetForwardMotion, speedFactor); // gradually lower to minimum speed
            }
            else
            {
                if (_previousCollision)
                {
                    // looks like we're moving out of collision, determine final speed
                    _physicBody.Speed = _targetForwardMotion;
                }
            }
        }

        void SetUpdateMethods()
        {
            if (_currentSegment.PathType == TrackSegment.Type.Flat)
            {
                _updateLocalRotation = UpdateLocalRotationFlat;
                _updateLateralMotion = UpdateLateralMotionFlat;
                _updateTransform = UpdateTransformFlat;
            }
            else
            {
                _updateLocalRotation = UpdateLocalRotationTunnel;
                _updateLateralMotion = UpdateLateralMotionTunnel;
                _updateTransform = UpdateTransformTunnel;
            }
        }

        void UpdateInventory()
        {
            _boostIcon.color = _boostCount > 0 ? _boostColor : _manager.LevelManager.InventoryExhaustedColor;
            _attackIcon.color = _attackCount > 0 ? _attackColor : _manager.LevelManager.InventoryExhaustedColor;
            _boostExhausted.SetActive(_boostCount == 0);
            _attackExhausted.SetActive(_attackCount == 0);
            _boostText.text = "x " + _boostCount;
            _attackText.text = "x " + _attackCount;

            if (_boostCount == 0 && _attackCount == 0)
                _manager.LevelManager.ClearTutorial();
        }

        public void OnRespawn()
        {
            _physicBody.Speed = 0.0f;
            _physicBody.CollisionResponseTimer = -1.0f;
            var tangent0 = _track.GetTangent(_curveIndex, 0.0f).normalized;
            _forward = _rotationForward = tangent0;
            _curvePos = new Vector2(_manager.LevelManager.TrackRadius, 0.0f);
            _forwardDist = 0.0f;

            Vector3 up, right;
            DetermineLocalBasis(out up, out right);
            _updateTransform(up, right); // local rotation need an on-track transform
            _updateLocalRotation(up);
            _updateLateralMotion(up, right);
            _updateTransform(up, right);
            _bodyHolder.transform.localPosition = Vector3.up * _groundLandHeight;

            _playerCamera.OnRespawn();
            _state = State.Ground;
        }

        public void GetCurveCoords(out Vector2 pos, out float dist, out BezierPath path, out int index)
        {
            pos = _curvePos;
            dist = _forwardDist;
            path = _track;
            index = _curveIndex;
        }

        public void GetCurveCoords(out Vector2 pos, out float dist, out int trackIndex, out int index)
        {
            pos = _curvePos;
            dist = _forwardDist;
            trackIndex = _track.TrackIndex;
            index = _curveIndex;
        }

        public void SetCurveCoords(Vector2 pos, float dist, int trackIndex, int index)
        {
            _curvePos = pos;
            _forwardDist = dist;
            var track = GameObject.FindObjectsOfType<BezierPath>().FirstOrDefault(t => t.TrackIndex == trackIndex);
            if (track)
                _track = track;
            _curveIndex = index;
        }

        public float GetSpeed()
        {
            if (_playerSync.isLocalPlayer)
                return _physicBody.Speed;
            else
                return _playerSync.PhysicBody.Speed;
        }

        public void ResetGroundState()
        {
            _groundState = GroundState.Normal;
        }

        public void OnPowerUpCollected(PowerUp.EType type)
        {
            switch (type)
            {
                case PowerUp.EType.Missile:
                    _attackCount++;
                    break;

                case PowerUp.EType.Boost:
                    _boostCount++;
                    break;
            }

            UpdateInventory();
        }

        public void OnMissileHit()
        {
            if (!_playerSync.isLocalPlayer)
            {
                if(!_playerSync.isServer)
                    Console.Print("EXPECTING _playerSync.isServer to be TRUE!!");
                _playerSync.RpcOnMissileHit();
            }                
            else
                _physicBody.Speed *= _gameSettings.MissileHitFactor;
        }

        public void ShootBullets(bool shoot)
        {
            if (shoot && !_shootingBullets)
                _bulletsTimer = -1.0f;

            _shootingBullets = shoot;            
        }
    }
}
