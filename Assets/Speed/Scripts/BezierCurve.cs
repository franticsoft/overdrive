﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace frantic
{
    [Serializable]
    public class BezierCurve : System.Object
    {
        public BezierPoint[] Points;
        public Vector3 InitialUp;

        public float GetLength()
        {
            if (_infos == null || _infos.Length != infosSize)
                UpdateInfos();

            return _length;
        }

        public Vector3 GetPoint(float t)
        {
            if (Points == null || Points.Length < 4)
                return Vector3.zero;

            t = Mathf.Clamp01(t);
            float oneMinusT = 1f - t;
            return
                oneMinusT * oneMinusT * oneMinusT * Points[0].Position +
                3f * oneMinusT * oneMinusT * t * Points[1].Position +
                3f * oneMinusT * t * t * Points[2].Position +
                t * t * t * Points[3].Position;
        }

        public Vector3 GetTangent(float t)
        {
            if (Points == null || Points.Length < 4)
                return Vector3.zero;

            t = Mathf.Clamp01(t);
            return GetTangent(t, Points[0].Position, Points[1].Position, Points[2].Position, Points[3].Position);
        }

        public Vector3 GetNonStableUp(float t)
        {            
            var tangent = GetTangent(t).normalized;
            Vector3 up, right;
            if (MathUtils.GetBasis(tangent, out up, out right))
                return up;

            return Vector3.forward;
        }

        public Vector3 GetUp(float t)
        {
            // make sure the up is consistent with the curve structure, as initiated by the initial up vector
            var step = .1f;
            if(t <= step)
            {
                var up = GetNonStableUp(t);
                if (Vector3.Dot(up, InitialUp) < 0.0f)
                    up *= -1.0f;
                return up;
            }

            var previousUp = InitialUp;
            var curUp = InitialUp;
            var curT = step;
            do
            {
                curUp = GetNonStableUp(curT);
                if (Vector3.Dot(curUp, previousUp) < 0.0f)
                    curUp *= -1.0f;

                previousUp = curUp;
                curT += step;
                if (curT >= t)
                {
                    curUp = GetNonStableUp(t);
                    if (Vector3.Dot(curUp, previousUp) < 0.0f)
                        curUp *= -1.0f;

                    return curUp;
                }
            } while (true);
        }

        public float GetBanking(float t)
        {
            if (Points == null || Points.Length < 4)
                return 0.0f;

            t = Mathf.Clamp01(t);
            float oneMinusT = 1f - t;

            // intermediate bankings for tangents            
            var oneThird = 1.0f / 3.0f;
            var twoThirds = 2.0f / 3.0f;
            var startBanking = Points[0].Banking;
            var endBanking = Points[3].Banking;
            var banking1 = twoThirds * startBanking + oneThird * endBanking;
            var banking2 = oneThird * startBanking + twoThirds * endBanking;

            if (Points[0].FlatBanking)
                banking1 = startBanking;
            
            if(Points[3].FlatBanking)
                banking2 = endBanking;
            
            return
            oneMinusT * oneMinusT * oneMinusT * startBanking +
            3f * oneMinusT * oneMinusT * t * banking1 +
            3f * oneMinusT * t * t * banking2 +
            t * t * t * endBanking;
        }

        private Vector3 GetTangent(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            float oneMinusT = 1f - t;
            return
                3f * oneMinusT * oneMinusT * (p1 - p0) +
                6f * oneMinusT * t * (p2 - p1) +
                3f * t * t * (p3 - p2);
        }

        struct CurveInfo
        {
            public float factor { get; set; }
            public float dist { get; set; }
        }

        const int infosSize = 20000;

        [SerializeField, HideInInspector]
        CurveInfo[] _infos;

        [SerializeField, HideInInspector]
        float _length;

        public void UpdateInfos()
        {
            if (_infos == null)
                _infos = new CurveInfo[infosSize];

            _length = 0.0f;
            var f1 = 0.0f;
            _infos[0] = new CurveInfo() { factor = 0.0f, dist = 0.0f };
            for (int i = 1; i < _infos.Length; ++i)
            {
                var f2 = (float)i / (_infos.Length - 1);
                var pt1 = GetPoint(f1);
                var pt2 = GetPoint(f2);
                _length += (pt2 - pt1).magnitude;
                _infos[i] = new CurveInfo() { factor = f2, dist = _length };
                f1 = f2;
            }
        }

        public float GetFactor(float localDist)
        {
            if (_infos == null || _infos.Length != infosSize)
                UpdateInfos();

            return GetFactor(0, _infos.Length, localDist);
        }

        float GetFactor(int startIndex, int size, float dist)
        {
            if (size > 2)
            {
                int halfIndex = startIndex + (size / 2);
                if (dist < _infos[halfIndex].dist)
                    return GetFactor(startIndex, size / 2, dist);
                else
                    return GetFactor(halfIndex, size / 2, dist);
            }
            else
            {
                var srcIndex = startIndex;
                var destIndex = startIndex + 1;
                if (dist > _infos[destIndex].dist)
                {
                    if (destIndex < _infos.Length - 1)
                    {
                        srcIndex = startIndex + 1;
                        destIndex = startIndex + 2;
                    }
                    else
                    {
                        dist = _infos[destIndex].dist;
                    }
                }
                var factor = (dist - _infos[srcIndex].dist) / (_infos[destIndex].dist - _infos[srcIndex].dist);
                return Mathf.Lerp(_infos[srcIndex].factor, _infos[destIndex].factor, factor);
            }
        }

        public float GetDist(float factor)
        {
            if (_infos == null)
                UpdateInfos();

            return GetDist(0, _infos.Length, factor);
        }

        float GetDist(int startIndex, int size, float factor)
        {
            if (size > 2)
            {
                int halfIndex = startIndex + (size / 2);
                if (factor < _infos[halfIndex].factor)
                    return GetDist(startIndex, size / 2, factor);
                else
                    return GetDist(halfIndex, size / 2, factor);
            }
            else
            {
                var srcIndex = startIndex;
                var destIndex = startIndex + 1;
                if (factor > _infos[destIndex].factor)
                {
                    if (destIndex < _infos.Length - 1)
                    {
                        srcIndex = startIndex + 1;
                        destIndex = startIndex + 2;
                    }
                    else
                    {
                        factor = _infos[destIndex].factor;
                    }
                }
                var f = (factor - _infos[srcIndex].factor) / (_infos[destIndex].factor - _infos[srcIndex].factor);
                return Mathf.Lerp(_infos[srcIndex].dist, _infos[destIndex].dist, f);
            }
        }
    }
}

