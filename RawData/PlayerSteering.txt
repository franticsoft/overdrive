            
			// DetermineLocalBasis
			_tangent = _track.GetTangent(_curveIndex, _curvePos.y).normalized;            
            up = _track.Curves[_curveIndex].GetUp(_curvePos.y);
            right = Vector3.Cross(up, _tangent);

			// UpdateLocalRotationFlat
			var plane = new Plane(_up, transform.position);
            var rayOrigin = transform.position + _forward;
            var rayDir = plane.GetSide(rayOrigin) ? -_up : _up;
            var ray = new Ray(rayOrigin, rayDir);
            float toPlane = 0.0f;
            if (plane.Raycast(ray, out toPlane))
            {
                var intersection = ray.origin + ray.direction * toPlane;
                _forward = (intersection - transform.position).normalized;
            }
            if (!MathUtils.IsZero(_turnAngle))                     
                _forward = Quaternion.AngleAxis(_turnAngle, _up) * _forward;			
			
			_physicBody.Direction = _forward;			
			
			// UpdateForwardMotion
			_fwdFactor = Vector3.Dot(_tangent, _physicBody.Direction);
            var deltaDist = _physicBody.Speed * _fwdFactor * _levelMgr.AverageDT;
            _forwardDist += deltaDist;
			_curvePos.y = _track.GetFactor(_curveIndex, _forwardDist);
			
			// DetermineLocalBasis
			_tangent = _track.GetTangent(_curveIndex, _curvePos.y).normalized;            
            up = _track.Curves[_curveIndex].GetUp(_curvePos.y);
            right = Vector3.Cross(up, _tangent);

			// _updateLateralMotion
			var curvePoint = _track.GetPoint(_curveIndex, _curvePos.y);
			_lateral0 = curvePoint - right * _curveRadius;
			_lateralFactor = Vector3.Dot(right, _physicBody.Direction);
            var deltaX = _physicBody.Speed * _lateralFactor * _levelMgr.AverageDT;
			_curvePos.x += deltaX;

			// _updateTransform
            transform.position = _lateral0 + right * _curvePos.x;
            transform.rotation = Quaternion.LookRotation(_forward, up);
